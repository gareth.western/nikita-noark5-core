import React, {Component} from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";

import {Grid} from "semantic-ui-react";

import './App.css';
import LoginPage from "./pages/LoginPage";
import NavigationBar from './navigation';
import CaseHandlerDashboard from "./pages/CaseHandlerDashboard"
import CaseFilePage from "./pages/CaseFilePage";
import LandingPage from "./pages/LandingPage";
import RecordsManagerDashboard from "./pages/RecordsManagerDashboard";
import LeaderDashboard from "./pages/LeaderDashboard";
import NewUserPage from "./pages/NewUserPage";
import RegistryEntryPage from "./pages/RegistryEntryPage";
import AdminDashboard from "./pages/AdminDashboard";
import {AuthContextProvider} from "./components/store/AuthContext";
import {AppContextProvider} from "./components/store/AppContext";
import CorrespondencePartPage from "./pages/CorrespondencePartPage";

class App extends Component {

    state = {
        organisationName: 'Eksempel kommune'
    }

    render() {
        const {organisationName} = this.state;
        return (
            <AuthContextProvider>
                <AppContextProvider>
                    <div className="App">
                        <BrowserRouter basename='/gui/'>
                            <Grid padded className="tablet computer only">
                                <NavigationBar organisationName={organisationName}/>
                            </Grid>
                            <Routes>
                                <Route path="" element={<LandingPage/>}/>
                                <Route path="ny-bruker" element={<NewUserPage/>}/>
                                <Route path="/login" element={<LoginPage/>}/>
                                <Route path="/arkivar" element={<RecordsManagerDashboard/>}/>
                                <Route path="/leder" element={<LeaderDashboard/>}/>
                                <Route path="/admin" element={<AdminDashboard/>}/>
                                <Route path="/saksbehandler" element={<CaseHandlerDashboard/>}/>
                                <Route path='/saksbehandler/saksmappe/journalpost' element={<RegistryEntryPage/>}/>
                                <Route path='/saksbehandler/saksmappe' element={<CaseFilePage/>}/>
                                <Route path='/cor/' element={<CorrespondencePartPage/>}/>
                            </Routes>
                        </BrowserRouter>
                    </div>
                </AppContextProvider>
            </AuthContextProvider>
        );
    }
}

export default App;
