export const FONDS_STRUCTURE_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/';
export const FONDS_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkiv/';
export const NEW_FONDS_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkiv/';
export const FONDS_CREATOR_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivskaper/';
export const NEW_FONDS_CREATOR_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkivskaper/';
export const SERIES_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivdel/';
export const NEW_SERIES_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkivdel/';
export const CLASSIFICATION_SYSTEM_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/klassifikasjonssystem/';
export const NEW_CLASSIFICATION_SYSTEM_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-klassifikasjonssystem/';
export const CLASS_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/klasse/';
export const NEW_CLASS_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-klasse/';
export const FILE_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/mappe/';
export const NEW_FILE_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-mappe/';
export const RECORD_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/registrering/';
export const NEW_RECORD_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-registrering/';
export const DOCUMENT_DESCRIPTION_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/dokumentbeskrivelse/';
export const NEW_DOCUMENT_DESCRIPTION_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-dokumentbeskrivelse/';
export const DOCUMENT_OBJECT_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/dokumentobjekt/';
export const NEW_DOCUMENT_OBJECT_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-dokumentobjekt/';

// RELS for casehandling
export const CASE_HANDLING_REL = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/';
export const CASE_FILE_REL = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/saksmappe/';
export const NEW_CASE_FILE_REL = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/ny-saksmappe/';
export const REGISTRY_ENTRY_FILE_REL = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/journalpost/';
export const REL_CORRESPONDENCE_PART = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/korrespondansepart/';
export const REL_NEW_CORRESPONDENCE_PART_PERSON = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-korrespondansepartperson/';
export const NEW_REGISTRY_ENTRY_REL = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/ny-journalpost/';
export const RECORD_NOTE_REL = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/arkivnotat/';
export const NEW_RECORD_NOTE_REL = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/ny-arkivnotat/';
export const DOCUMENT_FLOW_REL = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/dokumentflyt/';
export const NEW_DOCUMENT_FLOW_REL = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/ny-dokumentflyt/';
export const EXPAND_FILE_TO_CASE_FILE = 'https://rel.arkivverket.no/noark5/v5/api/sakarkiv/utvid-til-saksmappe/';
export const DOCUMENT_FILE_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/fil/';
export const LINKS = '_links';
export const SELF = 'self';

export const CONTENT_TYPE_NOARK = 'application/vnd.noark5+json';

export const documentObjects = [{
    'versjonsnummer': 12000,
    'variantformat': 'Produkjsjon',
    'format': 'odt',
    'sjekksum': 'edcaf37a214097d440376805430404dac1fb64fe8c1cdcfda23f87118d8a04d1',
    'sjekksumAlgoritme': 'SHA-256',
    'filnavn': 'The man how dared to cross the road',
    'filstoerrelse': 768383,
    'mimeType': 'application/vnd.oasis.opendocument.text',
    'opprettetDato': '2022-01-19T09:00:01+0200',
    'opprettetAv': 'Hans Hansen'
}, {
    'versjonsnummer': 1,
    'variantformat': 'Sladdet',
    'format': 'odt',
    'sjekksum': 'edcaf37a214097d440376805430404dac1fb64fe8c1cdcfda23f87118d8a04d1',
    'sjekksumAlgoritme': 'SHA-256',
    'filnavn': 'The man how dared to cross the road',
    'filstoerrelse': 45452,
    'mimeType': 'application/vnd.oasis.opendocument.text',
    'opprettetDato': '2022-01-19T09:00:01+0200',
    'opprettetAv': 'Hans Hansen'
}, {
    'versjonsnummer': 1,
    'variantformat': 'Arkiv',
    'format': 'odt',
    'sjekksum': 'edcaf37a214097d440376805430404dac1fb64fe8c1cdcfda23f87118d8a04d1',
    'sjekksumAlgoritme': 'SHA-256',
    'filnavn': 'The man how dared to cross the road',
    'filstoerrelse': 12345,
    'mimeType': 'application/vnd.oasis.opendocument.text',
    'opprettetDato': '2022-01-19T09:00:01+0200',
    'opprettetAv': 'Hans Hansen'
}];

export const documentFlowLeader = [{
    'systemID': 'f106ddf8-0c50-4ff0-bce4-ccf45e9e80fd',
    'flytTil': 'leder',
    'flytFra': 'arkivleder',
    'flytMottattDato': '2022-01-19T09:00:01+0200',
    'flytSendtDato': '2022-01-19T09:00:01+0200',
    'flytStatus': '',
    'flytMerknad': ''
}, {
    'systemID': '6754d488-900c-4016-b86d-dd0535e606ef',
    'flytTil': 'leder',
    'flytFra': 'arkivleder',
    'flytMottattDato': '2022-01-19T09:00:01+0200',
    'flytSendtDato': '2022-01-19T09:00:01+0200',
    'flytStatus': '',
    'flytMerknad': ''
}, {
    'systemID': 'f0ad3066-ac16-4d59-b44b-5668711c3b4b',
    'flytTil': 'leder',
    'flytFra': 'saksbehandler',
    'flytMottattDato': '2022-01-19T09:00:01+0200',
    'flytSendtDato': '2022-01-19T09:00:01+0200',
    'flytStatus': 'Venter godkjenning',
    'flytMerknad': ''
}, {
    'systemID': '74c73e29-7e4e-4c8e-adf1-486e88830e00',
    'flytTil': 'leder',
    'flytFra': 'saksbehandler',
    'flytMottattDato': '2022-01-19T09:00:01+0200',
    'flytSendtDato': '2022-01-19T09:00:01+0200',
    'flytStatus': 'Venter godkjenning',
    'flytMerknad': ''
}
];

export const documentFlowCaseHandler = [{
    'systemID': '59e7ae81-80df-458d-a67d-512e1ad365a2',
    'flytTil': 'leder',
    'flytFra': 'saksbehandler',
    'flytMottattDato': '2022-01-19T09:00:01+0200',
    'flytSendtDato': '2022-01-19T09:00:01+0200',
    'flytStatus': 'Venter godkjenning',
    'flytMerknad': ''
}, {
    'systemID': '326564e5-7617-4292-aabc-ad664ada3c5e',
    'flytTil': 'leder',
    'flytFra': 'saksbehandler',
    'flytMottattDato': '2022-01-19T09:00:01+0200',
    'flytSendtDato': '2022-01-19T09:00:01+0200',
    'flytStatus': 'Venter godkjenning',
    'flytMerknad': ''
}, {
    'systemID': '58607234-145a-4ff3-b73d-b4df9384f1eb',
    'flytTil': 'leder',
    'flytFra': 'saksbehandler',
    'flytMottattDato': '2022-01-19T09:00:01+0200',
    'flytSendtDato': '2022-01-19T09:00:01+0200',
    'flytStatus': 'Venter godkjenning',
    'flytMerknad': ''
}, {
    'systemID': 'ea0ea9a3-1414-447d-bd3a-44d85d4e3eae',
    'flytTil': 'leder',
    'flytFra': 'saksbehandler',
    'flytMottattDato': '2022-01-19T09:00:01+0200',
    'flytSendtDato': '2022-01-19T09:00:01+0200',
    'flytStatus': 'Venter godkjenning',
    'flytMerknad': ''
}
];

export const recordTypeOptions = [
    {key: 'an', text: 'Arkivnotat', value: 'Arkivnotat'},
    {key: 'jp', text: 'Journalpost', value: 'Journalpost'}
];

export const correspondencePartTypeOptions = [
    {key: 'EA', text: 'Avsender', value: 'Avsender'},
    {key: 'EM', text: 'Mottaker', value: 'Mottaker'},
    {key: 'EK', text: 'Kopimottaker', value: 'Kopimottaker'},
    {key: 'GM', text: 'Gruppemottaker', value: 'Gruppemottaker'},
    {key: 'IA', text: 'Intern avsender', value: 'Intern avsender'},
    {key: 'IM', text: 'Intern mottaker', value: 'Intern mottaker'},
    {key: 'IK', text: 'Intern kopimottaker', value: 'Intern kopimottaker'},
    {key: 'IS', text: 'Medavsender', value: 'Medavsender'}
];

export const caseFileStatusOptionsRM = [
    {key: 'oaa', text: 'Opprettet av arkivtjenesten', value: 'Opprettet av arkivtjenesten'},
    {key: 'aaa', text: 'Avsluttet av arkivtjenesten', value: 'Avsluttet av arkivtjenesten'},
    {key: 'aa1', text: 'Avsluttet av leder', value: 'Avsluttet av leder'},
    {key: 'aas', text: 'Avsluttet av saksbehandler', value: 'Avsluttet av saksbehandler'},
    {key: 'ugr', text: 'Utgår', value: 'Utgår'}
];

export const caseFileStatusOptions = [
    {key: 'oaa', text: 'Opprettet av arkivtjenesten', value: 'Opprettet av arkivtjenesten'},
    {key: 'aaa', text: 'Avsluttet av arkivtjenesten', value: 'Avsluttet av arkivtjenesten'},
    {key: 'aa1', text: 'Avsluttet av leder', value: 'Avsluttet av leder'},
    {key: 'aas', text: 'Avsluttet av saksbehandler', value: 'Avsluttet av saksbehandler'},
    {key: 'ugr', text: 'Utgår', value: 'Utgår'}
];

export const caseFileStatusOptionsLeader = [
    {key: 'aa1', text: 'Avsluttet av leder', value: 'Avsluttet av leder'},
    {key: 'aas', text: 'Avsluttet av saksbehandler', value: 'Avsluttet av saksbehandler'}
];

export const caseFileStatusOptionsCH = [
    {key: 'oas', text: 'Opprettet av saksbehandler', value: 'Opprettet av saksbehandler'},
    {key: 'aas', text: 'Avsluttet av saksbehandler', value: 'Avsluttet av saksbehandler'}
];

export const registryEntryStatusOptions = [
    {'key': 'J', 'text': 'Journalført', value: 'Journalført'},
    {'key': 'F', 'text': 'Ferdigstilt fra saksbehandler', value: 'Ferdigstilt fra saksbehandler'},
    {'key': 'G', 'text': 'Godkjent av leder', value: 'Godkjent av leder'},
    {'key': 'E', 'text': 'Ekspedert', value: 'Ekspedert'},
    {'key': 'A', 'text': 'Arkivert', value: 'Arkivert'},
    {'key': 'U', 'text': 'Utgår', value: 'Utgår'},
    {'key': 'M', 'text': 'Midlertidig registrering av', value: 'Midlertidig registrering av'}
];

export const registryEntryTypeOptions = [
    {'key': 'I', 'text': 'Inngående dokument', 'value': 'I'},
    {'key': 'U', 'text': 'Utgående dokument', 'value': 'U'},
    {'key': 'N', 'text': 'Organinternt dokument for oppfølging', 'value': 'N'},
    {'key': 'X', 'text': 'Organinternt dokument uten oppfølging', 'value': 'X'},
    {'key': 'S', 'text': 'Saksframlegg', 'value': 'S'}
];

export const variantFormatOptions = [
    {key: 'P', text: 'Produksjonsformat', value: 'Produksjonsformat'},
    {key: 'A', text: 'Arkivformat', value: 'Arkivformat'},
    {
        key: 'O',
        text: 'Dokument hvor deler av innholdet er skjermet',
        value: 'Dokument hvor deler av innholdet er skjermet'
    }
];

export const ROLE_RECORDS_MANAGER = 'arkivar';
export const ROLE_ADMINISTRATOR = 'admin';
export const ROLE_LEADER = 'leder';
export const ROLE_CASE_HANDLER = 'saksbehandler';

export const RECORDS_MANAGER = 'arkivar';
export const ADMINISTRATOR = 'admin';
export const LEADER = 'leder';
export const CASE_HANDLER = 'saksbehandler';

export const CODE = 'kode';
export const CODE_NAME = 'kodenavn';
export const VARIANT_FORMAT = 'variantFormat';

export const roleOptions = [
    {'key': 'R', 'text': 'Arkivar', 'value': 'Arkivar'},
    {'key': 'A', 'text': 'Administrator', 'value': 'Administrator'},
    {'key': 'L', 'text': 'Leder', 'value': 'Leder'},
    {'key': 'C', 'text': 'Saksbehandler', 'value': 'Saksbehandler'}

];

export const associatedAsOptions = [
    {key: 'H', text: 'Hoveddokument', value: 'Hoveddokument'},
    {key: 'V', text: 'Vedlegg', value: 'Vedlegg'}
];

export const documentStatusOptions = [
    {key: 'B', text: 'Dokumentet er under redigering', value: 'Dokumentet er under redigering'},
    {key: 'F', text: 'Dokumentet er ferdigstilt', value: 'Dokumentet er ferdigstilt'}
];
