import React, {useContext} from 'react';
import {useNavigate} from "react-router-dom";
import {Dropdown, Menu} from 'semantic-ui-react'
import {ROLE_ADMINISTRATOR, ROLE_CASE_HANDLER, ROLE_LEADER, ROLE_RECORDS_MANAGER, roleOptions} from "./constants";
import AuthContext from "./components/store/AuthContext";

const NavigationBar = (props) => {

    const authContext = useContext(AuthContext);
    let navigate = useNavigate();

    const handleChangeRole = (event, {value}) => {
        console.log("You chose the following role : " + value);
        if (value.toLowerCase() === ROLE_RECORDS_MANAGER.toLowerCase()) {
            navigate('/arkivar');
        } else if (value.toLowerCase() === ROLE_ADMINISTRATOR.toLowerCase()) {
            navigate('/saksbehandler');
        } else if (value.toLowerCase() === ROLE_LEADER.toLowerCase()) {
            navigate('/leder');
        } else if (value.toLowerCase() === ROLE_CASE_HANDLER.toLowerCase()) {
            navigate('/saksbehandler');
        }
    };

    const doLogout = () => {
        authContext.logout();
        navigate('/login');
    }

    const doLogin = () => {
        navigate('/login');
    }

    return (
        <Menu borderless inverted fluid fixed="top">
            <Menu.Item as="a">
                <h3>{authContext.organisation}</h3>
            </Menu.Item>
            <Menu.Menu position="right">
                <Menu.Item as="a">
                    <Dropdown placeholder='Rolle' search selection options={roleOptions}
                              onChange={handleChangeRole}/></Menu.Item>
                {
                    authContext.isLoggedIn &&
                    <Menu.Item as="a"
                               onClick={doLogout}>Logg ut</Menu.Item>
                }
                {
                    !authContext.isLoggedIn &&
                    <Menu.Item as="a"
                               onClick={doLogin}>Logg inn</Menu.Item>
                }
            </Menu.Menu>
        </Menu>

    )
}

export default NavigationBar;
