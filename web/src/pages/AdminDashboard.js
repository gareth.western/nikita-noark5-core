import React from "react";

import "semantic-ui-css/semantic.min.css";
import {Divider, Grid, Header, Tab} from "semantic-ui-react";
import CaseFilePagination from "../components/casehandling/CaseFilePagination";
import CaseHandlerTaskFeed from "../components/feeds/CaseHandlerTaskFeed";
import {caseFileStatusOptions, documentFlowLeader} from "../constants";
import CaseHandlerLogFeed from "../components/feeds/CaseHandlerLogFeed";
import CaseFileListView from "../components/casehandling/CaseFileListView";


const AdminDashboard = () => {

    const leaderPanes = [{
        menuItem: "Saker til godkjenning",
        render: () =>
            <Tab.Pane>
                {getApprovalPaneContent()}
            </Tab.Pane>
    }, {
        menuItem: "Saker til fordeling",
        render: () =>
            <Tab.Pane>
                {getDistributionPaneContent()}
            </Tab.Pane>
    }];

    const getApprovalPaneContent = () => {
        return (
            <CaseFileListView role={'admin'} showNewCaseFileButton={true} caseFileStatusOptions={caseFileStatusOptions}
                              caseFiles={getApprovalCaseFiles()}/>
        );
    }

    const getApprovalCaseFiles = () => {
        return [];
    }

    const getDistributionCaseFiles = () => {
        return [];
    }

    const getDistributionPaneContent = () => {
        return (
            <CaseFileListView role={'admin'} showNewCaseFileButton={true} caseFileStatusOptions={caseFileStatusOptions}
                              caseFiles={getDistributionCaseFiles()}/>
        );
    }

    const defaultActivePage = 0;
    const totalPages = 3;

    const [open, setOpen] = React.useState(false);

    return (
        <Grid>
            <Grid.Row columns={2}>
                <Grid.Column width={13}>
                    <Grid padded>
                        <Grid.Row>
                            <Grid.Column textAlign='left'>
                                <Header dividing size="large" as="h1">
                                    Lederoppgaver
                                </Header>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Tab menu={{pointing: true}} panes={leaderPanes}/>
                        </Grid.Row>
                        <Grid.Row>
                            <CaseFilePagination defaultActivePage={defaultActivePage}
                                                totalPages={totalPages}/>
                        </Grid.Row>
                    </Grid>
                </Grid.Column>
                <Grid.Column width={3}>
                    <CaseHandlerTaskFeed documentFlowCaseHandler={documentFlowLeader}/>
                    <Divider section/>
                    <CaseHandlerLogFeed/>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

export default AdminDashboard;
