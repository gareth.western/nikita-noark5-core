import React, {useContext, useEffect, useState} from "react";

import "semantic-ui-css/semantic.min.css";

import {Button, Grid, Header, Icon, Menu, Message, Modal, Pagination, Popup, Tab} from "semantic-ui-react";
import CaseFileListView from "../components/casehandling/CaseFileListView";
import {CASE_FILE_REL, caseFileStatusOptions, FILE_REL, LINKS} from "../constants";
import FileListView from "../components/recordsmanager/FileListView";
import AuthContext from "../components/store/AuthContext";
import NewCaseFile from "../components/casehandling/NewCaseFile";
import AppContext from "../components/store/AppContext";
import axios from "axios";
import CaseFileDistributionListView from "../components/casehandling/CaseFileDistributionListView";

const RecordsManagerDashboard = () => {

    const auth = useContext(AuthContext);
    const app = useContext(AppContext);

    // Filter to pick out all file objects that come from incoming email system
    const [urlPostFiles, setUrlPostFiles] = useState(app.postalSeries[LINKS][FILE_REL].href + '?%24orderby=opprettetDato desc');
    // Filter to pick out all casefiles that require need to be finalised by the record keepers
    const odataApproval = '?%24filter' + encodeURIComponent("=saksstatus/kodenavn eq 'Avsluttet av leder'");
    const [urlApprovalCaseFiles, setUrlApprovalCaseFiles] = useState(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + odataApproval);
    // Filter to pick out all casefiles that need to beassigned to a case handler
    const odataDistribution = '?%24filter' + encodeURIComponent("=saksansvarlig eq 'unassigned'");
    const [urlDistributionCaseFiles, setUrlDistributionCaseFiles] = useState(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + odataDistribution);
    // Filter for all casefiles
    const [urlAllCaseFiles, setUrlAllCaseFiles] = useState(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + '?%24orderby=opprettetDato desc');

    // Pagination data
    const [postFilesTotalPages, setPostFilesTotalPages] = useState(0);
    const [approvalCaseFilesTotalPages, setApprovalCaseFilesTotalPages] = useState(0);
    const [distributionCaseFilesTotalPages, setDistributionCaseFilesTotalPages] = useState(0);
    const [allCaseFilesTotalPages, setAllCaseFilesTotalPages] = useState(0);

    // Objects containing list of files/casesfiles
    const [postFiles, setPostFiles] = useState([]);
    const [approvalCaseFiles, setApprovalCaseFiles] = useState([]);
    const [allCaseFiles, setAllCaseFiles] = useState([]);
    const [distributionCaseFiles, setDistributionCaseFiles] = useState([]);
    const [openNewCaseFileModal, setOpenNewCaseFileModal] = useState(false);

    // Display error message at bottom of screen
    const [error, setError] = useState(null);

    const urlFilesSearchHandler = (searchString) => {
        if (searchString.length > 0) {
            let odata = encodeURIComponent("=contains(tittel, '" + searchString +
                "') or contains(beskrivelse, '" + searchString + "')");
            setUrlPostFiles(app.postalSeries[LINKS][FILE_REL].href + '?%24filter' + odata);
        } else {
            setUrlPostFiles(app.postalSeries[LINKS][FILE_REL].href + '?%24orderby=opprettetDato desc');
        }
    }

    const urlAllCaseFilesSearchHandler = (searchString) => {
        if (searchString.length > 0) {
            let odata = encodeURIComponent("=contains(tittel, '" + searchString +
                "') or contains(beskrivelse, '" + searchString + "')");
            setUrlAllCaseFiles(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + '?%24filter' + odata);
        } else {
            setUrlAllCaseFiles(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + '?%24orderby=mappeID asc');
        }
        // Get all casefile objects
        get(urlAllCaseFiles, setAllCaseFiles, setAllCaseFilesTotalPages);
    }

    const urlApprovalCaseFilesSearchHandler = (searchString) => {
        if (searchString.length > 0) {
            let odata = encodeURIComponent(" and contains(tittel, '" + searchString +
                "') or contains(beskrivelse, '" + searchString + "')");
            setUrlApprovalCaseFiles(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + odataApproval + odata);
        } else {
            setUrlApprovalCaseFiles(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + odataApproval);
        }
        // Get all approval casefile objects
        get(urlApprovalCaseFiles, setUrlApprovalCaseFiles, setApprovalCaseFilesTotalPages);
    }

    const urlDistributionCaseFilesSearchHandler = (searchString) => {
        if (searchString.length > 0) {
            let odata = encodeURIComponent(" and contains(tittel, '" + searchString +
                "') or contains(beskrivelse, '" + searchString + "')");
            setUrlDistributionCaseFiles(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + odataDistribution + odata);
        } else {
            setUrlDistributionCaseFiles(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + odataDistribution);
        }
        // Get all distribution casefile objects
        get(urlDistributionCaseFiles, setDistributionCaseFiles, setDistributionCaseFilesTotalPages);
    }

    // Close the new casefile modal
    const closeNewCaseFileModal = () => {
        setOpenNewCaseFileModal(false);
    }

    // Update the tabular lists. Currently, the approach is just to force a rerender by setting a value that will cause
    // a rerender. This causes 4 requests to the server, which is not ideal. Later, we should look at dividing this up a
    // little better
    const updateCaseFileList = () => {
        let old = urlPostFiles;
        setUrlPostFiles('');
        setUrlPostFiles(old);
    }

    // Create an axios instance for use by all requests
    const nikita = axios.create({
        headers: {
            'Accept': 'application/vnd.noark5+json',
            'Authorization': auth.token
        }
    });

    // Set the applicable object array and set error to null if everything is OK
    const handleResponse = (results, setFiles) => {
        if (results !== undefined) {
            setFiles(results);
        } else {
            setFiles([]);
        }
        setError(null);
    }

    const handlePaginationChangeAll = (e, pageInfo) => {
        handlePagination(urlAllCaseFiles, setUrlAllCaseFiles, ((pageInfo.activePage - 1) * app.resultsPerPage));
    }

    const handlePaginationChangeApproval = (e, pageInfo) => {
        handlePagination(urlApprovalCaseFiles, setUrlApprovalCaseFiles, ((pageInfo.activePage - 1) * app.resultsPerPage));
    }

    const handlePaginationChangeDistribution = (e, pageInfo) => {
        handlePagination(urlDistributionCaseFiles, setUrlDistributionCaseFiles, ((pageInfo.activePage - 1) * app.resultsPerPage));
    }

    const handlePaginationChangePostFiles = (e, pageInfo) => {
        handlePagination(urlPostFiles, setUrlPostFiles, ((pageInfo.activePage - 1) * app.resultsPerPage));
    }

    // Handle a pagination. Convert the page number to the skip value and update the corresponding url with its setUrl
    // method
    const handlePagination = (url, setMethod, skipCount) => {
        if (url.includes("%24skip")) {
            setMethod(url.replace(/24skip=\d+/g, '24skip=' + skipCount));
        } else {
            if (url.includes('mappe?') || url.includes('mappe/?')) {
                setMethod(url + "&%24skip=" + skipCount);
            } else {
                setMethod(url + "?%24skip=" + skipCount);
            }
        }
    }

    // Generic get list method
    const get = (url, setMethod, setTotalPages) => {
        nikita.get(url)
            .then((response) => {
                let tp = response.data.count / app.resultsPerPage;
                let val = Math.ceil(tp);
                setTotalPages(val);
                handleResponse(response.data.results, setMethod);
            }).catch(error => {
            setError(error);
        });
    }

    // Retrieve all data used in the various overview panes
    useEffect(() => {
        // Get file objects for postmottak
        get(urlPostFiles, setPostFiles, setPostFilesTotalPages);
        // Get casefile objects for approval
        get(urlApprovalCaseFiles, setApprovalCaseFiles, setApprovalCaseFilesTotalPages);
        // Get casefile objects for distribution
        get(urlDistributionCaseFiles, setDistributionCaseFiles, setDistributionCaseFilesTotalPages);
        // Get all casefile objects
        get(urlAllCaseFiles, setAllCaseFiles, setAllCaseFilesTotalPages);
    }, [urlAllCaseFiles, urlDistributionCaseFiles, urlApprovalCaseFiles, urlPostFiles])


    // Make all content for the various tab panes
    const recordsManagerPanes = [{
        menuItem: "Postmottak",
        render: () =>
            <Tab.Pane>
                {getMailPaneContent()}
            </Tab.Pane>
    }, {
        menuItem: "Saker til godkjenning",
        render: () =>
            <Tab.Pane>
                {getApprovalPaneContent()}
            </Tab.Pane>
    }, {
        menuItem: "Saker til fordeling",
        render: () =>
            <Tab.Pane>
                {getDistributionPaneContent()}
            </Tab.Pane>
    }, {
        menuItem: "Alle saker ",
        render: () =>
            <Tab.Pane>
                {getAllCasefilesPaneContent()}
            </Tab.Pane>
    }, {
        menuItem: (
            <Menu.Item key='messages'>
                <Popup
                    trigger={
                        <Modal
                            closeIcon
                            open={openNewCaseFileModal}
                            trigger={<Button icon>
                                <Icon name='plus'/>
                                Ny saksmappe
                            </Button>}
                            onClose={() => setOpenNewCaseFileModal(false)}
                            onOpen={() => setOpenNewCaseFileModal(true)}
                        >
                            <Header icon='folder open outline' content='Opprett Ny Saksmappe'/>
                            <Modal.Content>
                                <NewCaseFile closeModal={closeNewCaseFileModal} updateCaseFileList={updateCaseFileList}
                                             responsible={'unassigned'}/>
                            </Modal.Content>
                            <Modal.Actions>
                                <Button onClick={() => setOpenNewCaseFileModal(false)}>
                                    <Icon name='remove'/> Avbryt
                                </Button>
                            </Modal.Actions>
                        </Modal>
                    }
                    content='Opprett Ny Saksmappe'
                    size='small'
                />
            </Menu.Item>
        ),
        render: () =>
            <Tab.Pane>

            </Tab.Pane>
    }];

    const getApprovalPaneContent = () => {
        return (
            <div>
                <CaseFileListView role={'recordsmanager'}
                                  showNewCaseFileButton={false}
                                  caseFileStatusOptions={caseFileStatusOptions}
                                  caseFiles={approvalCaseFiles}
                                  doSearch={urlApprovalCaseFilesSearchHandler}/>
                {approvalCaseFilesTotalPages > 1 &&
                    <Pagination key={'approvalCaseFiles'} defaultActivePage={0}
                                totalPages={approvalCaseFilesTotalPages}
                                onPageChange={handlePaginationChangeApproval}/>
                }
            </div>
        );
    }

    const getMailPaneContent = () => {
        return (
            <div>
                <FileListView files={postFiles}
                              doSearch={urlFilesSearchHandler}
                              updateCaseFileList={updateCaseFileList}/>
                {postFilesTotalPages > 1 &&

                    <Pagination key={'postFiles'} defaultActivePage={0}
                                totalPages={postFilesTotalPages}
                                onPageChange={handlePaginationChangePostFiles}/>}
            </div>
        );
    }

    const getDistributionPaneContent = () => {
        return (
            <div>
                <CaseFileDistributionListView caseFileStatusOptions={caseFileStatusOptions}
                                              caseFiles={distributionCaseFiles}
                                              doSearch={urlDistributionCaseFilesSearchHandler}
                                              updateCaseFileList={updateCaseFileList}/>
                {distributionCaseFilesTotalPages > 1 &&
                    <Pagination key={'distributionCaseFiles'} defaultActivePage={0}
                                totalPages={distributionCaseFilesTotalPages}
                                onPageChange={handlePaginationChangeDistribution}/>}
            </div>
        );
    }

    const getAllCasefilesPaneContent = () => {
        return (
            <div>
                <CaseFileListView role={'recordsmanager'} showNewCaseFileButton={false}
                                  caseFileStatusOptions={caseFileStatusOptions} caseFiles={allCaseFiles}
                                  doSearch={urlAllCaseFilesSearchHandler}/>
                {allCaseFilesTotalPages > 1 &&
                    <Pagination key={'allCaseFile'} defaultActivePage={0}
                                totalPages={allCaseFilesTotalPages}
                                onPageChange={handlePaginationChangeAll}/>}
            </div>
        );
    }

    return (
        <Grid>
            <Grid.Row>
                <Grid.Column>
                    <Grid padded>
                        <Grid.Row>
                            <Grid.Column textAlign='left'>
                                <Header dividing size="large" as="h1">
                                    Arkivar
                                </Header>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Tab menu={{pointing: true}} panes={recordsManagerPanes}/>
                        </Grid.Row>
                    </Grid>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                {null !== error &&
                    <Message negative>
                        <Message.Header>Problemer med å hente innhold</Message.Header>
                        <p>Feilmelding: {error.message}</p>
                    </Message>}
            </Grid.Row>
        </Grid>
    )
}

export default RecordsManagerDashboard;
