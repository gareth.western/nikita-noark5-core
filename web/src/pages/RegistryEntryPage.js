import React, {useContext, useEffect, useState} from 'react';
import {
    Button,
    Container,
    Divider,
    Form,
    Grid,
    Header,
    Icon,
    Input,
    Modal,
    Popup,
    Segment,
    Table
} from 'semantic-ui-react';
import {useLocation} from "react-router-dom";
import DocumentDescriptionList from "../components/casehandling/document/DocumentDescriptionList";
import {
    CONTENT_TYPE_NOARK,
    DOCUMENT_DESCRIPTION_REL,
    LINKS,
    registryEntryStatusOptions,
    REL_CORRESPONDENCE_PART,
    SELF
} from "../constants";
import AuthContext from "../components/store/AuthContext";
import CorrespondencePartPage from "./CorrespondencePartPage";
import NewCorrespondencePartPage from "./NewCorrespondencePartPage";

const RegistryEntryPage = (props) => {

    const auth = useContext(AuthContext);
    const location = useLocation();
    const [eTag, setETag] = useState('');
    const [correspondencePartModal, setCorrespondencePartModal] = useState(false);
    const [newCorrespondencePartModal, setNewCorrespondencePartModal] = useState(false);
    const [registryEntry, setRegistryEntry] = useState(location.state.registryEntry);
    const [title, setTitle] = useState(registryEntry.tittel);
    const [publicTitle, setPublicTitle] = useState(registryEntry.offentligTittel);
    const [description, setDescription] = useState(registryEntry.beskrivelse);
    const [registrationNumber] = useState(registryEntry.registreringsID);
    const [registryEntryDate, setRegistryEntryDate] = useState(registryEntry.journaldato);
    const [registryEntryStatusCode, setRegistryEntryStatusCode] = useState(registryEntry.journalstatus.kode)
    const [registryEntryStatusCodeName, setRegistryEntryStatusCodeName] = useState(registryEntry.journalstatus.kodenavn)
    const [documentDescriptions, setDocumentDescriptions] = useState([]);
    const [documentDescriptionsUrl, setDocumentDescriptionsUrl] = useState(registryEntry[LINKS][DOCUMENT_DESCRIPTION_REL].href + '?%24orderby=opprettetDato asc');

    const [correspondenceParts, setCorrespondenceParts] = useState([]);
    const [chosenCorrespondencePart, setChosenCorrespondencePart] = useState('');
    const [correspondencePartsUrl, setCorrespondencePartsUrl] = useState(registryEntry[LINKS][REL_CORRESPONDENCE_PART].href + '?%24orderby=opprettetDato asc');


    useEffect(() => {

        fetch(registryEntry[LINKS][SELF].href, {
            headers: {
                'Accept': 'application/vnd.noark5+json',
                'Authorization': auth.token
            }
        })
            .then(response => {
                for (let pair of response.headers.entries()) {
                    if (pair[0].toLowerCase() === 'etag') {
                        setETag(pair[1]);
                    }
                }
                return response.json();
            }).then(data => {
            setRegistryEntry(data);
        })
            .catch(response => {
                console.log(response);
            })

        getDocumentDescriptions();
        getCorrespondenceParts();
    }, [documentDescriptionsUrl]);

    const getDocumentDescriptions = () => {
        fetch(documentDescriptionsUrl, {
            headers: {
                'Accept': 'application/vnd.noark5+json',
                'Authorization': auth.token
            }
        })
            .then(response => {
                return response.json();
            }).then(data => {
            if (data.results !== undefined) {
                setDocumentDescriptions(data.results);
            } else {
                setDocumentDescriptions([]);
            }
        })
            .catch(response => {
                console.log(response);
            })
    }

    const getCorrespondenceParts = () => {

        fetch(correspondencePartsUrl, {
            headers: {
                'Accept': 'application/vnd.noark5+json',
                'Authorization': auth.token
            }
        })
            .then(response => {
                return response.json();
            }).then(data => {
            if (data.results !== undefined) {
                setCorrespondenceParts(data.results);
            } else {
                setCorrespondenceParts([]);
            }
        })
            .catch(response => {
                console.log(response);
            })
    }

    function HandleUpdate() {

        registryEntry.tittel = title;
        registryEntry.offentligTittel = publicTitle;
        registryEntry.beskrivelse = description;
        registryEntry.journalstatus.kode = registryEntryStatusCode;
        registryEntry.journalstatus.kodenavn = registryEntryStatusCodeName;

        const updateRequestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': CONTENT_TYPE_NOARK,
                'Authorization': auth.token,
                'ETAG': eTag
            },
            body: JSON.stringify(registryEntry)
        };
        fetch(registryEntry[LINKS][SELF].href, updateRequestOptions)
            .then(response => {
                for (let pair of response.headers.entries()) {
                    if (pair[0].toLowerCase() === 'etag') {
                        setETag(pair[1]);
                    }
                }
                return response.json();
            }).then(data => {
            setRegistryEntry(data);
        })
            .catch(response => {
                console.log(response);
            })
    }

    const registryEntryStatusChosen = (event, data) => {
        const {value} = data;
        const {key} = data.options.find(o => o.value === value);
        setRegistryEntryStatusCode(key);
        setRegistryEntryStatusCodeName(value);
    }

    const correspondencePartClicked = (e, correspondencePart) => {
        console.log("clicked " + JSON.stringify(correspondencePart));
        setChosenCorrespondencePart(correspondencePart);
    }

    const closeCorrespondencePartModal = () => {
        console.log("clicked close closeCorrespondencePartModal");
        setCorrespondencePartModal(false);
    }

    const closeNewCorrespondencePartModal = () => {
        setNewCorrespondencePartModal(false);
    }

    const renderCorrespondencePartsItems = () => {
        return (
            correspondenceParts.map(
                (correspondencePart) => (
                    <Popup
                        key={correspondencePart.systemID}
                        trigger={
                            <Modal
                                key={correspondencePart.systemID}
                                closeIcon
                                open={correspondencePartModal}
                                trigger={
                                    <Table.Row onClick={(e) => correspondencePartClicked(e, correspondencePart)}>
                                        <Table.Cell>{correspondencePart.navn}</Table.Cell>
                                        <Table.Cell>{correspondencePart.korrespondanseparttype.kodenavn}</Table.Cell>
                                    </Table.Row>
                                }
                                onClose={() => setCorrespondencePartModal(false)}
                                onOpen={() => setCorrespondencePartModal(true)}
                            >
                                <Header icon='folder open outline' content='Korrespondansepart'/>
                                <Modal.Content>
                                    <CorrespondencePartPage
                                        key={correspondencePart.systemID}
                                        cpart={chosenCorrespondencePart}
                                        closeModal={closeCorrespondencePartModal}/>
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button onClick={() => setCorrespondencePartModal(false)}>
                                        <Icon name='remove'/> Avbryt
                                    </Button>
                                </Modal.Actions>
                            </Modal>
                        }
                        content='Opprett Ny Saksmappe'
                        size='small'
                    />
                )
            )
        );
    }

    function doFilterSearch(searchString) {
        if (searchString.length > 0) {
            let odata = encodeURIComponent("=contains(tittel, '" + searchString + "') or contains(beskrivelse, '" + searchString + "')");
            setDocumentDescriptionsUrl(registryEntry[LINKS][DOCUMENT_DESCRIPTION_REL].href + '?%24filter' + odata);
        } else {
            setDocumentDescriptionsUrl(registryEntry[LINKS][DOCUMENT_DESCRIPTION_REL].href + '?%24orderby=opprettetDato asc');
        }
        getDocumentDescriptions();
    }

    return (
        <div>
            <Form>
                <Form.Group widths='equal'>
                    <Form.Input
                        fluid
                        label='Jpnr'
                        placeholder='Jpnr'
                        value={registrationNumber}/>
                    <Form.Input
                        fluid
                        label='Journaldato'
                        placeholder='Journaldato'
                        value={registryEntryDate}/>
                    <Form.Select
                        fluid
                        label='Journalstatus'
                        options={registryEntryStatusOptions}
                        onChange={registryEntryStatusChosen}
                        key={registryEntryStatusCode}
                        text={registryEntryStatusCodeName}
                        value={registryEntry.journalstatus}
                    />

                    <Form.Input fluid label='Klasse (Primær)' placeholder='KlassePrimaer'/>
                    <Form.Input fluid label='Klasse (Sekundær)' placeholder='KlasseSekundaer'/>
                </Form.Group>
                <Form.Input fluid label='Tittel' placeholder='Tittel'
                            value={title} onChange={(e) => setTitle(e.target.value)}/>
                <Form.Input fluid label='Offentlig Tittel' placeholder='offentligTittel'
                            value={publicTitle} onChange={(e) => setPublicTitle(e.target.value)}/>
                <Form.Group>
                    <Form.TextArea fluid label='Besrkivelse' placeholder='beskrivelse'
                                   value={description} onChange={(e) => setDescription(e.target.value)}/>
                </Form.Group>
                <Form.Button
                    onClick={HandleUpdate}>Oppdater</Form.Button>
            </Form>
            <Grid padded>
                <Grid.Row>
                    <Header dividing size="large" as="h1">
                        Korrespondanseparter
                    </Header>
                </Grid.Row>
                <Grid.Row>
                    <Segment>
                        <Container textAlign='left'>
                            <Popup
                                trigger={
                                    <Modal
                                        closeIcon
                                        open={newCorrespondencePartModal}
                                        trigger={<Button icon labelPosition='left' floated='right'>
                                            <Icon name='plus'/>
                                            Legg til nytt korrespondansepart
                                        </Button>}
                                        onClose={() => setNewCorrespondencePartModal(false)}
                                        onOpen={() => setNewCorrespondencePartModal(true)}
                                    >
                                        <Header icon='plus' content='Opprett korrespondansepart'/>
                                        <Modal.Content>
                                            <NewCorrespondencePartPage
                                                registryEntry={registryEntry}
                                                closeModal={closeNewCorrespondencePartModal}/>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <Button onClick={() => setNewCorrespondencePartModal(false)}>
                                                <Icon name='remove'/> Avbryt
                                            </Button>
                                        </Modal.Actions>
                                    </Modal>
                                }
                                content='Opprett korrespondansepart'
                                size='small'
                            />
                            <Header as='h3'>&nbsp;</Header>
                            <Table>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell>Navn</Table.HeaderCell>
                                        <Table.HeaderCell>Type</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {renderCorrespondencePartsItems()}
                                </Table.Body>
                            </Table>
                        </Container>
                    </Segment>
                </Grid.Row>
                <Grid.Row>
                    <Divider/>
                </Grid.Row>
            </Grid>
            <Grid padded>
                <Grid.Row>
                    <Header dividing size="large" as="h1">
                        Dokumenter
                    </Header>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={1}>
                        <i className="filter large icon"/>
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Input
                            placeholder="Søk..."
                            size="small"
                            onChange={(e) => doFilterSearch(e.target.value)}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <DocumentDescriptionList
                        key={documentDescriptions}
                        registryEntry={registryEntry}
                        documentDescriptions={documentDescriptions}
                        doFilterSearch={doFilterSearch}/>
                </Grid.Row>
                <Grid.Row>
                </Grid.Row>
            </Grid>
        </div>
    )
}
export default RegistryEntryPage;
