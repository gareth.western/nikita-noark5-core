import React, {useContext, useEffect, useState} from 'react';
import {Button, Form, Grid, Header, Icon, Input, Modal, Popup} from 'semantic-ui-react';
import {useLocation} from "react-router-dom";
import CaseFilePagination from "../components/casehandling/CaseFilePagination";
import RegistryEntryList from "../components/casehandling/registry_entry/RegistryEntryList";
import {
    caseFileStatusOptionsCH,
    caseFileStatusOptionsLeader,
    caseFileStatusOptionsRM,
    CONTENT_TYPE_NOARK,
    LINKS,
    REGISTRY_ENTRY_FILE_REL,
    SELF
} from "../constants";
import NewRegistryEntry from "../components/casehandling/registry_entry/NewRegistryEntry";
import AuthContext from "../components/store/AuthContext";

const defaultActivePage = 0;
const totalPages = 3;

const CaseFilePage = () => {

    const location = useLocation();
    const auth = useContext(AuthContext);

    // Values for binding
    const [caseFile, setCaseFile] = useState(location.state.caseFile);
    const [role] = useState(location.state.role)
    const [eTag, setETag] = useState('');
    const [title, setTitle] = useState(caseFile.tittel);
    const [publicTitle, setPublicTitle] = useState(caseFile.offentligTittel);
    const [description, setDescription] = useState(caseFile.beskrivelse);
    const [caseNumber] = useState(caseFile.mappeID);
    const [caseDate, setCaseNumber] = useState(caseFile.saksdato);
    const [caseFileStatusCode, setCaseFileStatusCode] = useState(caseFile.saksstatus.kode);
    const [caseFileStatusCodeName, setCaseFileStatusCodeName] = useState(caseFile.saksstatus.kodenavn);

    const [open, setOpen] = useState(false);
    const [registryEntries, setRegistryEntries] = useState([]);
    const [registryEntriesUrl, setRegistryEntriesUrl] = useState(caseFile[LINKS][REGISTRY_ENTRY_FILE_REL].href);
    const token = auth.token;


    let caseStatusOptions = caseFileStatusOptionsCH;

    if (role === 'leader') {
        caseStatusOptions = caseFileStatusOptionsLeader;
    } else if (role === 'recordsmanager') {
        caseStatusOptions = caseFileStatusOptionsRM;
    }

    const closeModal = () => {
        setOpen(false);
    }

    useEffect(() => {
        // Download the latest version of the casefile, so we have an ETAG, as well as any updated values
        fetch(caseFile[LINKS][SELF].href, {
            headers: {
                'Accept': 'application/vnd.noark5+json',
                'Authorization': token
            }
        })
            .then(response => {
                for (let pair of response.headers.entries()) {
                    if (pair[0].toLowerCase() === 'etag') {
                        setETag(pair[1]);
                    }
                }
                return response.json();
            }).then(data => {
            setCaseFile(data);
        })
            .catch(response => {
                console.log(response);
            })
        // Next download all registryEntries associated with the casefile
        getRegistryEntries();
    }, [])

    const getRegistryEntries = () => {

        fetch(registryEntriesUrl, {
            headers: {
                'Accept': 'application/vnd.noark5+json',
                'Authorization': token
            }
        })
            .then(response => {
                return response.json();
            }).then(data => {
            setRegistryEntries(data.results);
        })
            .catch(response => {
                console.log(response);
            })
    }

    function HandleUpdate() {

        caseFile.tittel = title;
        caseFile.offentligTittel = publicTitle;
        caseFile.beskrivelse = description;
        caseFile.saksstatus.kode = caseFileStatusCode;
        caseFile.saksstatus.kodenavn = caseFileStatusCodeName;

        const updateRequestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': CONTENT_TYPE_NOARK,
                'Authorization': auth.token,
                'ETAG': eTag
            },
            body: JSON.stringify(caseFile)
        };
        fetch(caseFile[LINKS][SELF].href, updateRequestOptions)
            .then(response => {
                for (let pair of response.headers.entries()) {
                    if (pair[0].toLowerCase() === 'etag') {
                        setETag(pair[1]);
                    }
                }
                return response.json();
            }).then(data => {
            setCaseFile(data);
        })
            .catch(response => {
                console.log(response);
            })
    }

    const caseFileStatusChosen = (event, data) => {
        const {value} = data;
        const {key} = data.options.find(o => o.value === value);
        setCaseFileStatusCode(key);
        setCaseFileStatusCodeName(value);
    }

    function doFilterSearch(searchString) {
        if (searchString.length > 0) {
            let odata = encodeURIComponent("=contains(tittel, '" + searchString + "') or contains(beskrivelse, '" + searchString + "')");
            setRegistryEntriesUrl(caseFile[LINKS][REGISTRY_ENTRY_FILE_REL].href + '?%24filter' + odata);
        } else {
            setRegistryEntriesUrl(caseFile[LINKS][REGISTRY_ENTRY_FILE_REL].href + '?%24orderby=registreringID asc');
        }
        getRegistryEntries();
    }

    const addRegistryEntry = (registryEntry) => {
        // We should make it work by just adding the registryEntry
        // but currently I am not getting a refresh so forcing a re-retrieval of list
        getRegistryEntries();
    }

    return (
        <div>
            <Form>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Saksnr' placeholder='Saksnr'
                                value={caseNumber}/>
                    <Form.Input fluid label='Saksdato' placeholder='Saksdato'
                                value={caseDate}/>
                    <Form.Select
                        fluid
                        label='saksstatus'
                        options={caseStatusOptions}
                        onChange={caseFileStatusChosen}
                        key={caseFileStatusCode}
                        text={caseFileStatusCodeName}
                        value={caseFile.saksstatus}
                    />
                    <Form.Input fluid label='Klasse (Primær)' placeholder='KlassePrimaer'/>
                    <Form.Input fluid label='Klasse (Sekundær)' placeholder='KlasseSekundaer'/>
                </Form.Group>
                <Form.Input fluid label='Tittel' placeholder='Tittel'
                            value={title} onChange={(e) => setTitle(e.target.value)}/>
                <Form.Input fluid label='Offentlig Tittel' placeholder='offentligTittel'
                            value={publicTitle} onChange={(e) => setPublicTitle(e.target.value)}/>
                <Form.Group widths='equal'>
                    <Form.TextArea label='Besrkivelse' placeholder='beskrivelse'
                                   value={description} onChange={(e) => setDescription(e.target.value)}/>

                    <Form.Input fluid label='Merknader' placeholder='Merknader'/>
                </Form.Group>
                <Form.Button
                    onClick={HandleUpdate}>Oppdater</Form.Button>
            </Form>
            <Grid padded>
                <Grid.Row>
                    <Header dividing size="large" as="h1">
                        Registreringer
                    </Header>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={1}>
                        <i className="filter large icon"/>
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Input
                            placeholder="Søk..."
                            size="small"
                            onChange={(e) => doFilterSearch(e.target.value)}/>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <Popup
                            trigger={
                                <Modal
                                    closeIcon
                                    open={open}
                                    trigger={<Button icon>
                                        <Icon name='plus'/>
                                        Ny Registrering
                                    </Button>}
                                    onClose={() => setOpen(false)}
                                    onOpen={() => setOpen(true)}
                                >
                                    <Header icon='envelope open outline' content='Registrer dokument'/>
                                    <Modal.Content>
                                        <NewRegistryEntry addRegistryEntry={addRegistryEntry}
                                                          caseFile={caseFile}
                                                          closeModal={closeModal}/>
                                    </Modal.Content>
                                    <Modal.Actions>
                                        <Button onClick={() => setOpen(false)}>
                                            <Icon name='remove'/> Avbryt
                                        </Button>
                                    </Modal.Actions>
                                </Modal>
                            }
                            content='Opprett Ny Saksmappe'
                            size='small'
                        />

                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>x
                    <RegistryEntryList registryEntries={registryEntries}/>
                </Grid.Row>
                <Grid.Row>
                    <CaseFilePagination defaultActivePage={defaultActivePage}
                                        totalPages={totalPages}/>
                </Grid.Row>
            </Grid>
        </div>
    )
}
export default CaseFilePage;
