import React, {useContext, useState} from 'react'
import {Button, Form, Grid, Header, Message, Segment} from 'semantic-ui-react'
import {useNavigate} from "react-router-dom";
import {
    CASE_HANDLING_REL,
    CONTENT_TYPE_NOARK,
    FONDS_CREATOR_REL,
    FONDS_REL,
    FONDS_STRUCTURE_REL,
    LINKS,
    NEW_SERIES_REL,
    roleOptions,
    SERIES_REL
} from "../constants";
import AuthContext from "../components/store/AuthContext";
import AppContext from "../components/store/AppContext";
import axios from "axios";

const LoginPage = () => {

    const authContext = useContext(AuthContext);
    const appContext = useContext(AppContext);
    const apiUrl = "https://nikita.oslomet.no/noark5v5/";
    //const apiUrl = "http://localhost:8092/noark5v5/";
    const loginBasicToken = 'Basic bmlraXRhLWNsaWVudDpzZWNyZXQ=';
    const clientId = 'nikita-client';
    const [error, setError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [roleCode, setRoleCode] = useState('S');
    const [roleCodeName, setRoleCodeName] = useState('Saksbehandler');
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const role = {
        key: '',
        value: ''
    };

    const roleChosen = (event, data) => {
        const {value} = data;
        const {key} = data.options.find(o => o.value === value);
        setRoleCode(key);
        setRoleCodeName(value);
    }

    const navigate = useNavigate();

    const doLogin = async (e) => {
        e.preventDefault();

        const loginUrl = apiUrl + 'oauth/token?grant_type=password&client_id=' + clientId + '&username=' + username +
            '&password=' + encodeURIComponent(password);

        let loginConfig = {
            headers: {
                'Accept': CONTENT_TYPE_NOARK,
                'Authorization': loginBasicToken
            }
        };

        axios.post(loginUrl, '', loginConfig)
            .then((response) => {
                const tokenString = response.data.token_type + ' ' + response.data.access_token;
                const expirationTime = new Date((new Date().getTime() + (+response.data.expires_in * 1000)));
                authContext.login(username, tokenString, expirationTime);
                const getConfig = {
                    headers: {
                        'Content-Type': CONTENT_TYPE_NOARK,
                        'Authorization': tokenString
                    }
                };
                axios.get(apiUrl, getConfig)
                    .then((response) => {
                        const urlCaseHandlingLinks = response.data[LINKS][CASE_HANDLING_REL].href;
                        const urlFondsStructureLinks = response.data[LINKS][FONDS_STRUCTURE_REL].href;
                        // Get endpoint for casehandling
                        axios.get(urlCaseHandlingLinks, getConfig)
                            .then((response) => {
                                const caseHandlingLinks = response.data;
                                appContext.setCaseHandlingLinks(caseHandlingLinks);
                            }).catch(error => {
                            setErrorMessage("Error when accessing application root " + error.message + ' ' +
                                urlCaseHandlingLinks);
                            console.log(error.message);
                        });

                        // Get endpoint for top of fonds structure
                        axios.get(urlFondsStructureLinks, getConfig)
                            .then((response) => {
                                const fondsStructure = response.data;
                                appContext.setFondsLinks(fondsStructure);

                                // First get the default fonds object
                                const urlFonds = fondsStructure[LINKS][FONDS_REL].href;
                                axios.get(urlFonds, getConfig)
                                    .then((response) => {
                                        const fonds = response.data.results[0];
                                        const urlFondsCreator = fonds[LINKS][FONDS_CREATOR_REL].href;
                                        axios.get(urlFondsCreator, getConfig)
                                            .then((response) => {
                                                if (response.data.results !== undefined) {
                                                    const fondsCreator = response.data.results[0];
                                                    const orgName = 'nikita-postmottak+' + fondsCreator.arkivskaperNavn + '@protonmail.com';
                                                    authContext.setOrganisation(orgName);
                                                    // Next get the postmottak series
                                                    let odata = '?%24filter' + encodeURIComponent("=tittel eq 'postmottak" +
                                                        fondsCreator.arkivskaperNavn + "'");
                                                    const urlPostalSeries = fonds[LINKS][SERIES_REL].href + odata;
                                                    axios.get(urlPostalSeries, getConfig)
                                                        .then((response) => {
                                                            if (response.data.results !== undefined && response.data.count > 0) {
                                                                appContext.setPostalSeries(response.data.results[0]);
                                                                console.log("Setting postalSeries " + JSON.stringify(response.data.results[0]));
                                                            } else {
                                                                console.log("Could not find postal series for " + fondsCreator.arkivskaperNavn);
                                                                const urlNewSeries = fonds[LINKS][NEW_SERIES_REL].href;
                                                                const newSeriesObject = {
                                                                    'tittel': 'postmottak' + fondsCreator.arkivskaperNavn
                                                                };
                                                                axios.post(urlNewSeries, newSeriesObject, loginConfig)
                                                                    .then((response) => {
                                                                        appContext.setPostalSeries(response.data);
                                                                        console.log("Setting postalSeries " + JSON.stringify(response.data));
                                                                    })
                                                                    .catch(error => {
                                                                        setErrorMessage('Error Creating postal series  ['
                                                                            + error.message + '] ' + urlNewSeries);
                                                                        console.log(error.message);
                                                                    });
                                                            }
                                                        }).catch(error => {
                                                        setErrorMessage('Error when accessing fonds creator  [' + error.message + '] ' + urlFondsCreator);
                                                        console.log(error.message);
                                                    });

                                                    odata = '?%24filter' + encodeURIComponent("=contains(tittel, 'saksbehandling')");
                                                    const urlRegularSeries = fonds[LINKS][SERIES_REL].href + odata;

                                                    axios.get(urlRegularSeries, getConfig)
                                                        .then((response) => {
                                                            if (response.data.results !== undefined && response.data.count > 0) {
                                                                appContext.setDefaultSeries(response.data.results[0]);
                                                            } else {
                                                                console.log("Could not find default series for " +
                                                                    fondsCreator.arkivskaperNavn);
                                                                const urlNewSeries = fonds[LINKS][NEW_SERIES_REL].href;
                                                                const newSeriesObject = {
                                                                    'tittel': 'saksbehandling ' + fondsCreator.arkivskaperNavn
                                                                };
                                                                axios.post(urlNewSeries, newSeriesObject, loginConfig)
                                                                    .then((response) => {
                                                                        appContext.setPostalSeries(response.data);
                                                                        console.log("Setting postalSeries " +
                                                                            JSON.stringify(response.data));
                                                                    })
                                                                    .catch(error => {
                                                                        setErrorMessage('Error Creating postal series  ['
                                                                            + error.message + '] ' + urlNewSeries);
                                                                        console.log(error.message);
                                                                    });
                                                            }
                                                            console.log("Navigating to " + roleCodeName.toLowerCase())
                                                            navigate('/' + roleCodeName.toLowerCase());
                                                        }).catch(error => {
                                                        setErrorMessage('Error when accessing fonds creator  [' + error.message + '] ' + urlFondsCreator);
                                                        console.log(error.message);
                                                    });

                                                } else {
                                                    setErrorMessage('Error when accessing fonds links [' + error.message + '] ' + urlFondsStructureLinks);
                                                    console.log(error.message);
                                                }
                                            }).catch(error => {
                                            setErrorMessage('Error when accessing fonds creator  [' + error.message + '] ' + urlFondsCreator);
                                            console.log(error.message);
                                        });
                                    }).catch(error => {
                                    setErrorMessage('Error when accessing fonds links [' + error.message + '] ' + urlFondsStructureLinks);
                                    console.log(error.message);
                                });
                            }).catch(error => {
                            setErrorMessage('Error when accessing application root [' + error.message + '] ' + apiUrl);
                            console.log(error.message);
                        });
                    }).catch(error => {
                    setErrorMessage('Error when accessing application root [' + error.message + '] ' + apiUrl);
                    console.log(error.message);
                });
            }).catch(error => {
            setErrorMessage('Error when logging into application [' + error.message + '] ' + loginUrl);
            console.log(error.message);
        });
    }

    const x = () => {

        const loginUrl = apiUrl + 'oauth/token?grant_type=password&client_id=' + clientId + '&username=' + username +
            '&password=' + encodeURIComponent(password);

        let loginConfig = {
            headers: {
                'Accept': CONTENT_TYPE_NOARK,
                'Authorization': loginBasicToken
            }
        };

        fetch(loginUrl, loginConfig)
            .then(response => {
                return response.json();
            }).then(data => {
            const tokenString = data.token_type + ' ' + data.access_token;
            const expirationTime = new Date((new Date().getTime() + (+data.expires_in * 1000)));
            authContext.login(username, tokenString, expirationTime);
            const options = {
                method: 'GET',
                headers: {
                    'Content-Type': CONTENT_TYPE_NOARK,
                    'Authorization': tokenString
                }
            };
            fetch(apiUrl, options)
                .then(response => {
                    return response.json();
                }).then(apiData => {

                const urlCaseHandlingLinks = apiData[LINKS][CASE_HANDLING_REL].href;
                fetch(urlCaseHandlingLinks, options)
                    .then(response => {
                        return response.json();
                    }).then(caseHandlingLinks => {
                    console.log("Setting casehandling links!");
                    console.log(caseHandlingLinks);
                    appContext.setCaseHandlingLinks(caseHandlingLinks);

                    const urlFondsLinks = apiData[LINKS][FONDS_STRUCTURE_REL].href;
                    fetch(urlFondsLinks, options)
                        .then(response => {
                            return response.json();
                        }).then(fondsLinks => {
                        console.log("Setting fonds structure links!");
                        console.log(fondsLinks);
                        appContext.setFondsLinks(fondsLinks);
                        const urlFondsCreator = fondsLinks[LINKS][FONDS_CREATOR_REL].href;
                        fetch(urlFondsCreator, options)
                            .then(response => {
                                return response.json();
                            }).then(fondsCreators => {
                            if (fondsCreators.results !== undefined) {
                                const fondsCreator = fondsCreators.results[0];
                                const orgName = 'nikita-postmottak+' + fondsCreator.arkivskaperNavn + '@protonmail.com';
                                authContext.setOrganisation(orgName);
                                // Next get the postmottak series
                                let odata = '?%24filter' + encodeURIComponent("=tittel eq 'postmottak" +
                                    orgName + "'");
                                const urlPostalSeries = fondsLinks[LINKS][SERIES_REL].href + odata;

                                fetch(urlPostalSeries, options)
                                    .then(response => {
                                        return response.json();
                                    }).then(postalSeries => {
                                    if (postalSeries.results !== undefined) {
                                        appContext.setPostalSeries(postalSeries.results[0]);
                                    } else {
                                        console.log("Could not find postal series for " + orgName)
                                    }

                                    odata = '?%24filter' + encodeURIComponent("=contains(tittel, 'saksbehandling')");
                                    const urlRegularSeries = fondsLinks[LINKS][SERIES_REL].href + odata;

                                    fetch(urlRegularSeries, options)
                                        .then(response => {
                                            return response.json();
                                        }).then(regularSeries => {
                                        if (regularSeries.results !== undefined) {
                                            appContext.setDefaultSeries(regularSeries.results[0]);
                                        } else {
                                            console.log("Could not find regular series for " + orgName)
                                        }
                                        console.log("Navigating to " + roleCodeName.toLowerCase())
                                        navigate('/' + roleCodeName.toLowerCase());
                                    })
                                        .catch(response => {
                                            console.log(response);
                                        })


                                })
                                    .catch(response => {
                                        console.log(response);
                                    })

                            }
                        })
                            .catch(response => {
                                console.log(response);
                            })
                    })
                        .catch(response => {
                            console.log(response);
                        })
                })
                    .catch(response => {
                        console.log(response);
                    })
            })
                .catch(response => {
                    console.log(response);
                })
        })
            .catch(response => {
                console.log(response);
            })
    };


    const getSeriesObjects = (token, fondsStructureLinks) => {
        // Get organisation name from fondsCreator
        const urlFondsCreator = fondsStructureLinks[LINKS][FONDS_CREATOR_REL].href;

        const options = {
            method: 'GET',
            headers: {
                'Content-Type': CONTENT_TYPE_NOARK,
                'Authorization': token
            }
        };

    }

    return (
        <Grid textAlign='center' style={{height: '100vh'}} verticalAlign='middle'>
            <Grid.Column style={{maxWidth: 450}}>
                <Header as='h2' color='teal' textAlign='center'>
                    Nikita pålogging
                </Header>
                <Form size='large'>
                    <Segment stacked>
                        <Form.Input
                            fluid
                            icon='user'
                            iconPosition='left'
                            placeholder='Epostaddresse'
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}/>
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Passord'
                            type='password'
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        <Form.Select
                            fluid
                            label='Rolle'
                            options={roleOptions}
                            onChange={roleChosen}
                            key={roleCode}
                            text={roleCodeName}
                            value={role}
                        />
                        <Button color='teal' fluid size='large'
                                onClick={doLogin}>
                            Logg inn
                        </Button>
                    </Segment>
                </Form>
                <Message>
                    Ikke konto fra før? <a href='#'>Registrer deg</a>
                </Message>
            </Grid.Column>
        </Grid>
    )
};

export default LoginPage;
