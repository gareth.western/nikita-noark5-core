import React, {useContext, useState} from 'react';
import {Form, Grid, Input, Segment} from 'semantic-ui-react';
import AuthContext from "../components/store/AuthContext";
import {CONTENT_TYPE_NOARK, correspondencePartTypeOptions, LINKS, SELF} from "../constants";
import axios from "axios";

const CorrespondencePartPage = (props) => {

    const auth = useContext(AuthContext);
    const [eTag, setETag] = useState('')

    // If there is no CorrespondencePart then create a JSON object with empty values so that the code does not have
    // null pointer problems.
    function checkAndSetDefaultCorrespondencePart(props) {
        if (props.hasOwnProperty('cpart') === false) {
            return {
                'korrespondanseparttype': {
                    'kode': '',
                    'kodenavn': ''
                },
                'navn': '',
                'personidentifikator': {
                    'foedselsnummer': '',
                    'dNummer': ''
                },
                'kontaktinformasjon': {
                    'epostadresse': '',
                    'telefonnummer': '',
                    'mobiltelefon': ''
                },
                'postadresse': {
                    'adresselinje1': '',
                    'adresselinje2': '',
                    'adresselinje3': '',
                    'postnummer': '',
                    'poststed': '',
                    'landkode': ''
                },
                'bostedsadresse': {
                    'adresselinje1': '',
                    'adresselinje2': '',
                    'adresselinje3': '',
                    'postnummer': '',
                    'poststed': '',
                    'landkode': ''
                }
            }
        }
        return props.cpart;
    }

    const cpart = checkAndSetDefaultCorrespondencePart(props);

    const [name, setName] = useState(cpart.navn);
    const [socialSecurityNumber, setSocialSecurityNumber] = useState(cpart.personidentifikator.foedselsnummer);
    const [dNumber, setDNumber] = useState(cpart.personidentifikator.dNummer);
    const [emailAddress, setEmailAddress] = useState(cpart.kontaktinformasjon.epostadresse);
    const [phoneNumber, setPhoneNumber] = useState(cpart.kontaktinformasjon.telefonnummer);
    const [mobileNumber, setMobileNumber] = useState(cpart.kontaktinformasjon.mobiltelefon);
    const [postalAddressLine1, setPostalAddressLine1] = useState(cpart.postadresse.adresselinje1);
    const [postalAddressLine2, setPostalAddressLine2] = useState(cpart.postadresse.adresselinje2);
    const [postalAddressLine3, setPostalAddressLine3] = useState(cpart.postadresse.adresselinje3);
    const [postalAddressPostalNumber, setPostalAddressPostalNumber] = useState(cpart.postadresse.postnummer);
    const [postalAddressPlace, setPostalAddressPlace] = useState(cpart.postadresse.poststed);
    const [postalAddressCountry, setPostalAddressCountry] = useState(cpart.postadresse.landkode);
    const [livingAddressLine1, setLivingAddressLine1] = useState(cpart.bostedsadresse.adresselinje1);
    const [livingAddressLine2, setLivingAddressLine2] = useState(cpart.bostedsadresse.adresselinje2);
    const [livingAddressLine3, setLivingAddressLine3] = useState(cpart.bostedsadresse.adresselinje3);
    const [livingAddressPostalNumber, setLivingAddressPostalNumber] = useState(cpart.bostedsadresse.postnummer);
    const [livingAddressPlace, setLivingAddressPlace] = useState(cpart.bostedsadresse.poststed);
    const [livingAddressCountry, setLivingAddressCountry] = useState(cpart.bostedsadresse.landkode);
    const [correspondencePartStatusCode, setCorrespondencePartStatusCode] = useState(cpart.korrespondanseparttype.kode);
    const [correspondencePartStatusCodeName, setCorrespondencePartStatusCodeName] =
        useState(cpart.korrespondanseparttype.kodenavn);

    function setCorrespondencePartValues() {

        cpart['navn'] = name;
        cpart['personidentifikator']['foedselsnummer'] = socialSecurityNumber;
        cpart['personidentifikator']['dNummer'] = dNumber;
        cpart['kontaktinformasjon']['epostadresse'] = emailAddress;
        cpart['kontaktinformasjon']['telefonnummer'] = phoneNumber;
        cpart['kontaktinformasjon']['mobiltelefon'] = mobileNumber;
        cpart['postadresse']['adresselinje1'] = postalAddressLine1;
        cpart['postadresse']['adresselinje2'] = postalAddressLine2;
        cpart['postadresse']['adresselinje3'] = postalAddressLine3;
        cpart['postadresse']['postnummer'] = postalAddressPostalNumber;
        cpart['postadresse']['poststed'] = postalAddressPlace;
        cpart['postadresse']['landkode'] = postalAddressCountry;
        cpart['bostedsadresse']['adresselinje1'] = livingAddressLine1;
        cpart['bostedsadresse']['adresselinje2'] = livingAddressLine2;
        cpart['bostedsadresse']['adresselinje3'] = livingAddressLine3;
        cpart['bostedsadresse']['postnummer'] = livingAddressPostalNumber;
        cpart['bostedsadresse']['poststed'] = livingAddressPlace;
        cpart['bostedsadresse']['landkode'] = livingAddressCountry;
    }

    let configForGet = {
        headers: {
            'Accept': CONTENT_TYPE_NOARK,
            'Authorization': auth.token
        }
    };

    async function getCorrespondencePart() {
        await axios.get(cpart[LINKS][SELF].href, configForGet)
            .then((response) => {
                setETag(response.headers.get("ETag"));

            }).catch(error => {
                console.log(error.message);
            });
    }

    async function updateCorrespondencePart() {
        let configForPut = configForGet;
        configForPut['headers']['ETAG'] = eTag;

        await axios.put([LINKS][SELF].href, JSON.stringify(cpart), configForPut)
            .then(() => {
                props.closeCorrespondencePartModal();
            }).catch(error => {
                console.log(error.message);
            });
    }

    const correspondencePartStatusChosen = (event, data) => {
        const {value} = data;
        const {key} = data.options.find(o => o.value === value);
        setCorrespondencePartStatusCode(key);
        setCorrespondencePartStatusCodeName(value);
    }

    return (
        <div>
            <Form>
                <Segment placeholder>
                    <Grid>
                        <Grid.Row>
                            <Form.Select
                                fluid
                                label='Type korrespondansepart'
                                options={correspondencePartTypeOptions}
                                onChange={correspondencePartStatusChosen}
                                key={correspondencePartStatusCode}
                                text={correspondencePartStatusCodeName}
                                value={cpart.korrespondanseparttype}
                            />
                        </Grid.Row>
                        <Grid.Row>
                            <Grid columns={3} divided>
                                <label><h3>Kontaktdetaljer</h3></label>
                                <Grid.Row>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>Navn</label>
                                            <Input
                                                placeholder='navn'
                                                value={name}
                                                icon='user' iconPosition='left'
                                                onChange={(e) => setName(e.target.value)}
                                            />
                                        </Form.Field>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>Personnummer</label>
                                            <Input
                                                placeholder='personnummer'
                                                value={socialSecurityNumber}
                                                icon='id badge' iconPosition='left'
                                                onChange={(e) => setSocialSecurityNumber(e.target.value)}
                                            />
                                        </Form.Field>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>dNummer</label>
                                            <Input
                                                placeholder='dNummer'
                                                value={dNumber}
                                                icon='id badge' iconPosition='left'
                                                onChange={(e) => setDNumber(e.target.value)}
                                            />
                                        </Form.Field>
                                    </Grid.Column>
                                </Grid.Row>
                                <Grid.Row>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>epostAdresse</label>
                                            <Input
                                                placeholder='navn@eksempel.no'
                                                value={emailAddress}
                                                icon='at' iconPosition='left'
                                                onChange={(e) => setEmailAddress(e.target.value)}
                                            />
                                        </Form.Field>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>telefonnummer</label>
                                            <Input
                                                placeholder='+47 ...'
                                                value={phoneNumber}
                                                icon='phone' iconPosition='left'
                                                onChange={(e) => setPhoneNumber(e.target.value)}/>
                                        </Form.Field>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>mobilnummer</label>
                                            <Input
                                                placeholder='+47 ...'
                                                value={mobileNumber}
                                                icon='mobile' iconPosition='left'
                                                onChange={(e) => setMobileNumber(e.target.value)}/>
                                        </Form.Field>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid columns={2} divided>
                                <label><h3>Postadresse</h3></label>
                                <Grid.Row>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>adresselinje1</label>
                                            <Input
                                                placeholder='Eksempelgate 1'
                                                value={livingAddressLine1}
                                                icon='address book' iconPosition='left'
                                                onChange={(e) => setLivingAddressLine1(e.target.value)}/>
                                        </Form.Field>
                                        <Form.Field>
                                            <label>adresselinje2</label>
                                            <Input
                                                value={livingAddressLine2}
                                                icon='address book' iconPosition='left'
                                                onChange={(e) => setLivingAddressLine2(e.target.value)}/>
                                        </Form.Field>
                                        <Form.Field>
                                            <label>adresselinje3</label>
                                            <Input
                                                value={livingAddressLine3}
                                                icon='address book' iconPosition='left'
                                                onChange={(e) => setLivingAddressLine3(e.target.value)}/>
                                        </Form.Field>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>postnummer</label>
                                            <Input
                                                value={livingAddressPostalNumber}
                                                icon='hashtag' iconPosition='left'
                                                onChange={(e) => setLivingAddressPostalNumber(e.target.value)}/>
                                        </Form.Field>
                                        <Form.Field>
                                            <label>poststed</label>
                                            <Input
                                                value={livingAddressPlace}
                                                icon='map marker' iconPosition='left'
                                                onChange={(e) => setLivingAddressPlace(e.target.value)}/>
                                        </Form.Field>
                                        <Form.Field>
                                            <label>land</label>
                                            <Input
                                                value={livingAddressCountry}
                                                icon='globe' iconPosition='left'
                                                onChange={(e) => setLivingAddressCountry(e.target.value)}/>
                                        </Form.Field>
                                    </Grid.Column>
                                </Grid.Row>
                                <label><h3>Bostedsadresse</h3></label>
                                <Grid.Row>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>adresselinje1</label>
                                            <Input
                                                placeholder='Eksempelgate 1'
                                                value={postalAddressLine1}
                                                icon='address book' iconPosition='left'
                                                onChange={(e) => setPostalAddressLine1(e.target.value)}/>
                                        </Form.Field>
                                        <Form.Field>
                                            <label>adresselinje2</label>
                                            <Input
                                                value={postalAddressLine2}
                                                icon='address book' iconPosition='left'
                                                onChange={(e) => setPostalAddressLine2(e.target.value)}/>
                                        </Form.Field>
                                        <Form.Field>
                                            <label>adresselinje3</label>
                                            <Input
                                                value={postalAddressLine3}
                                                icon='address book' iconPosition='left'
                                                onChange={(e) => setPostalAddressLine3(e.target.value)}/>
                                        </Form.Field>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Form.Field>
                                            <label>postnummer</label>
                                            <Input
                                                value={postalAddressPostalNumber}
                                                icon='hashtag' iconPosition='left'
                                                onChange={(e) => setPostalAddressPostalNumber(e.target.value)}/>
                                        </Form.Field>
                                        <Form.Field>
                                            <label>poststed</label>
                                            <Input
                                                value={postalAddressPlace}
                                                icon='map marker' iconPosition='left'
                                                onChange={(e) => setPostalAddressPlace(e.target.value)}/>
                                        </Form.Field>
                                        <Form.Field>
                                            <label>land</label>
                                            <Input
                                                value={postalAddressCountry}
                                                icon='globe' iconPosition='left'
                                                onChange={(e) => setPostalAddressCountry(e.target.value)}/>
                                        </Form.Field>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Grid.Row>
                    </Grid>
                </Segment>
            </Form>
        </div>
    )
}
export default CorrespondencePartPage;

