import React, {useContext, useEffect, useState} from "react";

import {Divider, Grid, Pagination} from "semantic-ui-react";
import CaseHandlerTaskFeed from "../components/feeds/CaseHandlerTaskFeed";
import CaseHandlerLogFeed from "../components/feeds/CaseHandlerLogFeed";
import {CASE_FILE_REL, caseFileStatusOptions, documentFlowCaseHandler, LINKS} from "../constants";
import CaseFileListView from "../components/casehandling/CaseFileListView";
import AuthContext from "../components/store/AuthContext";
import AppContext from "../components/store/AppContext";

function CaseHandlerDashboard() {

    const auth = useContext(AuthContext);
    const app = useContext(AppContext);

    const token = auth.token;
    const resultsPerPage = app.resultsPerPage;

    const [caseFiles, setCaseFiles] = useState([]);
    const [activePage, setActivePage] = useState(0);
    const [totalPages, setTotalPages] = useState(1);
    const [urlCaseFiles, setUrlCaseFiles] = useState(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + '?%24orderby=mappeID asc');

    const doSearch = (searchString) => {
        if (searchString.length > 0) {
            let odata = encodeURIComponent("=contains(tittel, '" + searchString + "') or contains(beskrivelse, '" + searchString + "')");
            setUrlCaseFiles(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + '?%24filter' + odata);
        } else {
            setUrlCaseFiles(app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + '?%24orderby=mappeID asc');
        }
        console.log("searchString " + searchString);
        console.log("URL is " + urlCaseFiles);
        HandleChange(urlCaseFiles);
    }

    const HandleChange = (urlCaseFiles) => {


        useEffect(async () => {
            fetch(urlCaseFiles, {
                headers: {
                    'Accept': 'application/vnd.noark5+json',
                    'Authorization': token
                }
            })
                .then(response => {
                    return response.json();
                }).then(data => {
                if (data.results !== undefined) {
                    // If # results is < 10, then
                    let tp = data.count / resultsPerPage;
                    setTotalPages(Math.ceil(tp));
                    setCaseFiles(data.results);
                } else {
                    setCaseFiles([]);
                }
            })
                .catch(response => {
                    console.log(response);
                }).catch(response => {
                console.log(response);
            })
        }, [urlCaseFiles])
    }

    const handleChange = (e, pageInfo) => {
        setActivePage(pageInfo.activePage);
        const url = urlCaseFiles;
        let page = ((pageInfo.activePage - 1) * resultsPerPage);
        if (url.includes("%24skip")) {
            const x = url.replace(/24skip=\d+/g, '24skip=' + page);
            setUrlCaseFiles(x);
        } else {
            setUrlCaseFiles(url + "&%24skip=" + page);
        }
    }

    HandleChange(urlCaseFiles);

    return (
        <Grid>
            <Grid.Row columns={3}>
                <Grid.Column width={13}>
                    <Grid.Row>
                        <CaseFileListView role={'casehandler'}
                                          showNewCaseFileButton={true}
                                          headerTitle={'Mine saker'}
                                          caseFiles={caseFiles}
                                          caseFileStatusOptions={caseFileStatusOptions}
                                          doSearch={doSearch}/>
                    </Grid.Row>
                    <Grid.Row>
                        {totalPages > 1 &&
                            <Pagination defaultActivePage={activePage}
                                        totalPages={totalPages}
                                        onPageChange={handleChange}/>
                        }
                    </Grid.Row>
                </Grid.Column>
                <Grid.Column width={3}>
                    <CaseHandlerTaskFeed documentFlowCaseHandler={documentFlowCaseHandler}/>
                    <Divider section/>
                    <CaseHandlerLogFeed/>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

export default CaseHandlerDashboard;
