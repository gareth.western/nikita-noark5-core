import React, {useContext, useEffect, useState} from "react";

import "semantic-ui-css/semantic.min.css";

import {Grid, Header, Tab} from "semantic-ui-react";
import CaseFileListView from "../components/casehandling/CaseFileListView";
import {CASE_FILE_REL, caseFileStatusOptions, LINKS} from "../constants";
import AuthContext from "../components/store/AuthContext";
import AppContext from "../components/store/AppContext";

const LeaderDashboard = () => {

    const auth = useContext(AuthContext);
    const app = useContext(AppContext);
    const token = auth.token;
    const odataApproval = '?%24filter' + encodeURIComponent("=saksstatus/kodenavn eq 'Avsluttet av saksbehandler'");
    const urlApprovalCaseFiles = app.caseHandlingLinks[LINKS][CASE_FILE_REL].href + odataApproval;
    const [approvalCaseFiles, setApprovalCaseFiles] = useState([]);

    useEffect(() => {

        // Get casefile objects for approval
        fetch(urlApprovalCaseFiles, {
            headers: {
                'Accept': 'application/vnd.noark5+json',
                'Authorization': token
            }
        })
            .then(response => {
                return response.json();
            }).then(data => {
            if (data.results !== undefined) {
                setApprovalCaseFiles(data.results);
            } else {
                data.results = [];
            }
        })
            .catch(response => {
                console.log(response);
            })
    }, [])


    const leaderPanes = [{
        menuItem: "Saker til godkjenning",
        render: () =>
            <Tab.Pane>
                {getApprovalPaneContent()}
            </Tab.Pane>
    }];

    const getApprovalPaneContent = () => {
        return (
            <CaseFileListView showNewCaseFileButton={false} role={'leader'}
                              caseFileStatusOptions={caseFileStatusOptions} caseFiles={getApprovalCaseFiles()}/>
        );
    }

    const getApprovalCaseFiles = () => {
        return approvalCaseFiles;
    }

    return (
        <Grid>
            <Grid.Row columns={2}>
                <Grid.Column>
                    <Grid padded>
                        <Grid.Row>
                            <Grid.Column textAlign='left'>
                                <Header dividing size="large" as="h1">
                                    Lederoppgaver
                                </Header>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Tab menu={{pointing: true}} panes={leaderPanes}/>
                        </Grid.Row>
                    </Grid>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

export default LeaderDashboard;
