import React from "react";

import "semantic-ui-css/semantic.min.css";

import {Container, Header} from "semantic-ui-react";

const LandingPage = () => (
    <div>
        <Container text style={{marginTop: '6em'}}>
            <Header as='h1'>Nikita Noark 5</Header>
            <p>Velkommen til en instans av nikita-noark5-kjerne. Denne instansen brukes som en undervisnings- og
                forskningsverktøy for arkivstudiet ved OsloMet. Merk at data som lagres i nikita-noark5-kjerne slettes
                regelmessig. Da dette er et undervisningsverktøy skal du ikke laste opp ekte data om deg eller andre. I
                løpet av semesteret vil andre studenter se dine data i laborasjon </p>
            <p>
                Dersom du har en konto kan du trykke på 'logg på' lenken på høyre side av menyen på toppen av siden.
            </p>
            <Header as='h2'>Opprett konto</Header>
            <p>
                Dersom du har alt opprettet en konto kan du trykke på 'logg på' lenken på høyre side av menyen på toppen
                av siden. Dersom du ikke har en konto kan du opprette en ved å trykke på denne lenken.
            </p>
            <Header as='h2'>Kildekode</Header>
            <p>
                nikita-noark5-kjerne er fri programvare. Det betyr at kildekoden til programvaren er fritt tilgjengelig
                og du eller andre kan laste det ned. Du kan bruke nikita-noark5-kjerne for å lære mer om standardisert
                dokumentasjonsforvaltning i henhold til Noark 5 standarden.
            </p>
        </Container>
    </div>
)

export default LandingPage;
