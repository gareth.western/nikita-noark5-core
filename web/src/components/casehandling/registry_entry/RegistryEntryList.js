import React from 'react';
import {Table} from 'semantic-ui-react'

import RegistryEntryListRow from './RegistryEntryListRow';
import RegistryEntryListHeader from './RegistryEntryListHeader';

/**
 * Creates a Card that contains a table with header and row values.
 *
 * Includes a pagination button.
 *
 * @param props Takes in a List of relevant RegistryEntry
 * @returns {JSX.Element}
 * @constructor
 */
function RegistryEntryList(props) {

    const registryEntries = props.registryEntries || [];
    const DisplayRegistryEntries = registryEntries.map(
        (registryEntry) => {
            return (
                <RegistryEntryListRow key={registryEntry.systemID} registryEntry={registryEntry}/>
            )
        }
    )

    return (
        <Table celled padded>
            <RegistryEntryListHeader/>
            <Table.Body>
                {DisplayRegistryEntries}
            </Table.Body>
        </Table>
    );
}

export default RegistryEntryList;
