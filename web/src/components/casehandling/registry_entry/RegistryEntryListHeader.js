import React from "react";
import {Table} from 'semantic-ui-react'

/**
 * Returns a row of html that can create a header row of a table for a RegistryEntry.
 *
 * @returns {JSX.Element}
 * @constructor
 */
const RegistryEntryListHeader = () => {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>Journalpostnr</Table.HeaderCell>
                <Table.HeaderCell>Journaldato</Table.HeaderCell>
                <Table.HeaderCell>Type</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    );
}

export default RegistryEntryListHeader;
