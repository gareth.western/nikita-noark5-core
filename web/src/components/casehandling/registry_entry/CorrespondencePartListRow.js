import React, {useCallback} from 'react';
import {Table} from 'semantic-ui-react'
import {format, parseISO} from 'date-fns'
import {useNavigate} from "react-router-dom";

function RegistryEntryListRow(props) {

    const navigate = useNavigate();
    const handleOnClick = useCallback(urlCasefile => navigate('/saksbehandler/saksmappe/journalpost', {state: {registryEntry: props.registryEntry}}), [navigate]);

    return (
        <Table.Row onClick={() => handleOnClick(props.registryEntry)}>
            <Table.Cell key={props.registryEntry.registreringsID}>{props.registryEntry.registreringsID}</Table.Cell>
            <Table.Cell>{format(parseISO(props.registryEntry.opprettetDato), 'dd/MM/yyyy')}</Table.Cell>
            <Table.Cell>{props.registryEntry.tittel}</Table.Cell>
            <Table.Cell>{props.registryEntry.journalstatus.kodenavn}</Table.Cell>
        </Table.Row>
    );
}

export default RegistryEntryListRow;
