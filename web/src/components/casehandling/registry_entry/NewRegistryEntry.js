import React, {useContext, useState} from "react";
import {Button, Form, Grid, Icon} from "semantic-ui-react";
import {
    CONTENT_TYPE_NOARK,
    LINKS,
    NEW_RECORD_NOTE_REL,
    NEW_REGISTRY_ENTRY_REL,
    recordTypeOptions,
    registryEntryTypeOptions
} from "../../../constants";
import AuthContext from "../../store/AuthContext";

const NewRegistryEntry = (props) => {

    const auth = useContext(AuthContext);

    // Values for binding
    const [title, setTitle] = useState('');
    const [publicTitle, setPublicTitle] = useState('');
    const [description, setDescription] = useState('');
    const [registrationTypeCode, setRegistrationTypeCode] = useState('J')
    const [registrationTypeCodeName, setRegistrationTypeCodeName] = useState('Journalpost')
    const [registrationStatusCode, setRegistrationStatusCode] = useState('F')
    const [registrationStatusCodeName, setRegistrationStatusCodeName] = useState('Dokumentet er ferdigstilt')
    const [registryEntryTypeCode, setRegistryEntryTypeCode] = useState('I')
    const [registryEntryTypeCodeName, setRegistryEntryTypeCodeName] = useState('Inngående dokument')

    const caseFile = props.caseFile;

    const copyTitle = () => {
        setPublicTitle(title);
    };

    const registrationTypeChosen = (event, data) => {
        setRegistrationTypeCode(data.key);
        setRegistrationTypeCodeName(data.value);
        console.log(registrationTypeCodeName);
    }


    function getRegistrationType() {
        return registrationTypeCodeName;
    }

    function getRegistryEntryTypeCode() {
        return registryEntryTypeCode;
    }

    function getRegistryEntryTypeCodeName() {
        return registryEntryTypeCodeName;
    }

    function HandleSubmit() {

        const token = auth.token;
        let urlCreateRegistration = caseFile[LINKS][NEW_REGISTRY_ENTRY_REL].href;
        let requestOptions = null;

        let registrationType = getRegistrationType();
        if (registrationType.toLowerCase() === 'arkivnotat') {
            urlCreateRegistration = caseFile[LINKS][NEW_RECORD_NOTE_REL].href;
            requestOptions = {
                method: 'POST',
                headers: {
                    'Content-Type': CONTENT_TYPE_NOARK,
                    'Authorization': token
                },
                body: JSON.stringify(
                    {
                        'tittel': title,
                        'offentligTittel': publicTitle,
                        'beskrivelse': description,
                    }
                )
            };
        } else {
            requestOptions = {
                method: 'POST',
                headers: {
                    'Content-Type': CONTENT_TYPE_NOARK,
                    'Authorization': token
                },
                body: JSON.stringify(
                    {
                        'tittel': title,
                        'offentligTittel': publicTitle,
                        'beskrivelse': description,
                        'journalposttype': {
                            'kode': getRegistryEntryTypeCode(),
                            'kodenavn': getRegistryEntryTypeCodeName()
                        },
                        'journalstatus': {'kode': 'J', 'kodenavn': 'Journalført'}
                    }
                )
            };
        }

        fetch(urlCreateRegistration, requestOptions)
            .then(response => {
                return response.json();
            }).then(data => {
            if (data !== undefined) {
                props.closeModal();
                props.addRegistryEntry(data);
            }
        })
            .catch(response => {
                console.log(response);
            })

    }

    return (
        <Form>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.Input fluid label='Tittel' placeholder='Tittel'
                                    value={title} onChange={(e) => setTitle(e.target.value)}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.Input fluid label='OffentligTittel' placeholder='Offentlig Tittel'
                                    value={publicTitle}
                                    action={{
                                        icon: 'copy',
                                        onClick: () => copyTitle()
                                    }}
                                    onChange={(e) => setPublicTitle(e.target.value)}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.TextArea fluid label='Beskrivelse' placeholder='Beskrivelse'
                                       value={description} onChange={(e) => setDescription(e.target.value)}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.Dropdown text='Registrering type'
                                       options={recordTypeOptions}
                                       onChange={registrationTypeChosen}/>
                    </Grid.Column>
                </Grid.Row>
                {
                    registrationTypeCodeName === 'Journalpost' &&
                    (
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <Form.Dropdown text='Journalposttype' options={registryEntryTypeOptions}/>
                            </Grid.Column>
                        </Grid.Row>
                    )
                }
                <Grid.Row>
                    <Grid.Column textAlign='right'>
                        <Button color='green' onClick={HandleSubmit}>
                            <Icon name='plus'/> Opprett
                        </Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Form>
    );
}

export default NewRegistryEntry;
