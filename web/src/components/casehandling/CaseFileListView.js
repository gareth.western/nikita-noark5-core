import React, {useContext, useState} from "react";

import {Button, Grid, Header, Icon, Input, Modal, Popup} from "semantic-ui-react";
import CaseFileList from "./CaseFileList";
import NewCaseFile from "./NewCaseFile";
import CaseHandlingHeader from "./CaseHandlingHeader";
import AuthContext from "../store/AuthContext";

const CaseFileListView = (props) => {

    const auth = useContext(AuthContext);
    const [open, setOpen] = useState(false);
    const [showNewCaseFileButton] = useState(props.showNewCaseFileButton);

    const caseFiles = props.caseFiles;

    const closeModal = () => {
        setOpen(false);
    }

    const updateCaseFileList = (caseFile) => {
        caseFiles.push(caseFile);
    }

    function doFilterSearch(value) {
        props.doSearch(value);
    }

    return (
        <Grid>
            <Grid.Column width={13}>
                <Grid padded>
                    <CaseHandlingHeader header={props.headerTitle}/>
                    <Grid.Row>
                        <Grid.Column width={1}>
                            <i className="filter large icon"/>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Input placeholder="Søk..."
                                   size="small"
                                   onChange={(e) => doFilterSearch(e.target.value)}/>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            {showNewCaseFileButton &&
                                <Popup
                                    trigger={
                                        <Modal
                                            closeIcon
                                            open={open}
                                            trigger={<Button icon>
                                                <Icon name='plus'/>
                                                Ny saksmappe
                                            </Button>}
                                            onClose={() => setOpen(false)}
                                            onOpen={() => setOpen(true)}
                                        >
                                            <Header icon='folder open outline' content='Opprett Ny Saksmappe'/>
                                            <Modal.Content>
                                                <NewCaseFile closeModal={closeModal}
                                                             updateCaseFileList={updateCaseFileList}
                                                             responsible={auth.user}/>
                                            </Modal.Content>
                                            <Modal.Actions>
                                                <Button onClick={() => setOpen(false)}>
                                                    <Icon name='remove'/> Avbryt
                                                </Button>
                                            </Modal.Actions>
                                        </Modal>
                                    }
                                    content='Opprett Ny Saksmappe'
                                    size='small'
                                />
                            }
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <CaseFileList role={props.role} caseFiles={caseFiles}/>
                    </Grid.Row>
                </Grid>
            </Grid.Column>
        </Grid>
    )
}

export default CaseFileListView;
