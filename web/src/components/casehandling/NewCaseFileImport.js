import React, {useContext, useState} from "react";
import {Button, Form, Grid, Icon} from "semantic-ui-react";
import AuthContext from "../store/AuthContext";
import {CONTENT_TYPE_NOARK, EXPAND_FILE_TO_CASE_FILE, LINKS, SELF} from "../../constants";
import AppContext from "../store/AppContext";

const NewCaseFileImport = (props) => {

    const auth = useContext(AuthContext);
    const app = useContext(AppContext);
    const [title, setTitle] = useState(props.file.tittel);
    const [publicTitle, setPublicTitle] = useState('');
    const [description, setDescription] = useState('');

    const copyTitle = () => {
        setPublicTitle(title);
    };

    function HandleSubmit(event) {
        event.preventDefault();

        const fromSeries = app.postalSeries.systemID;
        const toSeries = app.defaultSeries.systemID;
        const urlFile = props.file[LINKS][SELF].href;

        // First move the file from the current series to the proper series
        const moveRequestOptions = {
            method: 'PATCH',
            headers: {
                'Content-Type': CONTENT_TYPE_NOARK,
                'Authorization': auth.token
            },
            body: JSON.stringify(
                {
                    "op": "move",
                    "from": fromSeries,
                    "path": toSeries
                })
        };

        fetch(urlFile, moveRequestOptions)
            .then(response => {
                return response.json();
            }).then(data => {

            // If that is successful, convert the file to a casefile
            const urlConvertToCaseFile = props.file[LINKS][EXPAND_FILE_TO_CASE_FILE].href;
            const requestOptions = {
                method: 'POST',
                headers: {
                    'Content-Type': CONTENT_TYPE_NOARK,
                    'Authorization': auth.token
                },
                body: JSON.stringify(
                    {
                        'tittel': title,
                        'offentligTittel': publicTitle,
                        'beskrivelse': description,
                        "saksstatus": {"kode": "B", "kodenavn": "Under behandling"},
                        "saksansvarlig": "unassigned"
                    })
            };

            fetch(urlConvertToCaseFile, requestOptions)
                .then(response => {
                    return response.json();
                }).then(data => {
                props.closeModal();
                props.updateCaseFileList();
            })
                .catch(response => {
                    console.log(response);
                })
            console.log(data);
        })
            .catch(response => {
                console.log(response);
            })
    }

    return (
        <Form>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.Input fluid label='Tittel' placeholder='Tittel'
                                    value={title} onChange={(e) => setTitle(e.target.value)}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.Input fluid label='OffentligTittel' placeholder='Offentlig Tittel'
                                    value={publicTitle}
                                    action={{
                                        icon: 'copy',
                                        onClick: () => copyTitle()
                                    }}
                                    onChange={(e) => setPublicTitle(e.target.value)}
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.TextArea fluid label='Beskrivelse' placeholder='Besrkivelse'
                                       value={description} onChange={(e) => setDescription(e.target.value)}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column textAlign='right'>
                        <Button color='green' onClick={HandleSubmit}>
                            <Icon name='plus'/> Opprett
                        </Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Form>
    );
}

export default NewCaseFileImport;
