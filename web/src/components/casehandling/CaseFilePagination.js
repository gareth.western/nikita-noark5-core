import React from 'react'
import {Pagination} from 'semantic-ui-react'

const CaseFilePagination = (props) => {

    return (
        <div>
            <Pagination
                defaultActivePage={props.defaultActivePage}
                totalPages={props.totalPages}
            />
        </div>
    )
}

export default CaseFilePagination;
