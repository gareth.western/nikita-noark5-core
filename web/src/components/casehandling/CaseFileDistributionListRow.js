import React, {useState} from 'react';
import {Button, Header, Icon, Modal, Table} from 'semantic-ui-react'
import {format, parseISO} from 'date-fns'
import Distribute from "./distribute/Distribute";

function CaseFileDistributionListRow(props) {

    const [distributeModal, setDistributeModal] = useState(false)

    const closeDistributeModal = () => {
        setDistributeModal(false);
        props.updateCaseFileList();
    }

    return (
        <Table.Row>
            <Table.Cell key={props.caseFile.mappeID}>{props.caseFile.mappeID}</Table.Cell>
            <Table.Cell>{format(parseISO(props.caseFile.saksdato), 'dd/MM/yyyy')}</Table.Cell>
            <Table.Cell>{props.caseFile.tittel}</Table.Cell>
            <Table.Cell>
                <Modal
                    onClose={() => setDistributeModal(false)}
                    onOpen={() => setDistributeModal(true)}
                    open={distributeModal}
                    trigger={
                        <Button icon>
                            <Icon name='share'/>
                            Fordel
                        </Button>
                    }
                >
                    <Header icon='share from square' content='Fordel til saksbehandler'/>
                    <Modal.Content>
                        <Distribute casefile={props.caseFile} closeModal={closeDistributeModal}/>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button onClick={() => setDistributeModal(false)}>Avbryt</Button>
                    </Modal.Actions>
                </Modal>
            </Table.Cell>
        </Table.Row>
    );
}

export default CaseFileDistributionListRow;
