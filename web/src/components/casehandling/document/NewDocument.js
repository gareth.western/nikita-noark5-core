import React, {useContext, useState} from "react";
import {Button, Dropdown, Form, Grid, Icon, Popup} from "semantic-ui-react";
import {
    associatedAsOptions,
    CONTENT_TYPE_NOARK,
    DOCUMENT_FILE_REL,
    documentStatusOptions,
    LINKS,
    NEW_DOCUMENT_DESCRIPTION_REL,
    NEW_DOCUMENT_OBJECT_REL,
    variantFormatOptions
} from "../../../constants";
import AuthContext from "../../store/AuthContext";

const NewDocument = (props) => {

    const registryEntry = props.registryEntry;

    const [file, setFile] = useState();
    const [filename, setFilename] = useState('');
    const [filesize, setFilesize] = useState('');
    const [mimeType, setMimeType] = useState('');
    const [loading, setLoading] = useState(false);

    // Required status values
    const [associatedWithRecordAsCode, setAssociatedWithRecordAsCode] = useState('H')
    const [associatedWithRecordAsCodeName, setAssociatedWithRecordAsCodeName] = useState('Hoveddokument')
    const [documentStatusCode, setDocumentStatusCode] = useState('F')
    const [documentStatusCodeName, setDocumentStatusCodeName] = useState('Dokumentet er ferdigstilt')
    const [variantFormatCode, setVariantFormatCode] = useState('P')
    const [variantFormatCodeName, setVariantFormatCodeName] = useState('Produksjonsformat')

    // Input values
    const [documentType, setDocumentType] = useState('Brev')
    const [title, setTitle] = useState('')

    const urlNewDocumentDescription = registryEntry[LINKS][NEW_DOCUMENT_DESCRIPTION_REL].href;
    const token = useContext(AuthContext).token;

    const getAssociatedWithRecordAs = () => {
        return {
            kode: associatedWithRecordAsCode,
            kodenavn: associatedWithRecordAsCodeName
        }
    }

    const getVariantFormat = () => {
        return {
            kode: variantFormatCode,
            kodenavn: variantFormatCodeName
        }
    }

    const getDocumentStatus = () => {
        return {
            kode: documentStatusCode,
            kodenavn: documentStatusCodeName
        }
    }

    const getDocumentType = () => {
        if (documentType.toLowerCase() === "rundskriv") {
            return {
                "kode": "R",
                "kodenavn": "Rundskriv"
            }
        } else if (documentType.toLowerCase() === "faktura") {
            return {
                "kode": "F",
                "kodenavn": "Faktura"
            }
        } else if (documentType.toLowerCase() === "ordrebekreftelse") {
            return {
                "kode": "O",
                "kodenavn": "Ordrebekreftelse"
            }
        } else {
            return {
                "kode": "B",
                "kodenavn": "Brev"
            }
        }
    }

    const variantFormatChosen = (event, data) => {
        const {value} = data;
        const {key} = data.options.find(o => o.value === value);
        setVariantFormatCode(key);
        setVariantFormatCodeName(value);
    }

    const documentStatusChosen = (event, data) => {
        const {value} = data;
        const {key} = data.options.find(o => o.value === value);
        setDocumentStatusCode(key);
        setDocumentStatusCodeName(value);
    }

    const associatedWithRecordAsChosen = (event, data) => {
        const {value} = data;
        const {key} = data.options.find(o => o.value === value);
        setAssociatedWithRecordAsCode(key);
        setAssociatedWithRecordAsCodeName(value);
    }

    const requestOptionsNewDocumentDescription = () => {
        return {
            method: 'POST',
            headers: {
                'Content-Type': CONTENT_TYPE_NOARK,
                'Authorization': token
            },
            body: JSON.stringify(
                {
                    "tittel": title,
                    dokumenttype: getDocumentType(),
                    dokumentstatus: getDocumentStatus(),
                    tilknyttetRegistreringSom: getAssociatedWithRecordAs()
                })
        }
    };

    const requestOptionsNewDocumentObject = () => {
        return {
            method: 'POST',
            headers: {
                'Content-Type': CONTENT_TYPE_NOARK,
                'Authorization': token
            },
            body: JSON.stringify(
                {
                    'filnavn': filename,
                    'filstoerrelse': filesize,
                    variantformat: getVariantFormat()
                })
        }
    };

    const requestOptionsUploadDocument = () => {
        if (mimeType === undefined) {
            setMimeType('application/x-www-form-urlencoded');
        }
        return {
            method: 'POST',
            headers: {
                'Content-Type': mimeType,
                'Authorization': token
            },
            body: file
        }
    };

    /**
     * There are three POST requests that happen here.
     * The first fetch creates a documentDescription
     * The second fetch creates a DocumentObject
     * The third fetch uploads the document associated with the DocumentObject
     *
     * @param event The click event
     */
    const handleSubmit = event => {
        event.preventDefault();
        setLoading(true);
        // First create the documentDescription
        fetch(urlNewDocumentDescription, requestOptionsNewDocumentDescription())
            .then(response => {
                return response.json();
            }).then(data => {
            // documentDescription is OK. Get the new the href for a new document object
            const urlNewDocumentObject = data[LINKS][NEW_DOCUMENT_OBJECT_REL].href;
            fetch(urlNewDocumentObject, requestOptionsNewDocumentObject())
                .then(response => {
                    return response.json();
                }).then(data => {
                // documentObject is OK. Now upload a document with the document object
                const urlUploadDocument = data[LINKS][DOCUMENT_FILE_REL].href
                fetch(urlUploadDocument, requestOptionsUploadDocument())
                    .then(response => {
                        return response.json();
                    }).then(data => {
                    props.closeModal();
                    setLoading(false);
                    props.addDocumentDescription();
                })
                    .catch(response => {
                        console.log(response);
                    })
            })
                .catch(response => {
                    console.log(response);
                })
        })
            .catch(response => {
                console.log(response);
            })
    };

    const copyTitleFromRegistryEntry = () => {
        setTitle(registryEntry.tittel);
    };


    const setChosenFile = (e) => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
        setMimeType(e.target.files[0].type);
        setFilesize(e.target.files[0].size);
    };

    return (
        <Form>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={4}>
                        <Popup
                            trigger={<Form.Input placeholder='Dokumenttype'/>}
                            content='Type dokument. Kan feks være Brev, Faktura o.l.'
                            size='small'
                        />
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Dropdown
                            placeholder='Variantformat'
                            options={variantFormatOptions}
                            onChange={variantFormatChosen}
                            selection
                            labeled
                        />
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Form.Dropdown
                            placeholder='Tilknyttet som'
                            onChange={associatedWithRecordAsChosen}
                            options={associatedAsOptions}
                            selection
                        />
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Form.Dropdown
                            placeholder='DokumentStatus'
                            onChange={documentStatusChosen}
                            options={documentStatusOptions}
                            selection
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Form.Input
                            label="Tittel"
                            placeholder="Tittel på dokumentet"
                            action={{
                                icon: 'copy',
                                onClick: () => copyTitleFromRegistryEntry()
                            }}
                            onChange={(e) => setTitle(e.target.value)}
                            value={title}
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={4}>
                        <Button size="medium" as="label" htmlFor="file" type="button">
                            <Icon name="file"/> Velg fil
                        </Button>
                        <input type="file" id="file" hidden onChange={setChosenFile}/>
                    </Grid.Column>
                    <Grid.Column width={12}>
                        <Form.Input
                            label="Filnavn"
                            placeholder="Navnet på filen som skal lastes opp"
                            readOnly
                            value={filename}
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column textAlign="right">
                        {!loading &&
                            <Button color='green' onClick={handleSubmit}>
                                <Icon name='plus'/> Legg til
                            </Button>
                        }
                        {loading &&
                            <Button loading color='blue' onClick={handleSubmit}>
                                <Icon name='upload'/>Laster opp
                            </Button>
                        }
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Form>
    );
}

export default NewDocument;
