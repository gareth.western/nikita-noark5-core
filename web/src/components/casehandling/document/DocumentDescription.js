import React, {useContext, useEffect, useState} from 'react'
import {Button, Container, Divider, Form, Grid, Header, Icon, Label, Popup, Segment, Tab} from 'semantic-ui-react'
import {
    CONTENT_TYPE_NOARK,
    DOCUMENT_FILE_REL,
    DOCUMENT_OBJECT_REL,
    documentStatusOptions,
    LINKS,
    SELF,
} from "../../../constants";
import AuthContext from "../../store/AuthContext";
import axios from "axios";

const DocumentDescription = (props) => {

    const auth = useContext(AuthContext);
    const [documentObjects, setDocumentObjects] = useState([]);
    const [documentStatus, setDocumentStatus] = useState(props.documentDescription.dokumentstatus.kodenavn);
    const [documentType] = useState(props.documentDescription.dokumenttype.kodenavn);
    const [open, setOpen] = useState(false);

    const closeModal = () => {
        setOpen(false);
    }

    useEffect(() => {

        let config = {
            headers: {
                'Accept': CONTENT_TYPE_NOARK,
                'Authorization': auth.token
            }
        };

        axios.get(props.documentDescription[LINKS][SELF].href, config)
            .then((response) => {
                const documentObjectsUrlString = response.data[LINKS][DOCUMENT_OBJECT_REL].href;
                axios.get(documentObjectsUrlString, config)
                    .then((response) => {
                        setDocumentObjects(response.data.results);
                    }).catch(error => {
                    console.log(error.message);
                });
            }).catch(error => {
            console.log(error.message);
        });
    }, [setDocumentObjects]);


    function downloadFile(documentObject) {
        let urlDocument = documentObject[LINKS][DOCUMENT_FILE_REL].href;
        fetch(urlDocument, {
            headers: {
                'Authorization': auth.token
            }
        }).then(response => {
                response.blob().then(blob => {
                    let a = document.createElement("a");
                    a.setAttribute('target', '_blank');
                    a.href = window.URL.createObjectURL(blob);
                    a.download = documentObject.filnavn;
                    const tab = window.open();
                    tab.location.href = a.href;
                    //parentNode.removeChild(a);
                });
            }
        ).catch(error => {
            console.log(error);
        });
    }

    function showDocumentUploadModal(doc) {
        return undefined;
    }

    const panes = documentObjects.map(doc => ({
        menuItem: doc.variantformat.kodenavn,
        render: () =>
            <Tab.Pane>
                <Popup
                    trigger={<Label as='a' color='grey' attached='top right'>{doc.versjonsnummer}</Label>}
                    content='Dokument versjon.'
                    size='small'
                />
                <Form>
                    <Form.Group>
                        <Popup
                            trigger={<Form.Input value={doc.filnavn} placeholder='Filnavn' label='Filnavn' width={12}/>}
                            content='Navnet filen kommer til å ha når den lastes ned. Merk! Uten filendelse'
                            size='small'/>
                        <Popup
                            trigger={<Form.Input value={doc.format.kodenavn} placeholder='Format' label='Format'
                                                 width={2}/>}
                            content='Filformatet til opplastet fil. Ofte forbindes filendelse med format'
                            size='small'/>
                        <Popup
                            trigger={<Form.Input value={doc.filstoerrelse} placeholder='Filstørrelse'
                                                 label='Filstørrelse' width={2}/>}
                            content='Størrelsen på filen i bytes. Heltal > 0. Settes automatisk'
                            size='small'/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Input value={doc.sjekksum} placeholder='Sjekksum' label='Sjekksum' width={13}/>
                        <Form.Input value={doc.sjekksumAlgoritme} placeholder='Algoritme' label='Algoritme' width={3}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Input value={doc.opprettetDato} placeholder='opprettetDato' label='opprettetDato'
                                    width={8}/>
                        <Form.Input value={doc.opprettetAv} placeholder='opprettetDato' label='opprettetAv' width={8}/>
                    </Form.Group>
                </Form>
                <Segment floated='right'>
                    <Popup
                        trigger={<Button
                            onClick={() => downloadFile(doc)}
                            icon
                            labelPosition='right'>
                            <Icon name='download'/>
                            Last ned
                        </Button>}
                        content='Last ned dokumentet'
                        size='small'
                    />
                </Segment>
            </Tab.Pane>
    }));

    return (
        <div>
            <Segment>
                <Container textAlign='left'>
                    <Header
                        as='h3'>{props.documentDescription.dokumentnummer}) {props.documentDescription.tilknyttetRegistreringSom.kodenavn}</Header>
                </Container>
                <Divider/>
                <Container>
                    <Form>
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={4}>
                                    <Form.Input fluid label='Tittel' placeholder='Tittel'
                                                value={documentType}/>
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <Popup
                                        trigger={<Form.Input placeholder='Forfatter' label='Forfatter'/>}
                                        content='Navn på hvem som har skrevet dokumentet'
                                        size='small' value={props.documentDescription.forfatter}
                                    />
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <Form.Dropdown text='DokumentStatus'
                                                   options={documentStatusOptions}
                                                   labeled
                                                   selection/>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column>
                                    <Form.Input fluid label='Tittel' placeholder='Tittel'
                                                value={props.documentDescription.tittel}/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Form>
                    <Divider/>
                    <Tab menu={{fluid: true, vertical: true, tabular: true}} panes={panes}/>
                </Container>
            </Segment>
        </div>
    )
}
export default DocumentDescription;
