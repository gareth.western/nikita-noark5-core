import React, {useState} from 'react'
import {Button, Container, Divider, Header, Icon, Modal, Popup, Segment} from 'semantic-ui-react'
import DocumentDescription from "./DocumentDescription";
import NewDocument from "./NewDocument";

const DocumentDescriptionList = (props) => {

    const [documentDescriptions, setDocumentDescriptions] = useState(props.documentDescriptions);
    const [open, setOpen] = useState(false);

    const closeModal = () => {
        setOpen(false);
    }

    const addDocumentDescription = () => {
        props.doFilterSearch("");
    }


    const DisplayDocumentDescriptions = documentDescriptions.map(documentDescription => (
        <DocumentDescription key={documentDescription.dokumentnummer}
                             documentDescription={documentDescription}/>));

    return (
        <div>
            <Segment>
                <Container textAlign='left'>
                    <Header as='h3'>Alle dokumenter tilknyttet journalposten</Header>
                    <Popup
                        trigger={
                            <Modal
                                closeIcon
                                open={open}
                                trigger={<Button icon labelPosition='left' floated='right'>
                                    <Icon name='plus'/>
                                    Legg til nytt dokument
                                </Button>}
                                onClose={() => setOpen(false)}
                                onOpen={() => setOpen(true)}
                            >
                                <Header icon='envelope' content='Legg dokument til journalpost'/>
                                <Modal.Content>
                                    <NewDocument
                                        registryEntry={props.registryEntry}
                                        closeModal={closeModal}
                                        addDocumentDescription={addDocumentDescription}/>
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button onClick={() => setOpen(false)}>
                                        <Icon name='remove'/> Avbryt
                                    </Button>
                                </Modal.Actions>
                            </Modal>
                        }
                        content='Last opp nytt dokumentet til journalposten'
                        size='small'
                    />
                    <Header as='h3'>&nbsp;</Header>
                </Container>
                <Divider/>
                <Container>
                    {DisplayDocumentDescriptions}
                </Container>
            </Segment>
        </div>
    )
}
export default DocumentDescriptionList;

