import React, {useContext, useState} from "react";
import {Button, Form, Grid, Icon} from "semantic-ui-react";
import AuthContext from "../../store/AuthContext";

const NewDocumentVersion = (props) => {

    const [file, setFile] = useState();
    const [filename, setFilename] = useState('');
    const [mimeType, setMimeType] = useState('');

    const urlDocumentUpload = props.url;

    const token = useContext(AuthContext).token;
    const requestOptionsUploadDocument = () => {
        if (mimeType === undefined) {
            setMimeType('application/x-www-form-urlencoded');
        }
        return {
            method: 'POST',
            headers: {
                'Content-Type': mimeType,
                'Authorization': token
            },
            body: file
        }
    };

    /**
     * There are three POST requests that happen here.
     * The first fetch creates a documentDescription
     * The second fetch creates a DocumentObject
     * The third fetch uploads the document associated with the DocumentObject
     *
     * @param event The click event
     */
    const handleUpload = event => {
        event.preventDefault();

        fetch(urlDocumentUpload, requestOptionsUploadDocument())
            .then(response => {
                return response.json();
            }).then(data => {
            props.closeModal();
        })
            .catch(response => {
                console.log(response);
            })
    };

    const setChosenFile = (e) => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
        setMimeType(e.target.files[0].type);
    };

    return (
        <Grid>
            <Grid.Row>
                <Grid.Column width={4}>
                    <Button size="medium" as="label" htmlFor="file" type="button">
                        <Icon name="file"/> Velg fil
                    </Button>
                    <input type="file" id="file" hidden onChange={setChosenFile}/>
                </Grid.Column>
                <Grid.Column width={12}>
                    <Form.Input
                        label="Filnavn"
                        placeholder="Navnet på filen som skal lastes opp"
                        readOnly
                        value={filename}
                    />
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column textAlign="right">
                    <Button color='green' onClick={handleUpload}>
                        <Icon name='plus'/> Last opp
                    </Button>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    );
}

export default NewDocumentVersion;
