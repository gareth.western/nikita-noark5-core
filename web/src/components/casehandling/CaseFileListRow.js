import React, {useCallback} from 'react';
import {Table} from 'semantic-ui-react'
import {format, parseISO} from 'date-fns'
import {useNavigate} from "react-router-dom";

function CaseFileListRow(props) {

    const navigate = useNavigate();
    const handleOnClick = useCallback(urlCasefile => navigate('/saksbehandler/saksmappe',
        {
            state: {
                caseFile: props.caseFile,
                role: props.role
            }
        }), [navigate]);

    return (
        <Table.Row onClick={() => handleOnClick(props.caseFile)}>
            <Table.Cell key={props.caseFile.mappeID}>{props.caseFile.mappeID}</Table.Cell>
            <Table.Cell>{format(parseISO(props.caseFile.saksdato), 'dd/MM/yyyy')}</Table.Cell>
            <Table.Cell>{props.caseFile.tittel}</Table.Cell>
            <Table.Cell>{props.caseFile.saksstatus.kodenavn}</Table.Cell>
        </Table.Row>
    );
}

export default CaseFileListRow;
