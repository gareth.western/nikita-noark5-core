import {Divider, Menu} from "semantic-ui-react";


function CaseHandlerVerticalMenu() {

    return (
        <Menu vertical borderless fluid text>
            <Menu.Item active as="a">Overview </Menu.Item>
            <Menu.Item as="a">Reports</Menu.Item>
            <Menu.Item as="a">Analytics</Menu.Item>
            <Menu.Item as="a">Export</Menu.Item>
            <Divider hidden/>
            <Menu.Item as="a">Nav item</Menu.Item>
        </Menu>
    )
}

export default CaseHandlerVerticalMenu;
