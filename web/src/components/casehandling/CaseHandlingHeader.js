import {Grid, Header} from "semantic-ui-react";

const CaseHandlingHeader = (props) => {

    if (props.headerTitle === undefined && typeof props.headerTitle == 'undefined') {
        return (<Grid.Row></Grid.Row>)
    } else return (
        <Grid.Row>
            <Grid.Column textAlign='left'>
                <Header dividing size="large" as="h1">
                    {props.headerTitle}
                </Header>
            </Grid.Column>
        </Grid.Row>
    );
};

export default CaseHandlingHeader;
