import React, {useContext, useState} from "react";
import {Button, Form, Grid, Icon} from "semantic-ui-react";
import AuthContext from "../store/AuthContext";
import {CONTENT_TYPE_NOARK, LINKS, NEW_CASE_FILE_REL} from "../../constants";
import AppContext from "../store/AppContext";

const NewCaseFile = (props) => {

    const auth = useContext(AuthContext);
    const app = useContext(AppContext);

    const [title, setTitle] = useState('');
    const [publicTitle, setPublicTitle] = useState('');
    const [description, setDescription] = useState('');

    const copyTitle = () => {
        setPublicTitle(title);
    };

    function HandleSubmit(event) {
        event.preventDefault();

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': CONTENT_TYPE_NOARK,
                'Authorization': auth.token
            },
            body: JSON.stringify(
                {
                    'tittel': title,
                    'offentligTittel': publicTitle,
                    'beskrivelse': description,
                    "saksstatus": {"kode": "B", "kodenavn": "Under behandling"},
                    "saksansvarlig": props.responsible
                })
        };

        fetch(app.defaultSeries[LINKS][NEW_CASE_FILE_REL].href, requestOptions)
            .then(response => {
                return response.json();
            }).then(data => {
            props.updateCaseFileList(data);
            props.closeModal();
        })
            .catch(response => {
                console.log(response);
            })
    }

    return (
        <Form>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.Input fluid label='Tittel' placeholder='Tittel'
                                    value={title} onChange={(e) => setTitle(e.target.value)}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.Input fluid label='OffentligTittel' placeholder='Offentlig Tittel'
                                    value={publicTitle}
                                    action={{
                                        icon: 'copy',
                                        onClick: () => copyTitle()
                                    }}
                                    onChange={(e) => setPublicTitle(e.target.value)}
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form.TextArea fluid label='Beskrivelse' placeholder='Besrkivelse'
                                       value={description} onChange={(e) => setDescription(e.target.value)}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column textAlign='right'>
                        <Button color='green' onClick={HandleSubmit}>
                            <Icon name='plus'/> Opprett
                        </Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Form>
    );
}

export default NewCaseFile;
