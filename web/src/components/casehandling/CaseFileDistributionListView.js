import React from "react";

import {Grid, Input} from "semantic-ui-react";
import CaseHandlingHeader from "./CaseHandlingHeader";
import CaseFileDistributionList from "./CaseFileDistributionList";

const CaseFileDistributionListView = (props) => {

    const [open, setOpen] = React.useState(false);
    const caseFiles = props.caseFiles;

    const closeModal = () => {
        setOpen(false);
    }

    function doFilterSearch(value) {
        props.doSearch(value);
    }

    return (
        <Grid>
            <Grid.Column width={13}>
                <Grid padded>
                    <CaseHandlingHeader header={props.headerTitle}/>
                    <Grid.Row>
                        <Grid.Column width={1}>
                            <i className="filter large icon"/>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Input placeholder="Søk..."
                                   size="small"
                                   onChange={(e) => doFilterSearch(e.target.value)}/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <CaseFileDistributionList
                            updateCaseFileList={props.updateCaseFileList}
                            caseFiles={caseFiles}/>
                    </Grid.Row>
                </Grid>
            </Grid.Column>
        </Grid>
    )
}

export default CaseFileDistributionListView;
