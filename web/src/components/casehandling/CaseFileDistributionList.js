import React from 'react';
import {Table} from 'semantic-ui-react'

import CaseFileDistributionListRow from "./CaseFileDistributionListRow";
import CaseFileDistributionListHeader from "./CaseFileDistributionListHeader";

/**
 * Creates a Card that contains a table with header and row values.
 *
 *
 * @param props Takes in a List of relevant CaseFiles
 * @returns {JSX.Element}
 * @constructor
 */
function CaseFileDistributionList(props) {

    const caseFiles = props.caseFiles.map(
        (caseFile) => {
            return (
                <CaseFileDistributionListRow key={caseFile.systemID}
                                             caseFile={caseFile}
                                             updateCaseFileList={props.updateCaseFileList}/>
            )
        }
    )

    return (
        <Table celled padded>
            <CaseFileDistributionListHeader/>
            <Table.Body>
                {caseFiles}
            </Table.Body>
        </Table>
    );
}

export default CaseFileDistributionList;
