import React from "react";
import {Table} from 'semantic-ui-react'

/**
 * Returns a row of html that can create a header row of a table for a CaseFile.
 *
 * @returns {JSX.Element}
 * @constructor
 */
const CaseFileListHeader = () => {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>Saksnr</Table.HeaderCell>
                <Table.HeaderCell>Saksdato</Table.HeaderCell>
                <Table.HeaderCell>Tittel</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    );
}

export default CaseFileListHeader;
