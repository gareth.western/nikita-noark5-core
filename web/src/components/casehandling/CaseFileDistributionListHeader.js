import React from "react";
import {Table} from 'semantic-ui-react'

/**
 * Returns a row of html that can create a header row of a table for a CaseFile.
 *
 * @returns {JSX.Element}
 * @constructor
 */
const CaseFileDistributionListHeader = () => {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>Saksnr</Table.HeaderCell>
                <Table.HeaderCell>Saksdato</Table.HeaderCell>
                <Table.HeaderCell>Tittel</Table.HeaderCell>
                <Table.HeaderCell>Fordel</Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    );
}

export default CaseFileDistributionListHeader;
