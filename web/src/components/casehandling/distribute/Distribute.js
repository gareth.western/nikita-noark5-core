import React, {useContext, useState} from "react";
import {Button, Grid, Icon, Input, List} from "semantic-ui-react";
import AuthContext from "../../store/AuthContext";
import {CONTENT_TYPE_NOARK, LINKS, SELF} from "../../../constants";
import axios from "axios";

const Distribute = (props) => {

    const auth = useContext(AuthContext);
    const [eTag, setETag] = useState('');

    async function HandleUpdate() {

        const caseFile = props.casefile;
        caseFile['saksansvarlig'] = auth.user;

        let config = {
            headers: {
                'Accept': CONTENT_TYPE_NOARK,
                'Authorization': auth.token
            }
        };

        await axios.get(props.casefile[LINKS][SELF].href, config)
            .then((response) => {
                let et = response.headers["etag"];
                setETag(et);
            }).catch(error => {
                console.log(error.message);
            });

        config['headers']['ETAG'] = eTag;
        config['headers']['Content-type'] = CONTENT_TYPE_NOARK;

        await axios.put(props.casefile[LINKS][SELF].href, JSON.stringify(props.casefile), config)
            .then(() => {
                props.closeModal();
            }).catch(error => {
                console.log(error.message);
            });
    }

    return (
        <Grid>
            <Grid.Row>
                <Grid.Column width={1}>
                    <i className="filter large icon"/>
                </Grid.Column>
                <Grid.Column width={3}>
                    <Input placeholder="Søk..."
                           size="small"/>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <List link>
                    <List.Item active>User is : {auth.user}</List.Item>
                </List>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column textAlign='right'>
                    <Button color='green' onClick={HandleUpdate}>
                        <Icon name='plus'/> Fordel til
                    </Button>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    );
}

export default Distribute;
