import React from 'react';
import {Table} from 'semantic-ui-react'

import CaseFileListRow from './CaseFileListRow';
import CaseFileListHeader from './CaseFileListHeader';

/**
 * Creates a Card that contains a table with header and row values.
 *
 *
 * @param props Takes in a List of relevant CaseFiles
 * @returns {JSX.Element}
 * @constructor
 */
function CaseFileList(props) {

    const caseFiles = props.caseFiles.map(
        (caseFile) => {
            return (
                <CaseFileListRow key={caseFile.systemID} role={props.role} caseFile={caseFile}/>
            )
        }
    )

    return (
        <Table celled padded>
            <CaseFileListHeader/>
            <Table.Body>
                {caseFiles}
            </Table.Body>
        </Table>
    );
}

export default CaseFileList;
