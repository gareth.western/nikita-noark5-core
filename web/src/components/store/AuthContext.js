import React, {createContext, useState} from "react";

const AuthContext = createContext({
        token: '',
        user: '',
        organisation: '',
        isLoggedIn: false,
    }
);

const calcRemainingTime = (expirationTime) => {
    const remainingTime = (new Date(expirationTime).getTime()) - (new Date().getTime());
    console.log("Setting logout in " + remainingTime + "ms")
    return remainingTime;
}

export const AuthContextProvider = (props) => {
    const [token, setToken] = useState(localStorage.getItem('token'));
    const [user, setUser] = useState(localStorage.getItem('user'));
    const [organisation, setOrganisation] = useState(localStorage.getItem('organisation'));
    const userLoggedIn = !!token;

    const logoutHandler = () => {
        setToken(null);
        setUser(null);
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        localStorage.removeItem('organisation');
    }

    const loginHandler = (username, token, expirationTime) => {
        setToken(token);
        localStorage.setItem('token', token);
        setUser(username);
        localStorage.setItem('user', username);
        setTimeout(logoutHandler, calcRemainingTime(expirationTime))
    }

    const organisationHandler = (organisation) => {
        setOrganisation(organisation);
        localStorage.setItem('organisation', organisation);
    }

    const contextValue = {
        token: token,
        user: user,
        organisation: organisation,
        isLoggedIn: userLoggedIn,
        login: loginHandler,
        logout: logoutHandler,
        setOrganisation: organisationHandler
    };

    return (
        <AuthContext.Provider value={contextValue}>
            {props.children}
        </AuthContext.Provider>
    );
};

export default AuthContext;
