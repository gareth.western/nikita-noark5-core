import React, {createContext, useState} from "react";

const AppContext = createContext({
        fondsLinks: '',
        caseHandlingLinks: '',
        defaultSeries: '',
        postalSeries: '',
        resultsPerPage: '10',
    }
);

export const AppContextProvider = (props) => {

    const [caseHandlingLinks, setCaseHandlingLinks] = useState(JSON.parse(localStorage.getItem('caseHandlingLinks')));
    const [fondsLinks, setFondsLinks] = useState(JSON.parse(localStorage.getItem('fondsLinks')));
    const [defaultSeries, setDefaultSeries] = useState(JSON.parse(localStorage.getItem('defaultSeries')));
    const [postalSeries, setPostalSeries] = useState(JSON.parse(localStorage.getItem('postalSeries')));
    const [resultsPerPage] = useState('10');

    const fondsLinksHandler = (links) => {
        setFondsLinks(links);
        localStorage.setItem('fondsLinks', JSON.stringify(links));
    }

    const caseHandlingLinksHandler = (links) => {
        setCaseHandlingLinks(links);
        localStorage.setItem('caseHandlingLinks', JSON.stringify(links));
    }

    const defaultSeriesHandler = (series) => {
        setDefaultSeries(series);
        localStorage.setItem('defaultSeries', JSON.stringify(series));
    }

    const postalSeriesHandler = (series) => {
        setPostalSeries(series);
        localStorage.setItem('postalSeries', JSON.stringify(series));
    }

    const appContextValue = {
        caseHandlingLinks: caseHandlingLinks,
        fondsLinks: fondsLinks,
        defaultSeries: defaultSeries,
        postalSeries: postalSeries,
        resultsPerPage: resultsPerPage,
        setDefaultSeries: defaultSeriesHandler,
        setPostalSeries: postalSeriesHandler,
        setFondsLinks: fondsLinksHandler,
        setCaseHandlingLinks: caseHandlingLinksHandler,
    };

    return (
        <AppContext.Provider value={appContextValue}>
            {props.children}
        </AppContext.Provider>
    );
};

export default AppContext;
