import React from 'react'
import {Feed, Header, Icon} from 'semantic-ui-react'

const CaseHandlerTaskFeed = (props) => {

    const displayDocumentFlow = props.documentFlowCaseHandler.map(
        (documentFlow) => {
            return (
                <Feed.Event key={documentFlow.systemID}>
                    <Feed.Label>
                        <Icon name='pencil'/>
                    </Feed.Label>
                    <Feed.Content>
                        <Feed.Date>{documentFlow.flytMottattDato}</Feed.Date>
                        <Feed.Summary>
                            Du har fått en ny sak. {documentFlow.flytMerknad} fra {documentFlow.flytFra}
                        </Feed.Summary>
                    </Feed.Content>
                </Feed.Event>
            )
        }
    )
    return (
        <div>
            <Header dividing size="medium" as="h1">
                Tildelte oppgaver
            </Header>
            <Feed>
                {displayDocumentFlow}
            </Feed>
        </div>
    )
}

export default CaseHandlerTaskFeed;
