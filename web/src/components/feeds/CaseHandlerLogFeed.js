import React from 'react'
import {Feed, Header, Icon} from 'semantic-ui-react'

const CaseHandlerLogFeed = () => (
    <div>
        <Header dividing size="medium" as="h1">
            Logg
        </Header>
        <Feed>
            <Feed.Event>
                <Feed.Label>
                    <Icon name='pencil'/>
                </Feed.Label>
                <Feed.Content>
                    <Feed.Date>I går</Feed.Date>
                    <Feed.Summary>
                        Hans Hansen godekjente dokument 2022/22-1000
                    </Feed.Summary>
                </Feed.Content>
            </Feed.Event>
            <Feed.Event>
                <Feed.Label>
                    <Icon name='address card'/>
                </Feed.Label>
                <Feed.Content>
                    <Feed.Date>I dag</Feed.Date>
                    <Feed.Summary>
                        Maria Hansen åpnet saksmappe 2022/21
                    </Feed.Summary>
                </Feed.Content>
            </Feed.Event>

            <Feed.Event>
                <Feed.Label>
                    <Icon name='pencil'/>
                </Feed.Label>
                <Feed.Content>
                    <Feed.Date>I går</Feed.Date>
                    <Feed.Summary>
                        Du opprettet en sak med saksnummer <a>2022/0002</a>
                    </Feed.Summary>
                </Feed.Content>
            </Feed.Event>
            <Feed.Event>
                <Feed.Label>
                    <Icon name='address card'/>
                </Feed.Label>
                <Feed.Content>
                    <Feed.Date>I dag</Feed.Date>
                    <Feed.Summary>
                        Du la til Maria Hansen som korrespondansepart
                    </Feed.Summary>
                </Feed.Content>
            </Feed.Event>
        </Feed>
    </div>
)

export default CaseHandlerLogFeed;
