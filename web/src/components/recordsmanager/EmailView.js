import {Grid, Input, Label, Segment, TextArea} from "semantic-ui-react";
import AttachmentList from './AttachmentList';

const EmailView = (props) => {

    return (
        <Grid>
            <Grid.Column width={1}/>
            <Grid.Column width={14}>
                <Grid padded>
                    <Grid.Row>
                        <Segment padded>
                            <Label attached='top'>Fra</Label>
                            <Input
                                placeholder='navn'
                                value={'Fra'}/>
                        </Segment>
                    </Grid.Row>
                    <Segment padded>
                        <Label attached='top'>Tittel</Label>
                        <Input
                            value={props.file.tittel}/>
                    </Segment>
                    <Grid.Row>
                    </Grid.Row>
                    <Grid.Row>
                        <Segment padded>
                            <Label attached='top'>Dato</Label>
                            <Input
                                value={props.file.opprettetDato}/>
                        </Segment>
                    </Grid.Row>
                    <Grid.Row>
                        <Segment padded>
                            <Label attached='top'>Innhold</Label>
                            <TextArea></TextArea>
                        </Segment>
                    </Grid.Row>
                    <Grid.Row>
                        <header>Vedlegg</header>
                        <AttachmentList documentObjects={props.documentObjects}/>
                    </Grid.Row>
                </Grid>
            </Grid.Column>
            <Grid.Column width={1}/>
        </Grid>
    )
};

export default EmailView;
