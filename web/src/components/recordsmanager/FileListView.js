import {Grid, Input} from "semantic-ui-react";
import FileList from "../file/FileList";

const FileListView = (props) => {
    return (
        <Grid>
            <Grid.Row>
                <Grid.Column width={13}>
                    <Grid padded>
                        <Grid.Column width={1}>
                            <i className="filter large icon"/>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Input placeholder="Søk..."
                                   size="small"
                                   onChange={(e) => props.doSearch(e.target.value)}/>
                        </Grid.Column>
                    </Grid>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column>
                    <Grid padded>
                        <Grid.Row>
                            <FileList
                                files={props.files}
                                updateCaseFileList={props.updateCaseFileList}/>
                        </Grid.Row>
                    </Grid>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
};

export default FileListView;
