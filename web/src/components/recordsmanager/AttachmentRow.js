import {Input, Table} from "semantic-ui-react";
import {DOCUMENT_FILE_REL, LINKS} from "../../constants";
import {useContext} from "react";
import AuthContext from "../store/AuthContext";

const AttachmentRow = (props) => {

    const auth = useContext(AuthContext);

    function downloadFile(documentObject) {
        let urlDocument = documentObject[LINKS][DOCUMENT_FILE_REL].href;
        fetch(urlDocument, {
            headers: {
                'Authorization': auth.token
            }
        }).then(response => {
                response.blob().then(blob => {
                    let a = document.createElement("a");
                    a.setAttribute('target', '_blank');
                    a.href = window.URL.createObjectURL(blob);
                    a.download = documentObject.filnavn;
                    const tab = window.open();
                    tab.location.href = a.href;
                });
            }
        ).catch(error => {
            console.log(error);
        });
    }

    return (
        <Table.Row onClick={() => downloadFile(props.documentObject)}>
            <Table.Cell width={12}>
                <Input disabled={true} value={props.documentObject.filnavn}/>
            </Table.Cell>
            <Table.Cell width={4}>
                <Input disabled={true} value={props.documentObject.filstoerrelse}/>
            </Table.Cell>
        </Table.Row>
    )
}

export default AttachmentRow;
