import React from "react";
import {Table} from 'semantic-ui-react'

const AttachmentHeader = () => {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>Filnavn</Table.HeaderCell>
                <Table.HeaderCell>Filstørrelse</Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    );
}

export default AttachmentHeader;
