import {Table} from "semantic-ui-react";
import React from "react";
import AttachmentHeader from "./AttachmentHeader";
import AttachmentRow from "./AttachmentRow";

const AttachmentList = (props) => {

    const documents = props.documentObjects.map(
        (documentObject) => {
            return (
                <AttachmentRow
                    key={documentObject.systemID}
                    documentObject={documentObject}/>
            )
        }
    )

    return (
        <Table celled padded>
            <AttachmentHeader/>
            <Table.Body>
                {documents}
            </Table.Body>
        </Table>
    )
}
export default AttachmentList;
