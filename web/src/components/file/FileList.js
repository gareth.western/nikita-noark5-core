import React from 'react';
import {Table} from 'semantic-ui-react'

import FileListRow from "./FileListRow";
import FileListHeader from "./FileListHeader";

function FileList(props) {

    const files = props.files.map(
        (file) => {
            return (
                <FileListRow
                    key={file.systemID}
                    file={file}
                    updateCaseFileList={props.updateCaseFileList}/>
            )
        }
    )

    return (
        <Table celled padded>
            <FileListHeader/>
            <Table.Body>
                {files}
            </Table.Body>
        </Table>
    );
}

export default FileList;
