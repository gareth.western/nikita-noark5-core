import React, {useContext, useEffect, useState} from 'react';
import {Button, Header, Icon, Modal, Table} from 'semantic-ui-react'
import {format, parseISO} from 'date-fns'
import EmailView from "../recordsmanager/EmailView";
import NewCaseFileImport from "../casehandling/NewCaseFileImport";
import {CONTENT_TYPE_NOARK, DOCUMENT_DESCRIPTION_REL, DOCUMENT_OBJECT_REL, LINKS, RECORD_REL} from "../../constants";
import axios from "axios";
import AuthContext from "../store/AuthContext";

const FileListRow = (props) => {

    const auth = useContext(AuthContext);
    const [mailOpen, setMailOpen] = useState(false)
    const [newCaseFileOpen, setNewCaseFilelOpen] = useState(false)
    const [documentObjects, setDocumentObjects] = useState([]);
    const [tempDocumentObjects, setTempDocumentObjects] = useState([]);

    const closeNewCaseFileModal = () => {
        setNewCaseFilelOpen(false);
        props.updateCaseFileList();
    }

    const config = {
        headers: {
            'Accept': CONTENT_TYPE_NOARK,
            'Authorization': auth.token
        }
    };

    useEffect(() => {
        setDocumentObjects([]);
        axios.get(props.file[LINKS][RECORD_REL].href, config)
            .then((response) => {
                if (response.data.results !== undefined && response.data.count > 0) {
                    const record = response.data.results[0];
                    axios.get(record[LINKS][DOCUMENT_DESCRIPTION_REL].href, config)
                        .then((response) => {
                            const documentDescriptions = response.data.results;
                            for (let i = 0; i < documentDescriptions.length; i++) {
                                const documentObjectsUrlString = documentDescriptions[i][LINKS][DOCUMENT_OBJECT_REL].href;
                                axios.get(documentObjectsUrlString, config)
                                    .then((response) => {
                                        if (documentObjects === []) {
                                            setDocumentObjects(response.data.results);
                                        } else {
                                            setDocumentObjects([...documentObjects, response.data.results]);
                                            //tempDocumentObjects.push(response.data.results);
                                            //setDocumentObjects(tempDocumentObjects);
                                        }
                                    }).catch(error => {
                                    console.log(error.message);
                                });
                            }
                        }).catch(error => {
                        console.log(error.message);
                    });
                }
            }).catch(error => {
            console.log(error.message);
        });
    }, []);

    return (
        <Table.Row>
            <Table.Cell>{format(parseISO(props.file.opprettetDato), 'dd/MM/yyyy')}</Table.Cell>
            <Modal
                onClose={() => setMailOpen(false)}
                onOpen={() => setMailOpen(true)}
                open={mailOpen}
                trigger={
                    <Table.Cell>{props.file.tittel}</Table.Cell>
                }
            >
                <Header icon='envelope' content='Se innhold i epost'/>
                <Modal.Content>
                    <EmailView file={props.file} documentObjects={documentObjects[0]}/>
                </Modal.Content>
                <Modal.Actions>
                </Modal.Actions>
            </Modal>
            <Table.Cell>
                <Modal
                    onClose={() => setNewCaseFilelOpen(false)}
                    onOpen={() => setNewCaseFilelOpen(true)}
                    open={newCaseFileOpen}
                    trigger={
                        <Button icon>
                            <Icon name='plus'/>
                            Ny saksmappe
                        </Button>
                    }
                >
                    <Header icon='envelope' content='Opprett ny saksmappe'/>
                    <Modal.Content>
                        <NewCaseFileImport
                            file={props.file}
                            closeModal={closeNewCaseFileModal}
                            updateCaseFileList={props.updateCaseFileList}/>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button onClick={() => setNewCaseFilelOpen(false)}>Avbryt</Button>
                    </Modal.Actions>
                </Modal>
            </Table.Cell>
        </Table.Row>
    );
}

export default FileListRow;
