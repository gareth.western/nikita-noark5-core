import {Button, Grid, Header, Icon, Input, Modal, Popup} from "semantic-ui-react";
import NewCaseFile from "../casehandling/NewCaseFile";

const SearchHeader = (props) => {

    if (props.headerTitle === undefined && typeof props.headerTitle == 'undefined') {
        return (
            <Grid.Row>
                <Grid.Column width={1}>
                    <i className="filter large icon"/>
                </Grid.Column>
                <Grid.Column width={3}>
                    <Input placeholder="Søk..." size="small"/>
                </Grid.Column>
            </Grid.Row>
        )
    } else {
        return (

            <Grid.Row>
                <Grid.Column width={1}>
                    <i className="filter large icon"/>
                </Grid.Column>
                <Grid.Column width={3}>
                    <Input placeholder="Søk..." size="small"/>
                </Grid.Column>
                <Grid.Column width={3}>
                    <Popup
                        trigger={
                            <Modal
                                closeIcon
                                //open={open}
                                trigger={<Button icon>
                                    <Icon name='plus'/>
                                    Ny saksmappe
                                </Button>}
                                //onClose={() => setOpen(false)}
                                //onOpen={() => setOpen(true)}
                            >
                                <Header icon='folder open outline' content='Opprett Ny Saksmappe'/>
                                <Modal.Content>
                                    <NewCaseFile/>
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button>
                                        <Icon name='remove'/> Avbryt
                                    </Button>
                                </Modal.Actions>
                            </Modal>
                        }
                        content='Opprett Ny Saksmappe'
                        size='small'
                    />
                </Grid.Column>
            </Grid.Row>
        )
    }
};

export default SearchHeader;
