import React from "react";
import {Table} from 'semantic-ui-react'

/**
 * Returns a row of html that can create a header row of a table for a File.
 *
 * @returns {JSX.Element}
 * @constructor
 */
const FileListHeader = () => {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>Dato</Table.HeaderCell>
                <Table.HeaderCell>Tittel</Table.HeaderCell>
                <Table.HeaderCell>Importer</Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    );
}

export default FileListHeader;
