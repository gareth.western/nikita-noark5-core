# nikita-gui

Nikita-gui is a semantic-ui / react front end for the nikita back-end. It should work with any Noark 5 API
implementation.

## Initial

Check the version of npm you are using. We have built with version 8.1.2. Run

> npm version

to see which version you have installed. Intellij seems to have version 6.x installed, and it causes problems. So it is
worthwhile checking the version before starting. Initial download of dependencies can be undertaken using

> npm install

### Known issues

There is a known (from externally) issue where two semicolons `;;` cause a problem in a file called `semantic.css`. This
kills the build process.

`data:application/x-font-ttf;charset=utf-8;;base64`

The simple fix to this is to locate the files `semantic.css` and `semantic.min.css` and remove the extra semicolon. This
problem should go away eventually with updates. After you have updated this you have to delete the node_modules cache.

> rm -rf node_modules/.cache

It is
not [acceptable](https://wptavern.com/german-court-fines-website-owner-for-violating-the-gdpr-by-using-google-hosted-fonts)
to force end-users download from CDN without their prior consent. Semantic-UI has
an [in-built](https://github.com/Semantic-Org/Semantic-UI/issues/7075) automatic downloading of a font hosted by google.
This needs to be replaced with a self hosted version.

Replace CDN with downloaded fonts file. In the file `semantic.css` there is an autolink to download fonts from google.
This should be self-hosted.

> @import url('https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin');

## Building / running

From the project directory, you can run the application in development mode or build a production release.

### Building in development mode

> npm start

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes. You may also see any lint errors in the console.

### Building

> npm run build

Builds nikita-gui for production to the `build` folder. React is bundled in production mode and the build is optimized
for performance. The build is minified and the filenames include the hashes. nikita-gui should be ready for deployment.

#### Building for webserver

To build the GUI for deploying to apache instance (how we use it a OsloMet).

##### Step 1

Locate any references to CDN downloads and make sure they can be hosted locally

##### Step 2

Edit LoginPage.js and make sure URL root is correct e.g.,

> const apiUrl = "https://nikita.oslomet.no/noark5v5/";

##### Step 3

Edit package.js and add what homepage the app is using :

> "homepage": "https://nikita.oslomet.no/gui",

##### Step 5

run `npm run build`

The contents of the `build` directory can now be copied to the correct apache location (in our case the directory called
gui)

## Future work

This is the first step in replacing the angular 1.x codebase we had. The bootstrap GUI from the earlier version of the
gui was also lacking, so it was decided to rebuild everything. What I did not realise is that semantic-ui is, for all
intents and purposes, dead. A community fork called [fomantic-ui](https://fomantic-ui.com/) has continued development,
but it does not have a good react extension in the same way that semantic-ui has. Small things like a simple date picker
are missing in semantic-ui. It really is strange how there are so many articles on the web advocating for semantic-ui in
its current state. Given time, I would like to swap out the semantic-ui dependency with the fomantic-ui dependency, so
we have more ui elements available to us.

### browser.html

For a long time we had a browser.html available that allowed us to start at the application root and traverse our way
through the core. I would like a new version of browser.html that achieves this, but that also supports a query
parameter systemID that will give you back any object you can identify in the core (subject to authorisation). It would
work like this:

> browser.html?systemID=f63d8dfe-449d-4891-8f66-9337a9cb5a48

Once an object has been retrieved, it should be possible to apply OData queries to any templated links from within
browser.html

### Missing ui-elements

There are a number of UI elements missing e.g. CorrespondencePartUnit and CorrespondencePartInternal

### Missing functionality

Tha UI doesn't properly show a RecordNote on a CaseFile pages. It just shows RegistryEntries.

We need to fix the login screen and show a message when a user login attempt fails.

## Comment

We are leaving the GUI attached to the main source for the moment. We may pull to GUI code out to its own project later.
I would much rather seperate backend from frontend as we likely will have lots of dependency security warnings about js
packages that are out of date.
