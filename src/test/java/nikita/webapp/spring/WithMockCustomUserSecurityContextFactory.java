package nikita.webapp.spring;

import nikita.common.model.noark5.v5.admin.Organisation;
import nikita.common.repository.n5v5.admin.IOrganisationRepository;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.List;
import java.util.UUID;

public class WithMockCustomUserSecurityContextFactory
        implements WithSecurityContextFactory<WithMockCustomUser> {

    private final IOrganisationRepository organisationRepository;

    public WithMockCustomUserSecurityContextFactory(
            IOrganisationRepository organisationRepository) {
        this.organisationRepository = organisationRepository;
    }

    @Override
    public SecurityContext createSecurityContext(WithMockCustomUser customUser) {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        Organisation organisation = organisationRepository.findBySystemId(
                UUID.fromString("e23e9106-aab3-426c-ac7b-65c65bfc1a85"));
        CustomUserDetails principal = new CustomUserDetails(organisation);
        securityContext.setAuthentication(new
                AnonymousAuthenticationToken("admin@example.com", principal,
                List.of(new SimpleGrantedAuthority("RECORDS_MANAGER"))));
        return securityContext;
    }
}
