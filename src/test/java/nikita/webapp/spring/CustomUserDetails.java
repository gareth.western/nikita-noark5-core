package nikita.webapp.spring;

import nikita.common.model.noark5.v5.admin.Organisation;
import nikita.common.model.noark5.v5.admin.User;
import nikita.webapp.spring.security.INikitaUserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class CustomUserDetails
        implements INikitaUserDetails {

    private final Organisation organisation;

    public CustomUserDetails(Organisation organisation) {
        this.organisation = organisation;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("RECORDS_MANAGER"));
    }

    @Override
    public String getPassword() {
        return "password";
    }

    @Override
    public String getUsername() {
        return "admin@example.com";
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Organisation getOrganisation() {
        return organisation;
    }

    @Override
    public User getUser() {
        return null;
    }

    public UUID getSystemId() {
        return getUser().getSystemId();
    }
}
