package nikita.webapp.general;

import nikita.webapp.spring.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static nikita.common.config.Constants.NEW_DOCUMENT_FLOW;
import static nikita.common.config.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static nikita.common.config.HATEOASConstants.SELF;
import static nikita.common.config.N5ResourceMappings.*;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static utils.DocumentFlowCreator.createDocumentFlowAsJSON;
import static utils.DocumentFlowCreator.createUpdatedDocumentFlowAsJSON;

/**
 * DocumentFlows are used to send documents from one person to another. A
 * typical use-case is that an incoming letter is sent from the Records
 * manager to a leader. The leader then sends the document to a case handler.
 * <p>
 * A DocumentFlow is associated with either a RecordNote or a RegistryEntry.
 * A DocumentFlow is also associated with a minimum of two users. The first
 * is the user that initiates a DocumentFlow (the from-user), the second is
 * the receiver of a DocumentFlow (the to-user)
 * <p>
 * Note: It is not clear why a document is sent rather than a reference to
 * for example a casefile.
 * A DocumentFlow does not support multiple to-user or from-user associations.
 * <p>
 * The following tests show the document flow functionality that nikita
 * supports.
 * 1. Possible to create a DocumentFlow
 */

public class DocumentFlowTest
        extends BaseTest {

    /**
     * Check that it is possible to add a DocumentFlow to a RegistryEntry
     * <p>
     * An incoming DocumetFlow will have a JSON that adheres to the following
     * format:
     * {
     * 'flytTil' : 'casehandler@example.com',
     * 'referanseFlytTil' : 'a63a38c5-58d4-4346-8e4d-279889c13b3b',
     * 'flytFra' : 'admin@example.com',
     * 'referanseFlytFra' : '7c33ed39-4c87-41a4-8ca1-e6a4d021bdca',
     * 'flytMottattDato' : '',
     * 'flytSendtDato' : '',
     * 'flytStatus' : {
     * 'kode': 'F',
     * 'kodenavn': 'Til fordeling'
     * },
     * 'flytMerknad' : 'A comment about the document'
     * }
     * <p>
     * In this case the records' manager creates a document flow associated with
     * a registryEntry that is to be sent to a casehandler.
     *
     * @throws Exception Serialising or validation exception
     */
    @Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/document_flow.sql"})
    @WithMockCustomUser
    public void addDocumentFlowToRegistryEntry() throws Exception {
        String urlNewDocumentFlow = "/noark5v5/api/sakarkiv/" +
                REGISTRY_ENTRY + "/8f6b084f-d727-4b46-bbe2-14bed2135fa9/" +
                NEW_DOCUMENT_FLOW;

        // Create an DocumentFlow object associated with the RegistryEntry
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewDocumentFlow)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createDocumentFlowAsJSON()));
        resultActions.andExpect(status().isCreated());
        printDocumentation(resultActions);

        // Retrieve an identified DocumentFlow
        String urlDocumentFlow = getHref(SELF, resultActions);
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlDocumentFlow)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk());
    }

    @Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/document_flow.sql"})
    @WithMockCustomUser
    public void updateDocumentFlow() throws Exception {
        String urlDocumentFlow = "/noark5v5/api/sakarkiv/" + DOCUMENT_FLOW +
                "/cf0f41f7-65e8-4471-85f3-18ff223cbdb0";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlDocumentFlow)
                .contextPath(contextPath)
                // 1 because the inheritance used in database will increase the
                // version from 0 to 1
                .header("ETAG", "\"1\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createUpdatedDocumentFlowAsJSON()));
        resultActions.andExpect(status().isOk());

        printDocumentation(resultActions);
    }

    @Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/document_flow.sql"})
    @WithMockCustomUser
    public void deleteDocumentFlow() throws Exception {
        String urlDocumentFlow = "/noark5v5/api/sakarkiv/" + DOCUMENT_FLOW +
                "/cf0f41f7-65e8-4471-85f3-18ff223cbdb0";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(urlDocumentFlow)
                .contextPath(contextPath));
        resultActions.andExpect(status().isNoContent());
        printDocumentation(resultActions);
    }

    @Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/document_flow.sql"})
    @WithMockCustomUser
    public void searchDocumentFlowWithOData() throws Exception {

        String odata = "?$filter=contains(" + DOCUMENT_FLOW_FLOW_COMMENT + "," +
                " 'Great')";
        String urlDocumentFlow = contextPath + "/odata/api/sakarkiv/" +
                DOCUMENT_FLOW + odata;

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlDocumentFlow)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(1)))
                .andExpect(jsonPath("$.results[0]." + DOCUMENT_FLOW_FLOW_COMMENT)
                        .value("Great stuff"));
        printDocumentation(resultActions);
    }

    @Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/document_flow.sql"})
    @WithMockCustomUser
    public void searchDocumentFlowCodeAndDateWithOData() throws Exception {

        String odata = "?$filter=flytStatus/kode eq " +
                "'F'and flytTil eq 'flow_to_a'&$orderby=opprettetDato DESC";
        String urlDocumentFlow = contextPath + "/odata/api/sakarkiv/" +
                DOCUMENT_FLOW + odata;

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlDocumentFlow)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        System.out.println(response.getContentAsString());
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(1)))
                .andExpect(jsonPath("$.results[0]." + SYSTEM_ID)
                        .value("cf0f41f7-65e8-4471-85f3-18ff223cbdb0"));
        printDocumentation(resultActions);
    }

    @Test
    @Sql({"/db-tests/basic_structure.sql", "/db-tests/document_flow.sql"})
    @WithMockCustomUser
    public void getDocumentFlowAssociatedWithRegistryEntry() throws Exception {

        String urlDocumentFlow = "/noark5v5/api/sakarkiv/" +
                REGISTRY_ENTRY + "/8f6b084f-d727-4b46-bbe2-14bed2135fa9/" +
                DOCUMENT_FLOW;

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlDocumentFlow)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(3)));
        printDocumentation(resultActions);
    }
}
