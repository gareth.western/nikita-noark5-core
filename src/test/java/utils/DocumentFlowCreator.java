package utils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import nikita.common.model.noark5.v5.admin.User;
import nikita.common.model.noark5.v5.metadata.FlowStatus;
import nikita.common.model.noark5.v5.secondary.DocumentFlow;

import java.io.IOException;
import java.io.StringWriter;
import java.util.UUID;

import static nikita.common.config.N5ResourceMappings.*;
import static nikita.common.util.CommonUtils.Hateoas.Serialize.*;

public final class DocumentFlowCreator {

    /**
     * Create a default DocumentFlow for testing purposes. Two users are
     * created and associated with the document flow. A From user and a To user.
     *
     * @return a DocumentFlow with all values set
     */
    public static DocumentFlow createDocumentFlow() {
        User admin = new User();
        admin.setUsername("admin@example.com");
        admin.setSystemId(UUID.randomUUID());

        User recordKeeper = new User();
        recordKeeper.setSystemId(UUID.randomUUID());
        recordKeeper.setUsername("recordkeeper@example.com");

        DocumentFlow documentFlow = new DocumentFlow();
        documentFlow.setReferenceFlowFrom(admin);
        documentFlow.setFlowFrom(admin.getUsername());
        documentFlow.setReferenceFlowFromSystemID(UUID.randomUUID());
        documentFlow.setFlowTo(recordKeeper.getUsername());

        documentFlow.setReferenceFlowTo(recordKeeper);
        documentFlow.setReferenceFlowToSystemID(UUID.randomUUID());
        documentFlow.setFlowStatus(new FlowStatus("F", "Til fordeling"));
        return documentFlow;
    }

    public static String createUpdatedDocumentFlowAsJSON() throws IOException {
        DocumentFlow documentFlow = createDocumentFlow();
        documentFlow.setFlowComment("Added a comment to the flow!");
        return createDocumentFlowAsJSON(documentFlow);
    }

    public static String createDocumentFlowAsJSON() throws IOException {
        return createDocumentFlowAsJSON(createDocumentFlow());
    }

    public static String createDocumentFlowAsJSON(DocumentFlow documentFlow) throws IOException {

        JsonFactory factory = new JsonFactory();
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = factory.createGenerator(jsonWriter);
        jgen.writeStartObject();
        printNullable(jgen, DOCUMENT_FLOW_FLOW_FROM, documentFlow.getFlowFrom());
        printNullable(jgen, DOCUMENT_FLOW_REFERENCE_FLOW_FROM,
                documentFlow.getReferenceFlowFrom().getSystemId().toString());
        printNullable(jgen, DOCUMENT_FLOW_FLOW_TO, documentFlow.getFlowTo());
        printNullable(jgen, DOCUMENT_FLOW_REFERENCE_FLOW_TO,
                documentFlow.getReferenceFlowTo().getSystemId().toString());
        printNullable(jgen, DOCUMENT_FLOW_FLOW_COMMENT, documentFlow.getFlowComment());
        printNullableDateTime(jgen, DOCUMENT_FLOW_FLOW_RECEIVED_DATE,
                documentFlow.getFlowReceivedDate());
        printNullableDateTime(jgen, DOCUMENT_FLOW_FLOW_SENT_DATE,
                documentFlow.getFlowSentDate());
        printNullable(jgen, DOCUMENT_FLOW_FLOW_COMMENT, documentFlow.getFlowComment());
        jgen.writeObjectFieldStart(DOCUMENT_FLOW_FLOW_STATUS);
        printMetadataEntity(jgen, documentFlow.getFlowStatus());
        jgen.writeEndObject();
        jgen.writeEndObject();
        jgen.close();
        return jsonWriter.toString();
    }
}
