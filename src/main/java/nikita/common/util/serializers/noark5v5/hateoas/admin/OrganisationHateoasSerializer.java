package nikita.common.util.serializers.noark5v5.hateoas.admin;

import com.fasterxml.jackson.core.JsonGenerator;
import nikita.common.model.noark5.v5.admin.Organisation;
import nikita.common.model.noark5.v5.hateoas.HateoasNoarkObject;
import nikita.common.model.noark5.v5.interfaces.entities.INoarkEntity;
import nikita.common.util.serializers.noark5v5.hateoas.HateoasSerializer;
import nikita.common.util.serializers.noark5v5.hateoas.interfaces.IHateoasSerializer;

import java.io.IOException;

import static nikita.common.config.N5ResourceMappings.ORGANISATION_NAME;
import static nikita.common.config.N5ResourceMappings.ORGANISATION_STATUS;
import static nikita.common.util.CommonUtils.Hateoas.Serialize.*;

/**
 * Serialise an outgoing Organisation object as JSON.
 */
public class OrganisationHateoasSerializer
        extends HateoasSerializer
        implements IHateoasSerializer {

    @Override
    public void serializeNoarkEntity(
            INoarkEntity noarkSystemIdEntity,
            HateoasNoarkObject organisationHateoas,
            JsonGenerator jgen) throws IOException {

        Organisation organisation = (Organisation) noarkSystemIdEntity;

        jgen.writeStartObject();
        printSystemIdEntity(jgen, organisation);
        printCreateEntity(jgen, organisation);
        printFinaliseEntity(jgen, organisation);
        print(jgen, ORGANISATION_NAME,
                organisation.getOrganisationName());
        printNullable(jgen, ORGANISATION_STATUS,
                organisation.getOrganisationStatus());
        printHateoasLinks(jgen, organisationHateoas
                .getLinks(organisation));
        jgen.writeEndObject();
    }
}
