package nikita.common.util.deserialisers.admin;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import nikita.common.model.noark5.v5.admin.Organisation;
import nikita.common.util.exceptions.NikitaMalformedInputDataException;

import java.io.IOException;

import static nikita.common.config.ErrorMessagesConstants.MALFORMED_PAYLOAD;
import static nikita.common.config.N5ResourceMappings.*;
import static nikita.common.util.CommonUtils.Hateoas.Deserialize.*;

/**
 * Deserialise an incoming Organisation JSON object.
 * Note:
 * - Unknown property values in the JSON will trigger an exception
 */
public class OrganisationDeserializer
        extends JsonDeserializer<Organisation> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Organisation deserialize(JsonParser jsonParser,
                                    DeserializationContext dc)
            throws IOException {
        StringBuilder errors = new StringBuilder();

        Organisation organisation = new Organisation();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        // Deserialise general properties
        deserialiseNoarkSystemIdEntity(organisation, objectNode);
        deserialiseNoarkCreateEntity(organisation, objectNode, errors);
        deserialiseNoarkFinaliseEntity(organisation, objectNode, errors);

        // Deserialize organisationStatus
        JsonNode currentNode = objectNode.get(ORGANISATION_STATUS);
        if (currentNode != null) {
            organisation.setOrganisationStatus(currentNode.textValue());
            objectNode.remove(ORGANISATION_STATUS);
        }

        // Deserialize organisationName
        currentNode = objectNode.get(ORGANISATION_NAME);
        if (currentNode != null) {
            organisation.setOrganisationName(currentNode.textValue());
            objectNode.remove(ORGANISATION_NAME);
        }

        // Check that there are no additional values left after processing the tree
        // If there are additional throw a malformed input exception
        if (objectNode.size() != 0) {
            errors.append(String.format(MALFORMED_PAYLOAD,
                    ORGANISATION, checkNodeObjectEmpty(objectNode)));
        }

        if (0 < errors.length())
            throw new NikitaMalformedInputDataException(errors.toString());

        return organisation;
    }
}

