package nikita.common.repository.n5v5.admin;

import nikita.common.model.noark5.v5.admin.Organisation;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface IOrganisationRepository
        extends PagingAndSortingRepository<Organisation, UUID> {

    Organisation findBySystemId(UUID systemId);
}
