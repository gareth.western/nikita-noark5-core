package nikita.common.model.noark5.v5.hateoas.admin;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import nikita.common.model.nikita.NikitaPage;
import nikita.common.model.noark5.v5.hateoas.HateoasNoarkObject;
import nikita.common.model.noark5.v5.hateoas.IHateoasNoarkObject;
import nikita.common.model.noark5.v5.interfaces.entities.INoarkEntity;
import nikita.common.util.serializers.noark5v5.hateoas.admin.OrganisationHateoasSerializer;

import static nikita.common.config.N5ResourceMappings.ORGANISATION;

@JsonSerialize(using = OrganisationHateoasSerializer.class)
public class OrganisationHateoas
        extends HateoasNoarkObject
        implements IHateoasNoarkObject {

    public OrganisationHateoas(INoarkEntity entity) {
        super(entity);
    }

    public OrganisationHateoas(NikitaPage page) {
        super(page, ORGANISATION);
    }
}
