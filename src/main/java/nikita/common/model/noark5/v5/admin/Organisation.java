package nikita.common.model.noark5.v5.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import nikita.common.model.noark5.v5.NoarkGeneralEntity;
import nikita.common.model.noark5.v5.hateoas.admin.OrganisationHateoas;
import nikita.common.util.deserialisers.admin.OrganisationDeserializer;
import nikita.webapp.hateoas.admin.OrganisationHateoasHandler;
import nikita.webapp.util.annotation.HateoasObject;
import nikita.webapp.util.annotation.HateoasPacker;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import static nikita.common.config.Constants.TABLE_ORGANISATION;
import static nikita.common.config.N5ResourceMappings.*;

@Entity
@Table(name = TABLE_ORGANISATION)
@JsonDeserialize(using = OrganisationDeserializer.class)
@HateoasPacker(using = OrganisationHateoasHandler.class)
@HateoasObject(using = OrganisationHateoas.class)
public class Organisation
        extends NoarkGeneralEntity {

    @Column(name = ORGANISATION_NAME_ENG)
    @JsonProperty(ORGANISATION_NAME)
    private String organisationName;

    @Column(name = ORGANISATION_STATUS_ENG)
    @JsonProperty(ORGANISATION_STATUS)
    private String organisationStatus;

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public String getOrganisationStatus() {
        return organisationStatus;
    }

    public void setOrganisationStatus(String organisationStatus) {
        this.organisationStatus = organisationStatus;
    }
}
