package nikita.webapp.service.interfaces;

import nikita.common.model.noark5.v5.admin.Organisation;
import nikita.common.model.noark5.v5.hateoas.admin.OrganisationHateoas;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface IOrganisationService {
    @Transactional
    void save(Organisation organisation);

    OrganisationHateoas findAll();

    OrganisationHateoas findBySystemId(@NotNull UUID systemId);

    Organisation findOrganisationBySystemId(@NotNull UUID systemId);

    @Transactional
    OrganisationHateoas update(UUID systemId, Long version,
                               Organisation incomingOrganisation);

    @Transactional
    void deleteEntity(@NotNull UUID systemId);

    OrganisationHateoas generateDefaultOrganisation();
}
