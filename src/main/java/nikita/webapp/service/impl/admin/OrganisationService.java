package nikita.webapp.service.impl.admin;

import nikita.common.model.noark5.v5.admin.Organisation;
import nikita.common.model.noark5.v5.hateoas.admin.OrganisationHateoas;
import nikita.common.repository.n5v5.admin.IOrganisationRepository;
import nikita.common.util.exceptions.NoarkEntityNotFoundException;
import nikita.webapp.hateoas.interfaces.admin.IOrganisationHateoasHandler;
import nikita.webapp.service.application.IPatchService;
import nikita.webapp.service.impl.NoarkService;
import nikita.webapp.service.interfaces.IOrganisationService;
import nikita.webapp.service.interfaces.odata.IODataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import java.util.UUID;

import static nikita.common.config.Constants.INFO_CANNOT_FIND_OBJECT;

/**
 * Service class for Organisation
 */
@Service
public class OrganisationService
        extends NoarkService
        implements IOrganisationService {

    private static final Logger logger =
            LoggerFactory.getLogger(OrganisationService.class);

    private final IOrganisationRepository organisationRepository;
    private final IOrganisationHateoasHandler organisationHateoasHandler;

    public OrganisationService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IODataService odataService,
            IPatchService patchService,
            IOrganisationRepository organisationRepository,
            IOrganisationHateoasHandler organisationHateoasHandler) {
        super(entityManager, applicationEventPublisher, patchService,
                odataService);
        this.organisationRepository = organisationRepository;
        this.organisationHateoasHandler = organisationHateoasHandler;
    }

    // All CREATE operations

    /**
     * Persists a new organisation object to the database where
     * owned_by is set "system". It checks first to make sure that an
     * administrative unit with the given name does not exist. If it does exist
     * do nothing. This code should really only be called when creating some
     * basic data i nikita to make it operational for demo purposes.
     *
     * @param organisation organisation object with values set
     * @return the newly persisted organisation object
     */
    @Override
    @Transactional
    public void save(Organisation organisation) {
        organisationRepository.save(organisation);
    }

    // All READ methods

    /**
     * Retrieve all organisation
     *
     * @return list of all organisation
     */
    @Override
    public OrganisationHateoas findAll() {
        return (OrganisationHateoas) odataService.processODataQueryGet();
    }

    /**
     * Retrieve a single organisation identified by systemId
     *
     * @param systemId systemId of the organisation
     * @return the organisation
     */
    @Override
    public OrganisationHateoas findBySystemId(@NotNull final UUID systemId) {
        return packAsHateoas(organisationRepository.findBySystemId(systemId));
    }

    /**
     * Retrieve a single organisation identified by systemId
     *
     * @param systemId systemId of the organisation
     * @return the organisation
     */
    @Override
    public Organisation findOrganisationBySystemId(@NotNull final UUID systemId) {
        return organisationRepository.findBySystemId(systemId);
    }

    /**
     * Update a particular organisation identified by the given systemId.
     *
     * @param incomingOrganisation organisation to update
     * @return the updated organisation
     */
    @Override
    @Transactional
    public OrganisationHateoas update(UUID systemId, Long version,
                                      Organisation incomingOrganisation) {
        Organisation existingOrganisation =
                getOrganisationOrThrow(systemId);
        // Here copy all the values you are allowed to copy ....
        if (null != existingOrganisation.getOrganisationName()) {
            existingOrganisation.setOrganisationName(
                    incomingOrganisation.getOrganisationName());
        }
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingOrganisation.setVersion(version);
        return packAsHateoas(incomingOrganisation);
    }

    /**
     * Delete a organisation identified by the given systemId from the database.
     *
     * @param systemId systemId of the organisation to delete
     */
    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        deleteEntity(getOrganisationOrThrow(systemId));
    }

    @Override
    public OrganisationHateoas generateDefaultOrganisation() {
        Organisation organisation = new Organisation();
        organisation.setOrganisationName(
                "Formell navn på organisasjonen");
        organisation.setVersion(-1L, true);
        return packAsHateoas(organisation);
    }

    // All HELPER methods

    public OrganisationHateoas packAsHateoas(
            @NotNull final Organisation organisation) {
        OrganisationHateoas organisationHateoas =
                new OrganisationHateoas(organisation);
        applyLinksAndHeader(organisationHateoas, organisationHateoasHandler);
        return organisationHateoas;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, be aware
     * that you will only ever get a valid Organisation back. If there is no valid
     * Organisation, an exception is thrown
     *
     * @param systemId systemId of the organisation object you are looking for
     * @return the newly found organisation object or null if it does not exist
     */
    private Organisation getOrganisationOrThrow(@NotNull final UUID systemId) {
        Organisation organisation = organisationRepository.findBySystemId(systemId);
        if (organisation == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " Organisation, using systemId " + systemId;
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        }
        return organisation;
    }
}
