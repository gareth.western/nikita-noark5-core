package nikita.webapp.spring.security;

import nikita.common.model.noark5.v5.admin.Organisation;
import nikita.common.model.noark5.v5.admin.User;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * INikitaUserDetails
 * <p>
 * Allows for the identification of an organisation that a user belongs to.
 * Nikita supports multiple organisations co-existing with a single database.
 */
public interface INikitaUserDetails
        extends UserDetails {
    Organisation getOrganisation();

    User getUser();
}
