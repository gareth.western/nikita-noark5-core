package nikita.webapp.hateoas.interfaces.admin;

import nikita.webapp.hateoas.interfaces.IHateoasHandler;

public interface IOrganisationHateoasHandler
        extends IHateoasHandler {
}
