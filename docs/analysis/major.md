# Problems marked major

2021-12-06 17:50:39.451 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceArchiveUnitSystemId] attribute in the [java.util.Map] entity is mapped to a large
column type. Consider using either compact types or moving the large columns to separate tables or using multiple
entities mapped to the same database table so that you can choose which properties are to be fetched from the database
based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy
loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. For more info about this event,
check out this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:39.715 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceArchiveUnitSystemId] attribute in the [nikita.common.model.noark5.v5.EventLog] entity
is mapped to a large column type. Consider using either compact types or moving the large columns to separate tables or
using multiple entities mapped to the same database table so that you can choose which properties are to be fetched from
the database based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode
enhancement lazy loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. You should use
the @DynamicUpdate annotation so that the UPDATE statement contains only the columns that have been modified by the
currently running Persistence Context. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:39.924 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
EnumTypeStringEvent - The [authorityName] enum attribute in the [nikita.common.model.noark5.v5.admin.Authority] entity
uses the EnumType.STRING strategy, which has a bigger memory footprint than EnumType.ORDINAL. For more info about this
event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#EnumTypeStringEvent
2021-12-06 17:50:39.940 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referencePrecedenceApprovedBySystemID] attribute in the [java.util.Map] entity is mapped to a
large column type. Consider using either compact types or moving the large columns to separate tables or using multiple
entities mapped to the same database table so that you can choose which properties are to be fetched from the database
based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy
loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. For more info about this event,
check out this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.016 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceArchiveUnitSystemId] attribute in the [nikita.common.model.noark5.v5.ChangeLog] entity
is mapped to a large column type. Consider using either compact types or moving the large columns to separate tables or
using multiple entities mapped to the same database table so that you can choose which properties are to be fetched from
the database based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode
enhancement lazy loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. You should use
the @DynamicUpdate annotation so that the UPDATE statement contains only the columns that have been modified by the
currently running Persistence Context. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.362 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [fromSystemId] attribute in the [nikita.common.model.noark5.v5.secondary.CrossReference] entity
is mapped to a large column type. Consider using either compact types or moving the large columns to separate tables or
using multiple entities mapped to the same database table so that you can choose which properties are to be fetched from
the database based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode
enhancement lazy loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. You should use
the @DynamicUpdate annotation so that the UPDATE statement contains only the columns that have been modified by the
currently running Persistence Context. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.368 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [toSystemId] attribute in the [nikita.common.model.noark5.v5.secondary.CrossReference] entity is
mapped to a large column type. Consider using either compact types or moving the large columns to separate tables or
using multiple entities mapped to the same database table so that you can choose which properties are to be fetched from
the database based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode
enhancement lazy loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. You should use
the @DynamicUpdate annotation so that the UPDATE statement contains only the columns that have been modified by the
currently running Persistence Context. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.424 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referencePrecedenceApprovedBySystemID] attribute in
the [nikita.common.model.noark5.v5.secondary.Precedence] entity is mapped to a large column type. Consider using either
compact types or moving the large columns to separate tables or using multiple entities mapped to the same database
table so that you can choose which properties are to be fetched from the database based on the entity type. You should
use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy loading mechanism as, otherwise, the
column is fetched eagerly when loading the entity. You should use the @DynamicUpdate annotation so that the UPDATE
statement contains only the columns that have been modified by the currently running Persistence Context. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.507 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceFlowFromSystemID] attribute in the [java.util.Map] entity is mapped to a large column
type. Consider using either compact types or moving the large columns to separate tables or using multiple entities
mapped to the same database table so that you can choose which properties are to be fetched from the database based on
the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy loading
mechanism as, otherwise, the column is fetched eagerly when loading the entity. For more info about this event, check
out this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.508 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceFlowToSystemID] attribute in the [java.util.Map] entity is mapped to a large column
type. Consider using either compact types or moving the large columns to separate tables or using multiple entities
mapped to the same database table so that you can choose which properties are to be fetched from the database based on
the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy loading
mechanism as, otherwise, the column is fetched eagerly when loading the entity. For more info about this event, check
out this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.536 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceSignedOffCorrespondencePartSystemID] attribute in the [java.util.Map] entity is mapped
to a large column type. Consider using either compact types or moving the large columns to separate tables or using
multiple entities mapped to the same database table so that you can choose which properties are to be fetched from the
database based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode
enhancement lazy loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.537 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceSignedOffRecordSystemID] attribute in the [java.util.Map] entity is mapped to a large
column type. Consider using either compact types or moving the large columns to separate tables or using multiple
entities mapped to the same database table so that you can choose which properties are to be fetched from the database
based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy
loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. For more info about this event,
check out this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.549 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceArchiveUnitSystemId] attribute in the [java.util.Map] entity is mapped to a large
column type. Consider using either compact types or moving the large columns to separate tables or using multiple
entities mapped to the same database table so that you can choose which properties are to be fetched from the database
based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy
loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. For more info about this event,
check out this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.974 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceFlowFromSystemID] attribute in
the [nikita.common.model.noark5.v5.secondary.DocumentFlow] entity is mapped to a large column type. Consider using
either compact types or moving the large columns to separate tables or using multiple entities mapped to the same
database table so that you can choose which properties are to be fetched from the database based on the entity type. You
should use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy loading mechanism as, otherwise,
the column is fetched eagerly when loading the entity. You should use the @DynamicUpdate annotation so that the UPDATE
statement contains only the columns that have been modified by the currently running Persistence Context. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:40.976 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceFlowToSystemID] attribute in the [nikita.common.model.noark5.v5.secondary.DocumentFlow]
entity is mapped to a large column type. Consider using either compact types or moving the large columns to separate
tables or using multiple entities mapped to the same database table so that you can choose which properties are to be
fetched from the database based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the
bytecode enhancement lazy loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. You
should use the @DynamicUpdate annotation so that the UPDATE statement contains only the columns that have been modified
by the currently running Persistence Context. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:41.022 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceSignedOffCorrespondencePartSystemID] attribute in
the [nikita.common.model.noark5.v5.secondary.SignOff] entity is mapped to a large column type. Consider using either
compact types or moving the large columns to separate tables or using multiple entities mapped to the same database
table so that you can choose which properties are to be fetched from the database based on the entity type. You should
use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy loading mechanism as, otherwise, the
column is fetched eagerly when loading the entity. You should use the @DynamicUpdate annotation so that the UPDATE
statement contains only the columns that have been modified by the currently running Persistence Context. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:41.024 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [referenceSignedOffRecordSystemID] attribute in
the [nikita.common.model.noark5.v5.secondary.SignOff] entity is mapped to a large column type. Consider using either
compact types or moving the large columns to separate tables or using multiple entities mapped to the same database
table so that you can choose which properties are to be fetched from the database based on the entity type. You should
use the @Basic(fetch=LAZY) annotation and activate the bytecode enhancement lazy loading mechanism as, otherwise, the
column is fetched eagerly when loading the entity. You should use the @DynamicUpdate annotation so that the UPDATE
statement contains only the columns that have been modified by the currently running Persistence Context. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:41.531 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
LargeColumnEvent - The [documentTokens] attribute in the [nikita.common.model.noark5.v5.DocumentObject] entity is mapped
to a large column type. Consider using either compact types or moving the large columns to separate tables or using
multiple entities mapped to the same database table so that you can choose which properties are to be fetched from the
database based on the entity type. You should use the @Basic(fetch=LAZY) annotation and activate the bytecode
enhancement lazy loading mechanism as, otherwise, the column is fetched eagerly when loading the entity. You should use
the @DynamicUpdate annotation so that the UPDATE statement contains only the columns that have been modified by the
currently running Persistence Context. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#LargeColumnEvent
2021-12-06 17:50:41.551 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
DialectVersionEvent - Your application is using the [org.hibernate.dialect.MySQL5InnoDBDialect] Hibernate-specific
Dialect. Consider using the [org.hibernate.dialect.MySQL57Dialect] instead, as it's closer to your current database
server version [MySQL 5.7]. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#DialectVersionEvent
2021-12-06 17:50:41.562 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
QueryInClauseParameterPaddingEvent - You should set the [hibernate.query.in_clause_parameter_padding] configuration
property to the value of [true], as Hibernate entity queries can then make better use of statement caching and fewer
entity queries will have to be compiled while varying the number of parameters passed to the in query clause. For more
info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#QueryInClauseParameterPaddingEvent
2021-12-06 17:50:41.565 WARN 20057 --- [           main] Hypersistence Optimizer                  : MAJOR -
DefaultAuditStrategyEvent - Your application is using the [org.hibernate.envers.strategy.DefaultAuditStrategy] Hibernate
Envers strategy. Consider setting the [org.hibernate.envers.audit_strategy] configuration property to the value
of [org.hibernate.envers.strategy.internal.ValidityAuditStrategy] instead, as it can generate more efficient audit log
queries. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#DefaultAuditStrategyEvent
2021-12-06 17:50:41.567 WARN 20057 --- [           main] Hypersistence Optimizer                  : 159 issues were
found: 1 BLOCKER, 42 CRITICAL, 21 MAJOR, 95 MINOR

