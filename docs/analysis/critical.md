# Problems marked critical
2021-12-06 17:50:39.492 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [businessAddress] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.CorrespondencePartUnit] entity is mapped as the parent-side of
this relationship. The parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy
loading is enabled and the association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this
event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:39.493 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [contactInformation] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.CorrespondencePartUnit] entity is mapped as the parent-side of
this relationship. The parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy
loading is enabled and the association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this
event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:39.494 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [postalAddress] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.CorrespondencePartUnit] entity is mapped as the parent-side of
this relationship. The parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy
loading is enabled and the association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this
event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:39.507 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
BidirectionalSynchronizationEvent - The [referenceChangeLog] bidirectional association in
the [nikita.common.model.noark5.v5.secondary.ElectronicSignature] entity requires both ends to be synchronized. Consider
adding the [addChangeLog(nikita.common.model.noark5.v5.ChangeLog changeLog)]
and [removeChangeLog(nikita.common.model.noark5.v5.ChangeLog changeLog)] synchronization methods. For more info about
this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#BidirectionalSynchronizationEvent
2021-12-06 17:50:39.581 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [referenceElectronicSignature] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.RegistryEntry] entity is mapped as the parent-side of this relationship.
The parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy loading is enabled and
the association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this event, check out this
User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:39.612 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [businessAddress] one-to-one association in
the [nikita.common.model.noark5.v5.secondary.PartUnit] entity is mapped as the parent-side of this relationship. The
parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy loading is enabled and the
association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this event, check out this User
Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:39.613 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [contactInformation] one-to-one association in
the [nikita.common.model.noark5.v5.secondary.PartUnit] entity is mapped as the parent-side of this relationship. The
parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy loading is enabled and the
association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this event, check out this User
Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:39.614 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [postalAddress] one-to-one association in
the [nikita.common.model.noark5.v5.secondary.PartUnit] entity is mapped as the parent-side of this relationship. The
parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy loading is enabled and the
association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this event, check out this User
Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:39.649 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [referenceElectronicSignature] one-to-one association in
the [nikita.common.model.noark5.v5.DocumentDescription] entity is mapped as the parent-side of this relationship. The
parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy loading is enabled and the
association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this event, check out this User
Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:39.922 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
TableGeneratorEvent - The [id] identifier attribute in the [nikita.common.model.noark5.v5.admin.Authority] entity uses
the TABLE strategy, which does not scale very well. Consider using the IDENTITY identifier strategy instead, even if it
does not allow JDBC batch inserts. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#TableGeneratorEvent
2021-12-06 17:50:40.056 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referencePrecursor] one-to-one association in
the [nikita.common.model.noark5.v5.Series] entity is using a separate Foreign Key to reference the parent record.
Consider using @MapsId so that the identifier is shared with the parent row. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.066 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [referenceSuccessor] one-to-one association in the [nikita.common.model.noark5.v5.Series]
entity is mapped as the parent-side of this relationship. The parent-side of a one-to-one association is fetched eagerly
unless bytecode enhancement lazy loading is enabled and the association is annotated with @LazyToOne(
LazyToOneOption.NO_PROXY). For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:40.493 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [partUnit] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.BusinessAddress] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.494 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceCorrespondencePartUnit] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.BusinessAddress] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.563 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [partPerson] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.PostalAddress] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.564 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [partUnit] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.PostalAddress] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.565 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceCorrespondencePartPerson] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.PostalAddress] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.566 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceCorrespondencePartUnit] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.PostalAddress] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.589 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [contactInformation] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.CorrespondencePartPerson] entity is mapped as the parent-side
of this relationship. The parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy
loading is enabled and the association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this
event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:40.590 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [postalAddress] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.CorrespondencePartPerson] entity is mapped as the parent-side
of this relationship. The parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy
loading is enabled and the association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this
event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:40.591 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [residingAddress] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.CorrespondencePartPerson] entity is mapped as the parent-side
of this relationship. The parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy
loading is enabled and the association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this
event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:40.668 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceNextMeeting] one-to-one association in
the [nikita.common.model.noark5.v5.meeting.MeetingFile] entity is using a separate Foreign Key to reference the parent
record. Consider using @MapsId so that the identifier is shared with the parent row. For more info about this event,
check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.669 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referencePreviousMeeting] one-to-one association in
the [nikita.common.model.noark5.v5.meeting.MeetingFile] entity is using a separate Foreign Key to reference the parent
record. Consider using @MapsId so that the identifier is shared with the parent row. For more info about this event,
check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.718 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceFromMeetingRegistration] one-to-one association in
the [nikita.common.model.noark5.v5.meeting.MeetingRecord] entity is using a separate Foreign Key to reference the parent
record. Consider using @MapsId so that the identifier is shared with the parent row. For more info about this event,
check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.720 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceToMeetingRegistration] one-to-one association in
the [nikita.common.model.noark5.v5.meeting.MeetingRecord] entity is using a separate Foreign Key to reference the parent
record. Consider using @MapsId so that the identifier is shared with the parent row. For more info about this event,
check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.839 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [partPerson] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.ResidingAddress] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:40.841 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceCorrespondencePartPerson] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.ResidingAddress] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:41.021 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceSignedOffCorrespondencePart] one-to-one association in
the [nikita.common.model.noark5.v5.secondary.SignOff] entity is using a separate Foreign Key to reference the parent
record. Consider using @MapsId so that the identifier is shared with the parent row. For more info about this event,
check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:41.023 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceSignedOffRecord] one-to-one association in
the [nikita.common.model.noark5.v5.secondary.SignOff] entity is using a separate Foreign Key to reference the parent
record. Consider using @MapsId so that the identifier is shared with the parent row. For more info about this event,
check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:41.080 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [contactInformation] one-to-one association in
the [nikita.common.model.noark5.v5.secondary.PartPerson] entity is mapped as the parent-side of this relationship. The
parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy loading is enabled and the
association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this event, check out this User
Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:41.081 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [postalAddress] one-to-one association in
the [nikita.common.model.noark5.v5.secondary.PartPerson] entity is mapped as the parent-side of this relationship. The
parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy loading is enabled and the
association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this event, check out this User
Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:41.082 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [residingAddress] one-to-one association in
the [nikita.common.model.noark5.v5.secondary.PartPerson] entity is mapped as the parent-side of this relationship. The
parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy loading is enabled and the
association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this event, check out this User
Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:41.282 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [partPerson] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.ContactInformation] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:41.283 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [partUnit] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.ContactInformation] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:41.284 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceCorrespondencePartPerson] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.ContactInformation] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:41.286 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneWithoutMapsIdEvent - The [referenceCorrespondencePartUnit] one-to-one association in
the [nikita.common.model.noark5.v5.casehandling.secondary.ContactInformation] entity is using a separate Foreign Key to
reference the parent record. Consider using @MapsId so that the identifier is shared with the parent row. For more info
about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneWithoutMapsIdEvent
2021-12-06 17:50:41.543 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
OneToOneParentSideEvent - The [referenceElectronicSignature] one-to-one association in
the [nikita.common.model.noark5.v5.DocumentObject] entity is mapped as the parent-side of this relationship. The
parent-side of a one-to-one association is fetched eagerly unless bytecode enhancement lazy loading is enabled and the
association is annotated with @LazyToOne(LazyToOneOption.NO_PROXY). For more info about this event, check out this User
Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#OneToOneParentSideEvent
2021-12-06 17:50:41.553 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
ConsoleStatementLoggingEvent - By enabling the [hibernate.show_sql] configuration property, SQL statements are going to
be printed in the console, and that's not a good idea. You should remove this property and set the [org.hibernate.SQL]
logger on the [DEBUG] level. Even better than Hibernate-based logging, a JDBC DataSource logging framework (e.g.,
datasource-proxy) is a much more flexible option. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#ConsoleStatementLoggingEvent
2021-12-06 17:50:41.555 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
JdbcBatchSizeEvent - If you set the [hibernate.jdbc.batch_size] configuration property to a value greater than 1 (
usually between 5 and 30), Hibernate can then execute SQL statements in batches, therefore reducing the number of
database network roundtrips. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#JdbcBatchSizeEvent
2021-12-06 17:50:41.562 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
QueryPaginationCollectionFetchingEvent - You should set the [hibernate.query.fail_on_pagination_over_collection_fetch]
configuration property to the value of [true], as Hibernate can then prevent in-memory pagination when join fetching a
child entity collection. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#QueryPaginationCollectionFetchingEvent
2021-12-06 17:50:41.563 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
DefaultQueryPlanCacheMaxSizeEvent - You should set the [hibernate.query.plan_cache_max_size] configuration property to a
value that allows you to hold all JPQL, Criteria API, or SQL queries executed with Hibernate. The default query plan
cache size is [2048] and might not be enough for a non-trivial application. For more info about this event, check out
this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#DefaultQueryPlanCacheMaxSizeEvent
2021-12-06 17:50:41.566 ERROR 20057 --- [           main] Hypersistence Optimizer                  : CRITICAL -
JdbcStatementCacheSizeEvent - The JDBC statement cache is disabled. You should consider enabling it, as follows. You
should set the [cachePrepStmts] JDBC configuration property to the value of [true] and the [prepStmtCacheSize] property
to the value of [256] . Consider lowering the statement cache size if you don't have enough memory and increasing it if
you have plenty of memory and need to execute lots of distinct SQL statements. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#JdbcStatementCacheSizeEvent
2021-12-06 17:50:41.567 WARN 20057 --- [           main] Hypersistence Optimizer                  : 159 issues were
found: 1 BLOCKER, 42 CRITICAL, 21 MAJOR, 95 MINOR 2021-12-06 17:51:18.180 ERROR 20057 --- [nio-8092-exec-9]
Hypersistence Optimizer                  : CRITICAL - PaginationWithoutOrderByEvent -
The [SELECT fonds_1 FROM Fonds AS fonds_1] query uses pagination without an ORDER BY clause. Therefore, the result is
not deterministic since SQL does not guarantee any particular ordering unless an ORDER BY clause is being used. For more
info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.184 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT fonds_1 FROM Fonds AS fonds_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.227 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT fondscreator_1 FROM FondsCreator AS fondscreator_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.230 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT fondscreator_1 FROM FondsCreator AS fondscreator_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.267 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT series_1 FROM Series AS series_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.270 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT series_1 FROM Series AS series_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.294 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT classificationsystem_1 FROM ClassificationSystem AS classificationsystem_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.297 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT classificationsystem_1 FROM ClassificationSystem AS classificationsystem_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.322 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT class_1 FROM Class AS class_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.325 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT class_1 FROM Class AS class_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.349 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT file_1 FROM File AS file_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.352 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT file_1 FROM File AS file_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.377 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.381 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.407 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.410 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.435 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1] query uses
pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.438 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1] query uses
pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.466 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT casefile_1 FROM CaseFile AS casefile_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.468 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT casefile_1 FROM CaseFile AS casefile_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.492 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT registryentry_1 FROM RegistryEntry AS registryentry_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.496 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT registryentry_1 FROM RegistryEntry AS registryentry_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.518 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.524 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.543 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT precedence_1 FROM Precedence AS precedence_1] query uses pagination without
an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering
unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:18.546 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT precedence_1 FROM Precedence AS precedence_1] query uses pagination without
an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering
unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:19.269 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
QueryResultListSizeEvent - The [select generatedAlias0 from Country as generatedAlias0] query returned a List with [249]
entries. You should avoid fetching large result sets as they can impact both the user experience and resource usage. For
more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#QueryResultListSizeEvent
2021-12-06 17:51:19.533 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT administrativeunit_1 FROM AdministrativeUnit AS administrativeunit_1] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:19.537 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT administrativeunit_1 FROM AdministrativeUnit AS administrativeunit_1] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:19.564 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT user_1 FROM User AS user_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:19.567 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT user_1 FROM User AS user_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:19.651 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT changelog_1 FROM ChangeLog AS changelog_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:19.654 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT changelog_1 FROM ChangeLog AS changelog_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:19.672 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT eventlog_1 FROM EventLog AS eventlog_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:19.674 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT eventlog_1 FROM EventLog AS eventlog_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.849 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT fonds_1 FROM Fonds AS fonds_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.852 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT fonds_1 FROM Fonds AS fonds_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.875 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT fondscreator_1 FROM FondsCreator AS fondscreator_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.877 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT fondscreator_1 FROM FondsCreator AS fondscreator_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.892 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT series_1 FROM Series AS series_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.896 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT series_1 FROM Series AS series_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.913 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT classificationsystem_1 FROM ClassificationSystem AS classificationsystem_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.916 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT classificationsystem_1 FROM ClassificationSystem AS classificationsystem_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.924 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT class_1 FROM Class AS class_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.927 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT class_1 FROM Class AS class_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.941 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT file_1 FROM File AS file_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.944 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT file_1 FROM File AS file_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.980 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:32.984 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.015 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.019 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.042 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1] query uses
pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.045 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1] query uses
pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.059 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT casefile_1 FROM CaseFile AS casefile_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.064 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT casefile_1 FROM CaseFile AS casefile_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.082 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT registryentry_1 FROM RegistryEntry AS registryentry_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.087 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT registryentry_1 FROM RegistryEntry AS registryentry_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.110 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.113 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1] query uses pagination
without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular
ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.126 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT precedence_1 FROM Precedence AS precedence_1] query uses pagination without
an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering
unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.129 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT precedence_1 FROM Precedence AS precedence_1] query uses pagination without
an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering
unless an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.667 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
QueryResultListSizeEvent - The [select generatedAlias0 from Country as generatedAlias0] query returned a List with [249]
entries. You should avoid fetching large result sets as they can impact both the user experience and resource usage. For
more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#QueryResultListSizeEvent
2021-12-06 17:51:33.819 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT administrativeunit_1 FROM AdministrativeUnit AS administrativeunit_1] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.821 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT administrativeunit_1 FROM AdministrativeUnit AS administrativeunit_1] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.833 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT user_1 FROM User AS user_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.836 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT user_1 FROM User AS user_1] query uses pagination without an ORDER BY
clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless an ORDER
BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.867 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT changelog_1 FROM ChangeLog AS changelog_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.869 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT changelog_1 FROM ChangeLog AS changelog_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.878 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT eventlog_1 FROM EventLog AS eventlog_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.880 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent - The [SELECT eventlog_1 FROM EventLog AS eventlog_1] query uses pagination without an
ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any particular ordering unless
an ORDER BY clause is being used. For more info about this event, check out this User Guide link
- https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.923 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFonds AS fonds_1 WHERE fonds_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.926 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFonds AS fonds_1 WHERE fonds_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.962 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT fonds_1 FROM Fonds AS fonds_1 JOIN fonds_1.referenceParentFonds AS fonds_1 WHERE fonds_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.965 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT fonds_1 FROM Fonds AS fonds_1 JOIN fonds_1.referenceParentFonds AS fonds_1 WHERE fonds_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.992 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT fondscreator_1 FROM FondsCreator AS fondscreator_1 JOIN fondscreator_1.referenceFonds AS fonds_1 WHERE fonds_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:33.995 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT fondscreator_1 FROM FondsCreator AS fondscreator_1 JOIN fondscreator_1.referenceFonds AS fonds_1 WHERE fonds_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.040 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT fonds_1 FROM Fonds AS fonds_1 JOIN fonds_1.referenceFondsCreator AS fondscreator_1 WHERE fondscreator_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.042 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT fonds_1 FROM Fonds AS fonds_1 JOIN fonds_1.referenceFondsCreator AS fondscreator_1 WHERE fondscreator_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.144 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.148 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.169 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.173 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.212 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT casefile_1 FROM CaseFile AS casefile_1 JOIN casefile_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.216 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT casefile_1 FROM CaseFile AS casefile_1 JOIN casefile_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.241 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT classificationsystem_1 FROM ClassificationSystem AS classificationsystem_1 JOIN classificationsystem_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.243 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT classificationsystem_1 FROM ClassificationSystem AS classificationsystem_1 JOIN classificationsystem_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.324 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.328 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.350 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.354 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.376 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT casefile_1 FROM CaseFile AS casefile_1 JOIN casefile_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.378 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT casefile_1 FROM CaseFile AS casefile_1 JOIN casefile_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.397 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT classificationsystem_1 FROM ClassificationSystem AS classificationsystem_1 JOIN classificationsystem_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.400 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT classificationsystem_1 FROM ClassificationSystem AS classificationsystem_1 JOIN classificationsystem_1.referenceSeries AS series_1 WHERE series_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.479 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceClassificationSystem AS classificationsystem_1 WHERE classificationsystem_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.483 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceClassificationSystem AS classificationsystem_1 WHERE classificationsystem_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.517 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceClassificationSystem AS classificationsystem_1 WHERE classificationsystem_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.521 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceClassificationSystem AS classificationsystem_1 WHERE classificationsystem_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.563 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.567 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.595 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.598 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.641 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceChildClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.644 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceChildClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.661 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.664 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.700 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT keyword_1 FROM Keyword AS keyword_1 JOIN keyword_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.703 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT keyword_1 FROM Keyword AS keyword_1 JOIN keyword_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.762 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.767 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.789 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.792 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.827 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceChildClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.830 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceChildClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.843 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.845 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceClass AS class_1 WHERE class_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.925 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.928 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.946 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.950 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.995 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:34.998 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.023 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.026 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.088 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.092 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.138 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.144 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.302 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.308 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.342 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT precedence_1 FROM Precedence AS precedence_1 JOIN precedence_1.referenceCaseFile AS casefile_1 WHERE casefile_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.346 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT precedence_1 FROM Precedence AS precedence_1 JOIN precedence_1.referenceCaseFile AS casefile_1 WHERE casefile_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.386 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT registryentry_1 FROM RegistryEntry AS registryentry_1 JOIN registryentry_1.referenceFile AS casefile_1 WHERE casefile_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.390 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT registryentry_1 FROM RegistryEntry AS registryentry_1 JOIN registryentry_1.referenceFile AS casefile_1 WHERE casefile_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.429 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordnote_1 FROM RecordNote AS recordnote_1 JOIN recordnote_1.referenceFile AS casefile_1 WHERE casefile_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.433 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordnote_1 FROM RecordNote AS recordnote_1 JOIN recordnote_1.referenceFile AS casefile_1 WHERE casefile_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.506 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.510 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.528 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.532 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.554 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.556 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.578 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.582 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.627 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.630 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.659 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.661 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.769 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.772 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.824 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.826 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.841 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.844 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.867 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.870 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.893 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.896 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.923 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.927 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.959 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:35.961 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.081 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.084 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.140 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.144 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.157 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.160 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.185 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.188 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.210 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.213 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.241 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.245 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.284 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.288 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.399 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.403 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.461 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.464 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceChildFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.476 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.480 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.504 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.507 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.525 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.527 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT crossreference_1 FROM CrossReference AS crossreference_1 JOIN crossreference_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.551 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.554 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT series_1 FROM Series AS series_1 JOIN series_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.583 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.588 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0] query
uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not guarantee any
particular ordering unless an ORDER BY clause is being used. For more info about this event, check out this User Guide
link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.651 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT keyword_1 FROM Keyword AS keyword_1 JOIN keyword_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.653 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT keyword_1 FROM Keyword AS keyword_1 JOIN keyword_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.692 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT storagelocation_1 FROM StorageLocation AS storagelocation_1 JOIN storagelocation_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.694 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT storagelocation_1 FROM StorageLocation AS storagelocation_1 JOIN storagelocation_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.770 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.773 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceFile AS file_1 WHERE file_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.821 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.824 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.865 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.868 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.906 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT correspondencepart_1 FROM CorrespondencePart AS correspondencepart_1 JOIN correspondencepart_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.909 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT correspondencepart_1 FROM CorrespondencePart AS correspondencepart_1 JOIN correspondencepart_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.969 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:36.972 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.068 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.072 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.102 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT signoff_1 FROM SignOff AS signoff_1 JOIN signoff_1.referenceRegistryEntry AS registryentry_1 WHERE registryentry_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.105 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT signoff_1 FROM SignOff AS signoff_1 JOIN signoff_1.referenceRegistryEntry AS registryentry_1 WHERE registryentry_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.125 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1 JOIN documentflow_1.referenceRegistryEntry AS registryentry_1 WHERE registryentry_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.128 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1 JOIN documentflow_1.referenceRegistryEntry AS registryentry_1 WHERE registryentry_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.186 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.190 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.226 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.229 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.268 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT correspondencepart_1 FROM CorrespondencePart AS correspondencepart_1 JOIN correspondencepart_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.274 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT correspondencepart_1 FROM CorrespondencePart AS correspondencepart_1 JOIN correspondencepart_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.362 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.364 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.473 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.476 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.493 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT signoff_1 FROM SignOff AS signoff_1 JOIN signoff_1.referenceRegistryEntry AS registryentry_1 WHERE registryentry_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.495 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT signoff_1 FROM SignOff AS signoff_1 JOIN signoff_1.referenceRegistryEntry AS registryentry_1 WHERE registryentry_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.513 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1 JOIN documentflow_1.referenceRegistryEntry AS registryentry_1 WHERE registryentry_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.515 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1 JOIN documentflow_1.referenceRegistryEntry AS registryentry_1 WHERE registryentry_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.563 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.569 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.608 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.611 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.658 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT correspondencepart_1 FROM CorrespondencePart AS correspondencepart_1 JOIN correspondencepart_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.661 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT correspondencepart_1 FROM CorrespondencePart AS correspondencepart_1 JOIN correspondencepart_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.725 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.727 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.814 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.816 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.832 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1 JOIN documentflow_1.referenceRecordNote AS recordnote_1 WHERE recordnote_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.834 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentflow_1 FROM DocumentFlow AS documentflow_1 JOIN documentflow_1.referenceRecordNote AS recordnote_1 WHERE recordnote_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.871 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.874 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.923 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.925 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.954 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT correspondencepart_1 FROM CorrespondencePart AS correspondencepart_1 JOIN correspondencepart_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.956 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT correspondencepart_1 FROM CorrespondencePart AS correspondencepart_1 JOIN correspondencepart_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:37.998 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.000 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.020 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT keyword_1 FROM Keyword AS keyword_1 JOIN keyword_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.023 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT keyword_1 FROM Keyword AS keyword_1 JOIN keyword_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.101 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.104 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT nationalidentifier_1 FROM NationalIdentifier AS nationalidentifier_1 JOIN nationalidentifier_1.referenceRecordEntity AS recordentity_1 WHERE recordentity_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.133 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.136 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.164 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 JOIN documentobject_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.166 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 JOIN documentobject_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.193 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.196 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.232 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.235 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.283 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.287 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.307 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 JOIN documentobject_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.310 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 JOIN documentobject_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.332 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.335 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.369 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.371 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.444 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.448 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.470 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 JOIN documentobject_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.474 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 JOIN documentobject_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.495 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.498 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.530 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.533 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.560 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT author_1 FROM Author AS author_1 JOIN author_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.563 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT author_1 FROM Author AS author_1 JOIN author_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.602 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.607 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.626 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 JOIN documentobject_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.628 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 JOIN documentobject_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.654 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.658 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT part_1 FROM Part AS part_1 JOIN part_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.688 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.692 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT comment_1 FROM Comment AS comment_1 JOIN comment_1.referenceDocumentDescription AS documentdescription_1 WHERE documentdescription_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.764 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT conversion_1 FROM Conversion AS conversion_1 JOIN conversion_1.referenceDocumentObject AS documentobject_1 WHERE documentobject_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:38.767 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT conversion_1 FROM Conversion AS conversion_1 JOIN conversion_1.referenceDocumentObject AS documentobject_1 WHERE documentobject_1.systemId = :parameter_0]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.587 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceKeyword AS keyword_1 WHERE class_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.589 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceKeyword AS keyword_1 WHERE class_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.619 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceKeyword AS keyword_1 WHERE class_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.623 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT class_1 FROM Class AS class_1 JOIN class_1.referenceKeyword AS keyword_1 WHERE class_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.779 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceComment AS comment_1 WHERE documentdescription_1.ownedBy = :parameter_0 and comment_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.782 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceComment AS comment_1 WHERE documentdescription_1.ownedBy = :parameter_0 and comment_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.803 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceComment AS comment_1 WHERE file_1.ownedBy = :parameter_0 and comment_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.807 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceComment AS comment_1 WHERE file_1.ownedBy = :parameter_0 and comment_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.965 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceKeyword AS keyword_1 WHERE file_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:43.970 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceKeyword AS keyword_1 WHERE file_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.006 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceKeyword AS keyword_1 WHERE file_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.010 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceKeyword AS keyword_1 WHERE file_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.050 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceStorageLocation AS storagelocation_1 WHERE file_1.ownedBy = :parameter_0 and storagelocation_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.054 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceStorageLocation AS storagelocation_1 WHERE file_1.ownedBy = :parameter_0 and storagelocation_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.090 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceStorageLocation AS storagelocation_1 WHERE file_1.ownedBy = :parameter_0 and storagelocation_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.094 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceStorageLocation AS storagelocation_1 WHERE file_1.ownedBy = :parameter_0 and storagelocation_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.421 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT registryentry_1 FROM RegistryEntry AS registryentry_1 JOIN registryentry_1.referenceSignOff AS signoff_1 WHERE registryentry_1.ownedBy = :parameter_0 and signoff_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.425 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT registryentry_1 FROM RegistryEntry AS registryentry_1 JOIN registryentry_1.referenceSignOff AS signoff_1 WHERE registryentry_1.ownedBy = :parameter_0 and signoff_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.466 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceKeyword AS keyword_1 WHERE recordentity_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.470 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceKeyword AS keyword_1 WHERE recordentity_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.503 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceKeyword AS keyword_1 WHERE recordentity_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:51:44.508 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 JOIN recordentity_1.referenceKeyword AS keyword_1 WHERE recordentity_1.ownedBy = :parameter_0 and keyword_1.systemId = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.518 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 WHERE documentobject_1.ownedBy = :parameter_0 and documentobject_1.checksumAlgorithm = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.524 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentobject_1 FROM DocumentObject AS documentobject_1 WHERE documentobject_1.ownedBy = :parameter_0 and documentobject_1.checksumAlgorithm = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.549 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 WHERE documentdescription_1.ownedBy = :parameter_0 and documentdescription_1.createdBy = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.554 ERROR 20057 --- [nio-8092-exec-6] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 WHERE documentdescription_1.ownedBy = :parameter_0 and documentdescription_1.createdBy = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.580 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 WHERE documentdescription_1.ownedBy = :parameter_0 and documentdescription_1.documentStatusCode = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.585 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 WHERE documentdescription_1.ownedBy = :parameter_0 and documentdescription_1.documentStatusCode = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.607 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 WHERE documentdescription_1.ownedBy = :parameter_0 and documentdescription_1.documentStatusCode = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.612 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 WHERE documentdescription_1.ownedBy = :parameter_0 and documentdescription_1.documentStatusCode = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.641 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 WHERE recordentity_1.ownedBy = :parameter_0 and recordentity_1.title like :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.646 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT recordentity_1 FROM RecordEntity AS recordentity_1 WHERE recordentity_1.ownedBy = :parameter_0 and recordentity_1.title like :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.675 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referencePart AS part_1 WHERE file_1.ownedBy = :parameter_0 and part_1.name like :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.681 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referencePart AS part_1 WHERE file_1.ownedBy = :parameter_0 and part_1.name like :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.708 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referencePart AS part_1 WHERE file_1.ownedBy = :parameter_0 and part_1.partRoleCode = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.712 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referencePart AS part_1 WHERE file_1.ownedBy = :parameter_0 and part_1.partRoleCode = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.738 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 WHERE file_1.ownedBy = :parameter_0 and file_1.title like :parameter_1 AND file_1.documentMediumCode is null and file_1.caseYear is not null]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.742 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 WHERE file_1.ownedBy = :parameter_0 and file_1.title like :parameter_1 AND file_1.documentMediumCode is null and file_1.caseYear is not null]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.765 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS unitidentifier_1 WHERE file_1.ownedBy = :parameter_0 and unitidentifier_1.organisationNumber = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.769 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS unitidentifier_1 WHERE file_1.ownedBy = :parameter_0 and unitidentifier_1.organisationNumber = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.785 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.organisationNumber = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.788 ERROR 20057 --- [nio-8092-exec-1] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.organisationNumber = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.804 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.socialSecurityNumber = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.807 ERROR 20057 --- [nio-8092-exec-2] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.socialSecurityNumber = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.838 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.socialSecurityNumber is not null]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.842 ERROR 20057 --- [nio-8092-exec-8] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.socialSecurityNumber is not null]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.865 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.municipalityNumber = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.868 ERROR 20057 --- [nio-8092-exec-4] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.municipalityNumber = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.887 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.coordinateSystemCode = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.892 ERROR 20057 --- [nio-8092-exec-7] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.coordinateSystemCode = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.917 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.x > :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.921 ERROR 20057 --- [io-8092-exec-10] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceNationalIdentifier AS nationalidentifier_1 WHERE file_1.ownedBy = :parameter_0 and nationalidentifier_1.x > :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.951 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceKeyword AS keyword_1 WHERE file_1.ownedBy = :parameter_0 and keyword_1.keyword = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.954 ERROR 20057 --- [nio-8092-exec-3] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceKeyword AS keyword_1 WHERE file_1.ownedBy = :parameter_0 and keyword_1.keyword = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.975 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceKeyword AS keyword_1 WHERE file_1.ownedBy = :parameter_0 and keyword_1.keyword is not null]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:00.978 ERROR 20057 --- [nio-8092-exec-5] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT file_1 FROM File AS file_1 JOIN file_1.referenceKeyword AS keyword_1 WHERE file_1.ownedBy = :parameter_0 and keyword_1.keyword is not null]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:01.008 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceAuthor AS author_1 WHERE documentdescription_1.ownedBy = :parameter_0 and author_1.author = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
2021-12-06 17:52:01.011 ERROR 20057 --- [nio-8092-exec-9] Hypersistence Optimizer                  : CRITICAL -
PaginationWithoutOrderByEvent -
The [SELECT documentdescription_1 FROM DocumentDescription AS documentdescription_1 JOIN documentdescription_1.referenceAuthor AS author_1 WHERE documentdescription_1.ownedBy = :parameter_0 and author_1.author = :parameter_1]
query uses pagination without an ORDER BY clause. Therefore, the result is not deterministic since SQL does not
guarantee any particular ordering unless an ORDER BY clause is being used. For more info about this event, check out
this User Guide link - https://vladmihalcea.com/hypersistence-optimizer/docs/user-guide/#PaginationWithoutOrderByEvent
