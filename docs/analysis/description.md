# Hibernate analysis

Thomas bought a one year license for [hypersistence optimizer](https://vladmihalcea.com/hypersistence-optimizer) to test
the nikita domain model. I have for a long time believed there are problems and that we are experiencing
the [N+1 problem](https://vladmihalcea.com/n-plus-1-query-problem/) somewhere.

I ran runtest with hypersistence optimizer and filtered out the categories of problems so that we can document them here
in the codebase and deal with them one-by-one.

A summary shows the following has to be dealt with:

    159 issues were found: 1 BLOCKER, 42 CRITICAL, 21 MAJOR, 95 MINOR

There are two types of minor issues. 1 that the @Version field entity is mapped to an 8-byte numeric column. I don't see
why this is a problem really. The hint is to use a more compact type rather than a larger type. The other type is that
there seems to be a save happening during a transaction. This is related to documentdescrption and documentobject. It's
not obvious why this is a problem at the moment. I think it likely has to do with the fact that we automatically
generate a pdf from an incoming odt and this causes a save within an outer transaction.

The major issues seem to be related to modelling and some recommendations. These should be examined and dealt with.

The critical issues are all a lack of orderby clause when pagination is in use as the result set may not be
deterministic. This should be straight forward to fix.

The BLOCKER issue is that we do not use a tool like liquibase for database migration. It is our intention to go to a
tool like liquibase.
